(:
    Exercício 7 (b) Faca uma consulta que retorne a média de bids de cada
    usuário (dica: usar um for para pegar cada usuário e depois um let)

    <avg_bids>
        <user id="U01" avgBid="220"></user>
        <user id="U02" avgBid="387"></user>
        <user id="U03" avgBid="487"></user>
        <user id="U04" avgBid="266"></user>
        <user id="U05" avgBid="110"></user>
        <user id="U06" avgBid=""></user>
    </avg_bids>
:)

<avg_bids>
{
    for $u in doc('users.xml')/users/user_tuple
    let $b := doc('bids.xml')/bids/bid_tuple
    where $b/userid = $u/userid
    order by $u/userid
    return
        <user id="{ $u/userid/text() }" avgBid="{ avg($b/bid) }"></user>
}
</avg_bids>

