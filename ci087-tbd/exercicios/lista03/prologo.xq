define global $bids { treat as document
(document("bids.xml")) }
define global $users { treat as document
(document("users.xml")) }
define global $items { treat as document
(document("items.xml")) }

define element items {
  element item_tuple*
}

define element users {
  element user_tuple*
}

define element bids {
  element bid_tuple*
}

define element item_tuple {
  element itemno,
  element description,
  element offered_by,
  element start_date,
  element end_date,
  element reserve_price
}

define  element itemno {xsd:int}
define  element description {xsd:string}
define  element offered_by {xsd:string}
define  element start_date {xsd:date}
define  element end_date {xsd:date}
define  element reserve_price {xsd:int}

define element user_tuple {
  element userid,
  element name,
  element rating
}

define   element userid {xsd:string}
define   element name {xsd:string}
define   element rating {xsd:string}

define element bid_tuple {
  element userid,
  element itemno,
  element bid,
  element bid_date
}

define   element userid {xsd:string}
define   element itemno {xsd:int}
define   element bid {xsd:int}
define   element bid_date {xsd:date}
