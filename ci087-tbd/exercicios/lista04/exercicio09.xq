(:9. Para cada de genero existente, obtenha o seu nome e todos os filmes do
genero. (Agrupamento) :)

<results>
{
    let $genres := distinct-values(fn:doc('imdb.xml')/imdb/movie/genre)
    for $g in $genres
    return
        <genre name="{ $g }">
        {
            let $movies := fn:doc('imdb.xml')/imdb/movie[genre = $g]
            for $m in $movies
            return $m/title
        }
        </genre>
}
</results>
