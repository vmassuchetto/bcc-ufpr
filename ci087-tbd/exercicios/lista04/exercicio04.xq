(: 4. Quantos filmes tem duracao (runtime) maior que 60 minutos?  (Agregacao)
:)

<results>
{
    for $m in fn:doc('imdb.xml')/imdb
    where $m/movie/runtime > 60
    return count($m/movie)
}
</results>
