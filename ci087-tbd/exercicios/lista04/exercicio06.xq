(: 6. Quantos paises sao mencionados no documento como pais de origem de um
filme (originCountry) ou pais de nascimento de uma pessoa (BirthCountry)?
(Agregacao e operacao aritmetica) :)

<results>
{
    for $pc in fn:doc('imdb.xml')/imdb/person/BirthCountry/text()
    let $pr := replace(replace($pc, '.*,', ''), '^ +', '')
    for $p in $pr
    return distinct-values($p)
}
</results>
