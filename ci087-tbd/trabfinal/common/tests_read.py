#!/usr/bin/python

#raw_input()

import sys
import os
sys.path.insert(0, os.path.abspath('../' + sys.argv[1]))

import random

from django.core.management import setup_environ
from project import settings
setup_environ(settings)

from project.app.models import Country, State, City, \
    Company, CompanyType, ProductType, ProductCurrency, \
    Product, Person, Sale

from tests_params import n_country, n_state, n_city, n_company_type, \
    n_company, n_product_type, n_product_currency, n_product, \
    n_person, n_sale

i = 0
while i < n_country:
    rand_country = Country.objects.filter(name = random.randint(0, n_country))[:1]
    print "try to get %s" % rand_country[0].name
    if rand_country:
        print "got rand_country %s" % rand_country[0].name
        i += 1

i = 0
while i < n_state:
    rand_state = State.objects.filter(name = random.randint(0, n_state-1))[:1]
    print "try to get %s" % rand_state[0].name
    if rand_state:
        print "got rand_state %s" % rand_state[0].name
        i += 1

i = 0
while i < n_city:
    rand_city = City.objects.filter(name = random.randint(0, n_city-1))[:1]
    print "try to get %s" % rand_city[0].name
    if rand_city:
        print "got rand_city %s" % rand_city[0].name
        i += 1

i = 0
while i < n_company_type:
    rand_company_type = CompanyType.objects.filter(type = random.randint(0, n_company_type-1))[:1]
    print "try to get %s" % rand_company_type[0].type
    if rand_company_type:
        print "got rand_company_type %s" % rand_company_type[0].type
        i += 1

i = 0
while i < n_company:
    rand_company = Company.objects.filter(name = random.randint(0, n_company))[:1]
    if rand_company:
        print "got rand_company %s" % rand_company[0].name
        i += 1

i = 0
while i < n_product_type:
    rand_product_type = ProductType.objects.filter(name = random.randint(0, n_product_type-1))[:1]
    print "try to get %s" % rand_product_type[0].name
    if rand_product_type:
        print "got rand_product_type %s" % rand_product_type[0].name
        i += 1

i = 0
while i < n_product_currency:
    rand_product_currency = ProductCurrency.objects.filter(name = random.randint(0, n_product_currency-1))[:1]
    print "try to get %s" % rand_product_currency[0].name
    if rand_product_currency:
        print "got rand_product_currency %s" % rand_product_currency[0].name
        i += 1

i = 0
while i < n_product:
    rand_product = Product.objects.filter(name = random.randint(0, n_product-1))[:1]
    print "try to get %s" % rand_product[0].name
    if rand_product:
        print "got rand_product %s" % rand_product[0].name
        i += 1

i = 0
while i < n_person:
    rand_person = Person.objects.filter(name = random.randint(0, n_person-1))[:1]
    print "try to get %s" % rand_person[0].name
    if rand_person:
        print "got rand_person %s" % rand_person[0].name
        i += 1

i = 0
while i < n_sale:
    rand_sale = Sale.objects.filter(id = random.randint(0, n_sale-1))[:1]
    print "try to get %s" % rand_sale[0].id
    if rand_sale:
        print "got rand_sale %s" % rand_sale[0].id
        i += 1


