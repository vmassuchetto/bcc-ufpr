#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    int n;
    int g;
    int m;
    int s;

    // node 1 --> [2, exit]
    if (argc != 2) {
        printf("Sem parametros\n");
        exit(1);

    // node 2
    } else {
        n = atoi(argv[1]);
        g = n/3600;
        m = (n/60) % 60;
        s = n % 60;
        printf("\n%d segundos = %d graus, %d minutos e %d segundos.\n\n",
            n, g, m, s);
    }

    return 0;
}

