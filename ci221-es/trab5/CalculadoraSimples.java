public class CalculadoraSimples {
     
    //Atributos
    private int memorySave_ms; //salva ou armazena um valor na memoria
             
    //Construtor da classe
    public CalculadoraSimples (){
        memorySave_ms = 0;
    }
     
    //metodos da classe calculadora
    public int somar ( int valor1 , int valor2 ){
        return valor1 + valor2;
    }
     
    public int subtrair ( int valor1 , int valor2 ){
        return valor1 - valor2;
         
    }
     
    public int multiplicar ( int valor1, int valor2 ){
        return valor1 * valor2;
    }
     
    public int dividir( int valor1 , int valor2 ){
        if ( valor2 != 0 ){
            return valor1 / valor2;
        }
        else
            return -1;
    }
     
    //Metodos das funcoes de memoria
    public void memoryAdiciona ( int valor ){
        memorySave_ms += valor;
    }
     
    public void memorySalvaMS  ( int valor ){
        memorySave_ms = valor;
    }
     
    public void limparMC(){
        memorySave_ms = 0;
    }
     
    //Metodos acessores
    public int getMS(){
        return memorySave_ms;
    }
             
}//fim da classe CalculadoraSimples