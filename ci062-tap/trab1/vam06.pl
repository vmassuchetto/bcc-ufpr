%                            ------------------
%                     Universidade Federal do Paraná
%                   Bacharelado em Ciência da Computação
%                   Técnicas Alternativas de Programação
%                            ------------------
%
%                     Busca do Menor Caminho em Prolog

%                        Vinicius André Massuchetto
%                          (vam06 - GRR20063784)

%                 Programa construído sobre o SWI Prolog 5.8.2


% Declaração das distâncias do grafo

	via_terrestre(a,b,4).
	via_terrestre(b,e,6).
	via_terrestre(a,c,23).
	via_terrestre(a,g,13).
	via_terrestre(c,d,54).
	via_terrestre(d,e,30).
	via_terrestre(e,f,17).
	via_terrestre(f,g,45).
	via_terrestre(j,k,75).
	via_terrestre(j,l,75).
	via_terrestre(k,h,75).
	via_terrestre(k,i,211).
	via_terrestre(i,h,142).
	via_terrestre(m,n,101).
	via_terrestre(m,o,138).
	via_terrestre(m,r,17).
	via_terrestre(n,o,23).
	via_terrestre(r,q,23).
	via_terrestre(q,p,15).
	via_terrestre(p,o,12).

	via_aerea(e,j,3).
	via_aerea(e,m,87).
	via_aerea(j,m,9).

% `vai_terrestre(A,B,C)`
% A,B: Cidades
% C: Distância entre as cidades
% Faz com que as distâncias terrestres possam ser interpretadas
% também de trás para frente.

	vai_terrestre(A,B,C) :- via_terrestre(A,B,C).
	vai_terrestre(A,B,C) :- via_terrestre(B,A,C).

% `vai_aerea(A,B,C)`
% A,B: Cidades
% C: Distância entre as cidades
% Faz com que as distâncias aéreas possam ser interpretadas
% também de trás para frente.

	vai_aerea(A,B,C) :- via_aerea(A,B,C).
	vai_aerea(A,B,C) :- via_aerea(B,A,C).

% `subcaminho(A,B,V,VR,C,CT)`
% Subcaminhos possíveis entre duas cidades
% A,B: Cidades
% V: Cidades visitadas de A a B
% VR: Fornece lista das cidades visitadas
% C: Custo das etapas de visita das cidades
% CT: Custo total de visita de todas as cidades

	subcaminho(A, A, [A], [], _, 0).
	subcaminho(B, B, _, [B], _, 0).
	subcaminho(I, F, V, [I|C], _, S) :-
		vai_terrestre(I, E, Ni),
		not(member(E, V)),
		subcaminho(E, F, [E|V], C, Ni, Si),
		S is Ni + Si.

% `menor_custo(L,V,C)`
% Vaminho de menor custo entre os caminhos fornecidos.
% L: Lista com os caminhos [a,b|...] no formato [[a,b|...], custo]
% V: Via com o menor custo das sublistas de L
% C: Menor custo das sublistas de L

	menor_custo([],_,_).
	menor_custo([[V,C]], V, C).
	menor_custo([[V1,C1],[_,C2]|L], V, C) :-
		C1 =< C2,
		menor_custo([[V1,C1]|L], V, C).
	menor_custo([[_,C1],[V2,C2]|L], V, C) :-
		C1 > C2,
		menor_custo([[V2,C2]|L], V, C).

% `menor_subcaminho(A,B,V,C)`
% Viagem de menor custo entre duas vias terrestres.
% A,B: Cidades
% V: Via com o menor custo
% C: Custo da via V

	melhor_subcaminho(A, B, V, C) :-
		findall([X,K],subcaminho(A, B, [A], X, _, K),VS),
		menor_custo(VS, V, C).

% `vias_aereas(L)`
% Fornece todas as cidades que possuem aeroportos
% L: Lista de todas as cidades com aeroporto

	vias_aereas(L) :-
		findall(A,vai_aerea(A,_,_),B),
		sort(B,L).

% `aeroporto(C,A,V,K)`
% Fornece o melhor caminho de uma cidade ao seu
% aeroporto correspondente
% C: Cidade para encontrar o aeroporto correspondente
% A: Aeroporto que possui ligação terrestre com a cidade C
% V: Via que leva a cidade C ao aeroporto A
% K: Custo da via V

	aeroporto(C, A, V, K) :-
		vias_aereas(VA),
		aeroporto2(VA, C, A, V, K).

% `aeroporto2(L,C,A,V,K)`
% Auxiliar à `aeroporto(C,A,V,K)`, testa os caminhos possíveis de uma
% cidade ao seu aeroporto correspondente.

	aeroporto2([],_,_,_,_).
	aeroporto2([H|T], C, A, V, K) :-
		aeroporto2(T, C, _, V, K),
		melhor_subcaminho(C, H, V, K),
		A = H.

% `inverte(L,I)`
% Inverte a ordem de uma lista
% L: Lista
% I: Lista invertida

	inverte(L, I) :-
		inv(L, I, []).
	inv([], I, I) :- !.
	inv([X|Y], I, A) :-
		inv(Y, I, [X|A]).

% `concatena(A,B,L)`
% Concatena duas listas
% A,B: Listas
% L: Listas A e B concatenadas

	concatena([], B, B).
	concatena([AH|AT], B, [AH|L]) :-
		concatena(AT, B, L).

% `caminho(A,B,L)`
% Melhor caminho com via aérea entre duas cidades de
% subculturas diferentes
% A,B: Cidades
% L: Caminho entre as cidades A e B

	caminho(A,B,L) :-
		aeroporto(A, _, VA, _),
		aeroporto(B, _, VB, _),
		inverte(VB, VBI),
		concatena(VA, VBI, L).
