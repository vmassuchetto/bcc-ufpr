#!/usr/bin/perl

# Lançamento do modo servidor
# Arquivo em UTF-8

use IO::Socket::INET;
use Net::Ping;
use POSIX qw/strftime/;
use Sys::Hostname qw(hostname);
use Getopt::Long;

# Parse dos parâmetros

GetOptions(
    'local-port=i'  => \$local_port,
    'remote-host=s' => \$remote_host,
    'remote-port=i' => \$remote_port
);

# Opções padrão

$proxy_mode = 0;
$ping_tries = 3;

# Tenta determinar o próprio IP

$local_host = inet_ntoa ((gethostbyname(hostname()))[4])
    or die printf "[%s] Não foi possível determinar o próprio IP. Saindo.\n",
        strftime ('%D %T', localtime);

# Verifica validade dos parâmetros

if ((!$local_port) || (
        ($remote_host && !$remote_port) ||
        (!$remote_host && $remote_port)
    )) {
    printf "Modo de uso:\n" .
        "./server.pl\n" .
        "\t--local-port <porta>\n" .
        "\t[--remote-host <host>]\n" .
        "\t[--remote-port <porta>]\n\n";
    exit(1);
}

# Inicia em modo de escuta na porta especificada

printf "[%s] Iniciando servidor em %s:%s.\n",
    strftime ('%D %T', localtime), $local_host, $local_port;

$socket = new IO::Socket::INET (
    LocalHost => $local_host,
    LocalPort => $local_port,
    Proto => 'tcp',
    Listen => 5,
    Reuse => 1
) or die "[" . strftime ('%D %T', localtime) . "]" .
    " Erro ao criar socket. Saindo. \n";

if ($remote_host && $remote_port) {
    $proxy_mode = 1;
    printf "[%s] Requisições serão encaminhadas para %s:%s.\n",
        strftime ('%D %T', localtime), $remote_host, $remote_port;
}

# Loop do servidor

while (1) {

    printf "[%s] Escutando na porta %s.\n",
        strftime('%D %T', localtime), $local_port;

    # Recebe pacote

    $socket_client = $socket->accept();

    printf "[%s] Conexão realizada com %s:%s.\n",
        strftime ('%D %T', localtime), $socket_client->peerhost,
        $socket_client->peerport();

    $socket_client->recv($data, 1024);

    # Verifica validade do pacote

    if (length($data) <= 0) {

        printf "[%s] Requisição inválida de %s:%s.\n",
            strftime ('%D %T', localtime), $socket_client->peerhost,
            $socket_client->peerport;

        printf "[%s] Informando falha ao cliente %s:%s.\n",
            strftime ('%D %T', localtime), $socket_client->peerhost,
            $socket_client->peerport;

        $socket_client->send(0);

        next;
    }

    # Inicia o ping se estiver em modo ping

    if (!$proxy_mode) { $p = Net::Ping->new('syn'); }

    # Loop para tentativas do ping

    $i = 0;
    while (1) {

        $i++;

        printf "[%s] Tentativa %s para %s.\n",
            strftime ('%D %T', localtime), $i, $data;

        sleep(1);

        # Número máximo de tentativas do ping

        if ($i > $ping_tries) {

            # Informa falha e sai

            printf "[%s] [FALHA] %s não está acessível.\n",
                strftime ('%D %T', localtime), $data;

            printf "[%s] Informando falha ao cliente %s:%s.\n",
                strftime ('%D %T', localtime), $socket_client->peerhost,
                $socket_client->peerport;

            $socket_client->send(0);

            last;
        }

        # Repassa a requisição se estiver em modo de repasse

        if ($proxy_mode) {

            printf "[%s] Requisitando ping em %s para %s:%s.\n",
                strftime ('%D %T', localtime), $data, $remote_host,
                $remote_port;

            $socket_server = new IO::Socket::INET (
                PeerHost => $remote_host,
                PeerPort => $remote_port,
                Proto => 'tcp',
            ) or die "[" . strftime ('%D %T', localtime) . "]" .
                " Erro ao criar socket. Saindo.\n";

            # Espera resposta

            $socket_server->send($data);
            $socket_server->recv($recdata, 1024);
            $socket_server->close();

            $socket_client->send($recdata);
            last;

        # Executa ping se estiver em modo ping

        } else {

            if (!$p->ping($data)) {
                printf "[%s] Resposta falhou de %s.\n",
                    strftime ('%D %T', localtime), $data;
                next;
            }

        }

        # Informa sucesso e sai

        printf "[%s] [SUCESSO] %s está acessível.\n",
            strftime ('%D %T', localtime), $data;

        printf "[%s] Informando cliente %s:%s do sucesso.\n",
            strftime ('%D %T', localtime), $socket_client->peerhost,
            $socket_client->peerport;

        $socket_client->send(1);

        last;
    }

    printf "[%s] Fechando conexão com o cliente %s:%s.\n",
        strftime ('%D %T', localtime), $socket_client->peerhost,
        $socket_client->peerport;

    $socket_client->close();

    if (!$proxy_mode) { $p->close(); }

}
