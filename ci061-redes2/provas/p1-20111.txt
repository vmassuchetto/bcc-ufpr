Vinicius Massuchetto

CI061 - Redes de Computadores II
Primeira Prova do Primeiro Semestre de 2011

 1. As camadas 5 e 6 do modelo OSI (Sessão e Apresentação) não estão presentes
 no modelo TCP/IP da Internet. Responda:

    a) O que aconteceu com a funcionalidade destas camadas no modelo TCP/IP?



    b) Existe alguma vantagem em implementar estas funções em camadas
    exclusivas, como o modelo ISO/OSI?



 2. Os endereços IP são organizados em classes, sendo a classe D de multicast.

    a) O que é multicast?

    É a entrega de uma mensagem para vários destinatários a partir de uma única
    transmissão.

    b) Cite 1 (uma) situação em que o multicast melhora a eficiência da
    transmissão na Internet.

    Aplicações para transferência multimídia em tempo real podem fazer uso
    desta tecnologia para suportar múltiplos hosts de maneira menos custosa.

 3. Considere o endereço IP 192.34.82.201 e a máscara de subrede
 255.255.255.192. Responda:

    a) qual host está sendo endereçado?

    Endereço 192.34.82.201
             192 = 1100 0000
                   110 --> Classe C
                   NET-ID  = 192.34.82.0
                   HOST-ID =   0. 0. 0.201

    HOST-ID = 201   1100 1001
    Máscara = 192   1100 0000
                      -- ---- host    = 001001 = 9
                              subrede = 192.34.82.11000000
                                        192.34.82.192

    b) Quantas subredes internas a organização possui?

    Máscara de subrede:
        255.255.255.192
                    192 = 1100 0000
                          -- 2 bits = 4 subredes

 4. Com relação à fragmentação no protocolo IPv4 responda:

    a) os fragmentos podem ser re-fragmentados antes de chegar ao destino?

    Sim. Caso o fragmento encontre um enlace em que o MTU é menor do que seu
    tamanho, seus fragmentos serão re-fragmentados em pedaços menores.

    b) os fragmentos podem ser juntados no pacote original antes de chegar ao
    destino? Por que?

    Não. Somente o host e o destinatário reconstroem os pacotes a partir dos
    fragmentos, e isso se dá porque:

    * A retransmissão de um pacote por um enlace demoraria muito tempo se ele
      esperasse todos os demais para poder reconstruir e retransmitir.

    * Em alguns casos isto sequer seria possível, já que é possível que um
      fragmento tome uma outra rota para chegar no destinatário.

    * Mesmo se todos os pacotes chegassem no enlace, é muito provável que um
      próximo enlace também necessite fragmentá-los. Reconstruir o pacote para
      logo tê-lo fragmentado novamente é contraproducente.

 5. O campo TTL do pacote IPv4 permite determinar um tempo máximo de vida de um
 pacote IP, antes que seja removido da rede. Explique como este campo é
 implementado.

    Este campo é inicializado com um valor inteiro, e cada roteador em que o
    pacote passa é feita uma operação de decremento. Quando o valor chegar a
    zero o pacote é descartado.

 6. O CIDR permitiu que o IPv4 continuasse sendo usado na Internet por ter
 resolvido dois problemas. Explique.

    No esquema original do esquema de endereçamento IP, cada rede física possui
    um endereço de rede (NET-ID), e cada host em uma rede possui um endereço de
    rede como prefixo de um endereço de host (HOST-ID) individual.

    O crescimento de redes de escritório causou um inchaço da capacidade de
    endereços de rede, o que não era visível na fase de projeto dos protocolos
    TCP/IP devido ao alto preço e um número moderado de roteadores nas redes
    pelo mundo.

    Essa grande alocação de endereços de rede causam os principais problemas:

    * Overhead administrativo de gerenciamento destes endereços;
    * Grande tamanho das tabelas de roteamento de endereços nos roteadores;
    * Falta de endereços disponíveis para serem fornecidos à novas redes.

    As soluções propostas para estes problemas foram, basicamente:

    * Multiplexação dos endereços via CIDR, que foi a solução tomada de imediato
      e que conteve o colapso da internet no início dos anos 90;
    * Elaboração de um novo protocolo de endereçamento, o que no caso é o IPv6.

    A técnica consiste basicamente em atribuir um endereço físico via
    multiplexação a vários hosts de uma rede local, como estes hosts estivessem
    conectados de forma direta à WAN. O router desta rede demultiplexa os
    quadros que nele chegam a fim de destiná-lo ao host correto.

    O CIDR apresenta-se no formato XXX.XXX.XXX.XXX/YY, em que YY é o prefixo
    CIDR, variando de 13 a 27. Este prefixo indica quantos bits estão sendo
    usados para endereçamento de rede (NET-ID), o que também define quantos bits
    estão sendo usados para o endereçamento local (HOST-ID), que varia de 5 a
    19.

    Assim, o número de hosts possíveis segundo o prefixo CIDR é exemplificado:

    Endereço        Prefixo      Bits HOST-ID       Número de hosts
    XXX.XXX.XXX.XXX/27           5 bits             32
                   /27           6 bits             64
                   /25           7 bits             128
                   ...           ...                ...
                   /13           19 bits            524.288


 7. Considere o código formado por mensagens de 3 bits, e checksum
 correspondente a 1 único bit de paridade.

    a) Mostre todas as palavras deste código.

    Palavras    Paridade    Código
    000         0           0000
    001         1           0011
    010         1           0101
    011         0           0110
    100         1           1001
    101         0           1010
    110         0           1100
    111         1           1111

    b) Qual a distância do código? Explique sua resposta.

    4. Pois nenhuma palavra tem mais do que 4 bits diferentes uma da outra.

 8. O popular comando ping, implementado em virtualmente todos os sistemas
 operacionais, é uma implementação do ICMP Echo-Request-Reply. Exatamente o que
 significa uma expressão bem sucedida do ping?

    Trata-se do comando `ping`, que consulta a interface requerida pelo
    protocolo IP, logo não chega a consultar as camadas superiores.

    O sucesso na execução deste comando quer dizer que a interface de rede
    da máquina em questão está apropriadamente conectada e preparada para
    receber dados. Contanto, não quer dizer que os protocolos e aplicações
    superiores estão corretamente configurados e responderão as suas
    requisições com sucesso.

