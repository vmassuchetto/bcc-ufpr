#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define WORDSIZE 5
#define BUFFSIZE 256

typedef struct _node *node;
struct _node {
	int bit;
    int key;
	node left, right;
};

/**
 * @var buffer  Varíavel global a ser usada como buffer de transferência
 *              na transformação entre as árvores, e também na memorização
 *             dos nós visitados para as funções de busca */
extern int *buffer;

/**
 * @var count   Variáveis auxiliares para contagem geral. Serve
 *      count2  principalmente para nós e marcação do número de
 *              itens do buffer.
 */
extern int  count;
extern int  count2;

int           apl_bin2dec     (int bin);
unsigned int *apl_dec2bin     (int dec);
node          apl_patr2patz   (node p);
int           apl_digit       (int key, int bit);
void          apl_free_buffer (void);

void patricia_print       (node p, int bit);
void patricia_search      (node p, int key, int bit);
node patricia_search_node (node p, int key, int bit, node parent);
node patricia_insert      (node p, int key);
node patricia_insert_node (node p, int key, int bit, node parent);
void patricia_in_order    (node p, int bit);
void patricia_cost        (node p, int bit);
node patricia_from_patriz (node p);

node node_create (int key, int bit);
