#include "libPatriz.h"

/**
 * @brief Cria um nó a ser utilizado para as árvores
 *        Para a Patriz, p->key < 0 são nós internos
 */
node node_create (int key, int bit) {
	node p = malloc (sizeof(struct _node));
	p->left = NULL;
	p->right = NULL;
	p->bit = bit;
	p->key = key;
	return p;
}

/**
 * @brief Insere nó em árvore Patricia
 */
node patricia_insert (node p, int key) {
	int i;

   /**
    * @note Cria a árvore caso ela não exista admitindo o primeiro
    *       bit mais à esquerda da representação binária
    */
	if (!p) {
		for (i = 1; i < WORDSIZE && apl_digit (key, i); i++);

		if (i >= WORDSIZE)
			i = 0;

        p = patricia_insert_node (p, key, i-1, 0);
		return p;
	}

    /**
     * @note Busca o nó a ser usado como referência para a inserção
     */
    node q = patricia_search_node (p, key, -1, NULL);

    /**
     * @note Verifica o bit mais à esquerda em que o nó de
     *       referência e a chave a ser inserida diferem
     */
    for (i = 0; i < WORDSIZE &&
        (apl_digit (key, i) == apl_digit (q->key, i)); i++);

	if (i == q->bit)
		return patricia_insert_node (p, key, i+1, NULL);
	else
		return patricia_insert_node (p, key, i, NULL);
}

/**
 * @brief Insere um nó recursivamente em uma árvore Patricia
 */
node patricia_insert_node (node p, int key, int bit, node parent) {

    /**
     * @note Verifica se deve criar um novo ou
     *       prosseguir navegando
     */
    int new = 0;
    if (!p || p->bit >= bit)
        new = 1;
    else if (parent)
        if (p->bit <= parent->bit)
            new = 1;

    if (new) {
		node n = node_create (key, bit);
		if (apl_digit(key, bit) == 0) {
			n->right = p;
			n->left = n;
        } else {
			n->right = n;
			n->left = p;
		}
        return n;

    /**
     * @note Continua navegando para os nós ancestrais
     */
	} else {
		if (apl_digit(key, p->bit) == 0)
			p->left = patricia_insert_node (p->left, key, bit, p);
		else
            p->right = patricia_insert_node (p->right, key, bit, p);
		return p;
	}

}

/**
 * @brief Busca um nó em uma árvore Patricia
 */
void patricia_search (node p, int key, int bit) {

	if (!p) {
        printf("nao achou");
        return;
    }

    if (p->bit <= bit && p->key == key) {
        int i;
        for (i = 0; i < count; i++)
            printf("%d, ", buffer[i]);

        printf("{%d}", p->key);
        return;

    } else if (p->bit <= bit) {
        printf ("nao achou");
        return;
    }

    /**
     * @note Guarda os nós visitados em um buffer para
     *       mostrá-los caso o nó desejado seja encontrado.
     */
    buffer[count++] = p->key;

    if (apl_digit(key, p->bit) == 0)
        patricia_search (p->left, key, p->bit);
    else
        patricia_search (p->right, key, p->bit);

}

/**
 * @brief Realiza uma bsca recursiva nos nós de uma Patricia
 */
node patricia_search_node (node p, int key, int bit, node parent) {

    if (!p)
		return parent;

    /**
     * @note Se um ponteiro para um nó acima foi seguido
     */
    if (p->bit <= bit )
		return p;

	if (apl_digit (key, p->bit) == 0)
		return patricia_search_node (p->left, key, p->bit, p);
	else
		return patricia_search_node (p->right, key, p->bit, p);
}

/**
 * @brief Imprime os nós de uma árvore Patricia
 */
void patricia_print (node p, int bit) {
    if (!p) {
		printf ("()");
		return;
	}

	if (p->bit <= bit) {
        printf( "{%d}", p->key );
        return;
	}

    printf ("(");
    printf ("%d[%d] ", p->key, p->bit);
	patricia_print (p->left, p->bit);
	patricia_print (p->right, p->bit);
    printf(")");
}

/**
 * @brief Percorre uma árvore Patriz em ordem e preenche o buffer
 *        para ser usado na transformação de árvores
 */
void patriz_in_order (node p, int bit) {

    if (!p)
        return;

    patriz_in_order (p->left, p->bit);
    if (p->key > 0)
        buffer[count++] = p->key;
    patriz_in_order (p->right, p->bit);
}

/**
 * @brief Constróia uma árvore Patricia de uma Patriz
 */
node patricia_from_patriz (node patriz) {

    apl_free_buffer ();
    patriz_in_order (patriz, 0);

    int i;
    node patricia = NULL;
    for (i = 0; i < count; i++)
        patricia = patricia_insert (patricia, buffer[i]);

    return patricia;
}

/**
 * @brief Conta o número de nós visitados e soma
 *        os tamanhos da estrutura que os nós são compostos
 */
void patricia_cost (node p, int bit) {

    if (!p)
        return;

    count += sizeof (struct _node);
    count2++;

    if (p->bit <= bit)
        return;

    patricia_cost (p->left, p->bit);
    patricia_cost (p->right, p->bit);

}
