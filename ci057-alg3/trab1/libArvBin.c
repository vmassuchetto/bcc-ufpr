#include "libArvBin.h"

/* Funções fornecidas */

Apontador criaNo( Registro r, Apontador esq, Apontador dir ){
  Apontador p;

  p = (Apontador) malloc( sizeof(No) );
  p->Reg = r;
  p->Esq = esq;
  p->Dir = dir;
  p->AltEsq = p->AltDir = 0;
  return p;
}

void Inicializa(Apontador *raiz){
  Registro r;

  r.Chave = 0;
  nodoNull = criaNo( r, (Apontador)0, (Apontador)0 );
  *raiz = nodoNull;
  return;
}

void imprimeArv(Apontador p){
  int bal;

  if( p == nodoNull )
    printf("()");
  else {
    printf("(");
    imprimeArv( noEsq(p) );
    bal = p->AltEsq - p->AltDir;
    printf("% ld[%d] ", p->Reg.Chave, bal);
    imprimeArv( noDir(p) );
    printf(")");
  }
  return;
}

Apontador Pesquisa(TipoChave k, Apontador p){
  if (p == nodoNull || k == p->Reg.Chave)
    return p;
  if (k < p->Reg.Chave)
    return Pesquisa(k, p->Esq);
  else
    return Pesquisa(k, p->Dir);
}

void rotacaoDireita( Apontador *p ){
  Apontador fe, netoDir;

  fe = (*p)->Esq;
  netoDir = fe->Dir;
  (*p)->Esq = netoDir;
  fe->Dir = *p;
  *p = fe;
  return;
}

void rotacaoEsquerda( Apontador *p ){
  Apontador fd, netoEsq;

  fd = (*p)->Dir;
  netoEsq = fd->Esq;
  (*p)->Dir = netoEsq;
  fd->Esq = *p;
  *p = fd;
  return;
}

Apontador nodoNull = NULL;

/**
 * @brief Recupera o filho esquerdo de um nó
 * @param p Nó a ser pesquisado
 * @return Nó filho
 */
Apontador noEsq (Apontador p) {
	if (p != nodoNull)
		return p->Esq;

	return nodoNull;
}

/**
 * @brief Recupera o filho direito de um nó
 * @param p Nó a ser pesquisado
 * @return Nó filho
 */
Apontador noDir (Apontador p) {
	if (p != nodoNull)
		return p->Dir;

	return nodoNull;
}

/**
 * @brief Calcula a altura de um ramo de árvore
 * @param p Nó que constitui o início do ramo
 * @return O valor da maior sub árvore de p
 */
int noAltMax (Apontador p) {

	if (p == nodoNull)
		return 0;

	if (p->AltDir > p->AltEsq)
		return p->AltDir;
	else
		return p->AltEsq;

}

/**
 * @brief Rotaciona um nó para a direita, alterando as alturas de suas
 *        sub árvores esquerda e direita
 * @param p Nó a ser rotacionado
 */
void RotDirComAlt (Apontador *p) {

	Apontador fe = (*p)->Esq;
	Apontador netoDir = fe->Dir;

	/** @note Altera as alturas de acordo com os valores das
	 *        alturas do neto e filho a ser deslocado.
	 */
	if (netoDir != nodoNull)
		(*p)->AltEsq = noAltMax (netoDir) + 1;
	else
		(*p)->AltEsq = 0;
	fe->AltDir = noAltMax (*p) + 1;

	(*p)->Esq = netoDir;
	fe->Dir = *p;
	*p = fe;

	return;
}

/**
 * @brief Rotaciona um nó para a esquerda, alterando as alturas de suas
 *        sub árvores esquerda e direita
 * @param p Nó a ser rotacionado
 */
void RotEsqComAlt (Apontador *p) {

	Apontador fd = (*p)->Dir;
	Apontador netoEsq = fd->Esq;

	/** @note Altera as alturas de acordo com os valores das
	 *        alturas do neto e filho a ser deslocado.
	 */
	if (netoEsq != nodoNull)
		(*p)->AltDir = noAltMax (netoEsq) + 1;
	else
		(*p)->AltDir = 0;
	fd->AltEsq = noAltMax (*p) + 1;

	(*p)->Dir = netoEsq;
	fd->Esq = *p;
	*p = fd;

	return;
}

/**
 * @brief Insere o nó em uma árvore
 * @param r Registro a ser inserido
 * @param raiz Raiz da árvore a ter o nó inserido
 * @return 1 para sucesso, -1 para nó já existente
 */
int Insere (Registro r, Apontador *raiz) {

	if (*raiz == nodoNull) {
		*raiz = criaNo (r, nodoNull, nodoNull);
		return OK;
	}

	if (r.Chave < (*raiz)->Reg.Chave) {
		Insere (r, &(*raiz)->Esq);
		RotDirComAlt (raiz);

	} else if (r.Chave > (*raiz)->Reg.Chave) {
		Insere (r, &(*raiz)->Dir);
		RotEsqComAlt (raiz);

	} else if (r.Chave == (*raiz)->Reg.Chave) {
		return ERRO;
	}

	return OK;
}

/**
 * @brief Realiza a busca por um registro na árvore, imprimindo os nós
 *        visitados separados por vírgula
 * @param b Registro a ser buscado
 * @param p O último nó visitado
 * @return Nulo se não encontradoNó encontrado
 */
Apontador Busca (Registro b, Apontador p) {

	if (p != nodoNull)
		printf("%ld", p->Reg.Chave);

	if (p == nodoNull || b.Chave == p->Reg.Chave)
		return p;

	/**
	 *  @note Somente imprime a vírgula se o próxima nó
	 *        a ser visitado não é nulo
	 */
	if ((b.Chave < p->Reg.Chave && p->Esq != nodoNull) ||
	    (b.Chave > p->Reg.Chave && p->Dir != nodoNull))
		printf(",");

	if (b.Chave < p->Reg.Chave)
		return Busca (b, p->Esq);
	else
		return Busca (b, p->Dir);

}

/**
 * @brief Busca pelo sucessor de um nó
 * @param p Nó referência
 * @return Sucessor de p
 */
Apontador noSuc (Apontador p) {

	if (noDir (p))
		p = noDir (p);

	while (noEsq (p) != nodoNull)
		p = noEsq (p);

	return p;

}

/**
 * @brief Efetua a remoção de um registro de uma árvore
 * @param x Registro a ser removido
 * @param raiz Raiz da árvore a ter o registro procurado
 * @param p Nós visitados em busca pelo registro x
 * @param pai Pai de p utilizado para o caso 4 de remoção
 * @return 1 para sucesso, -1 para falha
 */
int Remove (Registro x, Apontador raiz, Apontador *p, Apontador *pai) {

	if ((*p) == nodoNull)
		return ERRO;

	if (x.Chave == (*p)->Reg.Chave) {

		/** @note Caso 1: Nó folha */
		if (noEsq (*p) == nodoNull && noDir (*p) == nodoNull) {
			*p = nodoNull;

		/** @note Caso 2: Somente com filho esquerdo */
		} else if (noEsq (*p) != nodoNull && noDir (*p) == nodoNull) {
			*p = (*p)->Esq;
			(*p)->Esq = nodoNull;

		/** @note Caso 3: Somente com filho direito */
		} else if (noEsq (*p) == nodoNull && noDir (*p) != nodoNull) {
			*p = (*p)->Dir;
			(*p)->Dir = nodoNull;

		/** @note Caso 4: Com dois filhos */
		} else if (noEsq (*p) != nodoNull && noDir (*p) != nodoNull) {

			Apontador q = noSuc (*p);
			(*p)->Reg = q->Reg;

			/** @note Caso seja a raiz */
			if (!(*pai)->Reg.Chave)
				pai = p;

			/**
			 *  @note Desvincula o nó sucessor da árvore, retirando
			 *        uma unidade de altura da sub-árvore que ele pertence.
			 **/
			if ((*pai)->Dir == q) {
				(*pai)->AltDir -= 1;
				(*pai)->Dir = nodoNull;
			} else if ((*pai)->Esq == q) {
				(*pai)->AltEsq -= 1;
				(*pai)->Esq = nodoNull;
			}

			q = nodoNull;
		}

		return OK;

	}

	else if (x.Chave < (*p)->Reg.Chave) {
		(*p)->AltEsq -= 1;
		Remove (x, raiz, &(*p)->Esq, p);

	} else if (x.Chave > (*p)->Reg.Chave) {
		(*p)->AltDir -= 1;
		Remove (x, raiz, &(*p)->Dir, p);
	}


	return ERRO;
}
