#include <stdio.h>
#include <stdlib.h>

#include "libPatricia.h"

int patr_digito (int dec, int bit) {
	int i = 0, bin[N_BITS];

	while (i <= N_BITS) {
		bin[i] = dec % 2;
		// Se for o bit desejado
		if (N_BITS - i == bit)
			return bin[i];
		dec /= 2;
		i++;
	}

	// Imprime o binário correspondente
	if (bit == -1)
		for (i = 0; i <= N_BITS; i++)
			printf("%d", bin[i]);

	return 0;
}

int patr_no_valido (patr_no no) {
	if (no != NULL)
		if (no->valor >= 0)
			return 1;
	return 0;
}

void patr_imprimir (patr_no no) {
	if (patr_no_valido(no)) {
		printf ("(");

		if (patr_no_valido(no->esq))
			if (no->esq->bit < no->bit)
				patr_imprimir (no->esq);

		if (patr_no_valido(no)) {
			printf ("(%d[%d]", no->valor, no->bit);

			if (patr_no_valido(no->esq))
				printf(" (%d)", no->esq->valor);
			else
				printf(" ()");

			if (patr_no_valido(no->dir))
				printf(" (%d)", no->dir->valor);
			else
				printf(" ()");

			printf (")");
		}

		if (patr_no_valido(no->dir))
			if (no->dir->bit < no->bit)
				patr_imprimir (no->dir);

		printf (")");
	}
}

patr_no patr_criar (int valor, int bit) {
	patr_no no = (patr_no) malloc (sizeof(struct _patr_no));
	no->valor = valor;
	no->bit = bit;
	no->esq = no->dir = NULL;
	return no;
}

patr_no patr_prefixo (patr_no no, patr_no pai, int valor, int bit) {

	// Retorna o pai ao chegar em uma extremidade
	if (!patr_no_valido(no))
		return pai;

	// Termina a busca se alcançar um nó superior
	if (no->bit <= bit)
		return no;

	// Segue navegando
	else
		if (patr_digito (valor, no->bit) == 0)
			return patr_prefixo (no->esq, no, valor, no->bit);
		else
			return patr_prefixo (no->dir, no, valor, no->bit);
}

patr_no patr_dividir (patr_no no, patr_no pai, int valor, int bit) {

	// Cria o nó
	if (no->bit >= bit || no->bit <= pai->bit) {
		patr_no n = patr_criar (valor, bit);

		if (patr_digito (valor, bit) == 0) {
			pai->esq = n;
			n->dir = no;
		} else {
			n->esq = no;
			pai->dir = n;
		}
		return n;
	}

	// Segue navegando
	if (patr_digito(valor, no->bit) == 0)
		no->esq = patr_dividir (no->esq, no, valor, bit);
	else
		no->dir = patr_dividir (no->dir, no, valor, bit);
	return no;
}

void patr_buscar (patr_no no, int valor) {

	if (no) {
		if (no->valor >= 0) {
			printf("%d\n", no->valor);
			return;

		} else {

			if (patr_digito(valor, no->bit) == 0)
				patr_buscar (no->esq, valor);
			else
				patr_buscar (no->dir, valor);
		}
	}
}

patr_no patr_inserir (patr_no no, int valor) {

	int i;

	// Cria o primeiro nó da árvore
	if (no == NULL) {
		for (i = 0; i <= N_BITS; i++)
			if (patr_digito (valor, i) == 1)
				break;

		if (i >= N_BITS)
			i = 0;

		return patr_criar (valor, i);
	}

	patr_no pai = patr_criar (-2, -2);
	pai = patr_prefixo (no, pai, valor, -1);
	for (i = 0; i <= N_BITS; i++)
		if (patr_digito(valor, i) != patr_digito(pai->valor, i))
			break;

	int diff_bit = N_BITS-i;

	patr_no n = patr_criar (valor, diff_bit);
	int ini_bit = pai->bit;

	while (1) {
		if (pai->bit >= diff_bit || pai->bit <= ini_bit) {
			if (patr_digito (valor, diff_bit) == 0) {
				pai->esq = n;
				n->dir = pai;
			} else {
				n->esq = pai;
				pai->dir = n;
			}
			break;
		}

		// Segue navegando
		if (patr_digito(valor, no->bit) == 0)
			pai = pai->esq;
		else
			pai = pai->dir;
	}

	return patr_dividir (no, pai, valor, diff_bit);
}
