
Questão:
----------------------------------------

  Com base nas árvores Patricia geradas no item (1) e (2), compare as árvores
  resultantes e explique por que as estruturas geradas são identicas ou
  diferentes. Sua justificativa é valida para qualquer Árvore Patricia gerada
  a partir de um percurso em pré-ordem de uma trie?

----------------------------------------

  Para as árvores Patricia a disposição das chaves independe da ordem em que
  são inseridas, uma vez que há um rearranjo destas chaves de acordo com o
  nível em que se encontram na árvore - o que determina qual bit da chave
  armazenada que o nó representa no percurso.

  No entanto, não há como afirmar que estruturas construídas através de
  diferentes ordens de inserção são completamente idênticas, pois um fator que
  determina o fim da busca é o retorno a um nível acima da árvore, o que faz
  com que os ponteiros para nós antecessores possam ligar nós diferentes
  dependendo da ordem de inserção.
