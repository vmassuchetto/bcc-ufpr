#include <stdio.h>
#include <stdlib.h>

#include "libTrie.h"
#include "libPatricia.h"

void pre_ordem (trie_no trie, patr_no patr) {

	if (trie == NULL)
		return;

	if (trie_no_valido(trie))
		patr_inserir (patr, trie->valor);

	pre_ordem (trie->esq, patr);
	pre_ordem (trie->dir, patr);
}

int main (int argc, char **argv) {

	char operacao;
	int valor;

	trie_no trie_raiz = NULL;
	patr_no patr_raiz = NULL;

	while (scanf("%c %d", &operacao, &valor) != EOF) {

		switch (operacao) {
			case 'i' :
				printf("i ");
				trie_digito (valor, -1);
				printf("\n");

				trie_raiz = trie_inserir (trie_raiz, valor, 0);
				patr_raiz = patr_inserir (patr_raiz, valor);

				printf("trie ");
				trie_imprimir (trie_raiz);
				printf("\n");

				printf("patricia ");
				patr_imprimir (patr_raiz);
				printf("\n");
			break;

			case 'b' :
				printf("b %d\n", valor);
				trie_buscar (trie_raiz, valor, 0);
				patr_buscar (patr_raiz, valor);
			break;
		}

	}

	patr_raiz = NULL;
	pre_ordem (trie_raiz, patr_raiz);
	return 0;
}
