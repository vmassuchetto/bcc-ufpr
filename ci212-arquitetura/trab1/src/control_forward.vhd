library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity control_forward is
    port(
        opcode:          in  std_logic_vector (5 downto 0);
        MEM_RegWrite:    in  std_logic;
        WB_RegWrite:     in  std_logic;
        EX_RegRs:        in  std_logic_vector (4 downto 0);
        EX_RegRt:        in  std_logic_vector (4 downto 0);
        MEM_RegRd:       in  std_logic_vector (4 downto 0);
        WB_RegRd:        in  std_logic_vector (4 downto 0);
        ALUSrcA:         out std_logic_vector (1 downto 0);
        ALUSrcB:         out std_logic_vector (1 downto 0)
    );
end control_forward;

architecture arq_control_forward of control_forward is

begin

    process(opcode)
    begin

        --! ALU A
        --! adiantamento de EX/MEM
        if MEM_RegWrite = '1' and
            MEM_RegRd /= "00000" and
            MEM_RegRd = EX_RegRs then
            ALUSrcA <= "01";
        --! adiantamento de MEM/WB
        elsif WB_RegWrite = '1' and
            WB_RegRd /= "00000" and
            WB_RegRd = EX_RegRs and
            MEM_RegRd /= EX_RegRs then
            ALUSrcA <= "10";
        --! sem adiantamento
        else
            ALUSrcA <= "00";
        end if;

        --! ALU B
        --! adiantamento de EX/MEM
        if MEM_RegWrite = '1' and
            MEM_RegRd /= "00000" and
            MEM_RegRd = EX_RegRt then
            ALUSrcB <= "01";
        --! adiantamento de MEM/WB
        elsif WB_RegWrite = '1' and
            WB_RegRd /= "00000" and
            WB_RegRd = EX_RegRt and
            MEM_RegRd /= EX_RegRt then
            ALUSrcB <= "10";
        --! sem adiantamento
        else
            ALUSrcB <= "00";
        end if;

    end process;
    
end arq_control_forward;
