--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Registrador genérico sensível à borda de subida do clock
-- com possibilidade de inicialização de valor
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity reg32_ce is
    generic( 
        width : integer := 32
    );

    port(
        clock, reset, ce: in std_logic;
        in_signal: in  std_logic_vector(width-1 downto 0);
        out_signal: out std_logic_vector(width-1 downto 0)
    );
end reg32_ce;

architecture arq_reg32_ce of reg32_ce is 
begin

    process(clock, reset)
    begin
        if reset = '1' then
            out_signal <= (others => '0');
        elsif clock'event and clock = '1' then
            if ce = '1' then
                out_signal <= in_signal;
            end if;
        end if;
    end process;
        
end arq_reg32_ce;
