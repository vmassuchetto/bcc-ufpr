library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.p_MI0.all;



entity rom32 is
	port (
		address: in reg32;
		data_out: out reg32
	);
end rom32;

architecture arq_rom32 of rom32 is

	signal mem_offset: std_logic_vector(5 downto 0);
	signal address_select: std_logic;
	

begin
	mem_offset <= address(7 downto 2);
        
	add_sel: process(address)
	begin
		if address(31 downto 8) = x"000000" then 	
			address_select <= '1';
		else
			address_select <= '0';
		end if;
	end process;

	access_rom: process(address_select, mem_offset)
	begin
		if address_select = '1' then
			case mem_offset is
				when 	"000000" => data_out <= "100101" & "00100" & "00010" & x"0008"; -- lw $2, 8($4)
				when 	"000001" => data_out <= "000100" & "00000" & "00000" & x"fffe"; -- beq $0, $0, -2
				when 	"000010" => data_out <= "000011" & "00000000000000000000000101"; -- jal 5
				when 	"000011" => data_out <= "000010" & "00000000000000000000000010"; -- j 2
				when 	"000100" => data_out <= "000000" & "00001" & "00010" & "00101" & "00000" & "100000"; -- add $5, $1, $2
				when 	"000101" => data_out <= "001000" & "00010" & "00100" & x"fffe"; -- addi $4, $2, -2
				when 	others  => data_out <= (others => 'X');
			end case;
    		end if;
  	end process; 

end arq_rom32;
