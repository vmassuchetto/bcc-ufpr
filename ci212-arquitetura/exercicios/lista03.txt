
    Lista 3: 1, 2, 3, 4, 7, 9, 10 e 11

1.  Considere a execu��o do trecho de c�digo abaixo no pipeline de 5
    est�gios, sem adiantamento.

    loop:
        ld.d  f0,0(r4)
        ld.d  f2,0(r6)
        mul.d f4,f2,f0
        add.d f6,f4,f8
        st.d  f4,0(r8)
        st.d  f6,8(r8)
        addi  r4,r4,8
        addi  r6,r6,8
        addi  r8,r8,16
        bne   r8,r9,loop

    a)  Modifique o c�digo para que ele execute corretamente num processador
        sem l�gica de bloqueios (stalls).
        
        loop:
            ld.d  f0,0(r4)
            ld.d  f2,0(r6)
            addi  r4,r4,8
            mul.d f4,f2,f0    # f2 <= adiantamento MEM | f0 <= simult�neo WB
            add.d f6,f4,f8    # f4 <= adiantamento EX
            st.d  f4,0(r8)    # f4 <= adiantamento MEM
            st.d  f6,8(r8)    # f6 <= adiantamento MEM
            addi  r6,r6,8
            addi  r8,r8,16
            bne   r8,r9,loop  # r8 <= adiantamento EX

    b)  Re-escreva o c�digo para que ele execute com um m�nimo de bolhas
        (pipeline sem adiantamento e nem bloqueios).
    
        loop:
            ld.d  f0,0(r4)
            ld.d  f2,0(r6)
            addi  r4,r4,8
            addi  r6,r6,8
            mul.d f4,f2,f0    # f2 <= simult�neo WB
            --    -- --
            --    -- --       # disponibiliza f4
            add.d f6,f4,f8    # f4 <= simult�neo WB
            st.d  f4,0(r8)
            --    -- --       # disponibiliza f6
            st.d  f6,8(r8)    # f6 <= simult�neo WB
            addi  r8,r8,16
            --    -- --
            --    -- --       # disponibiliza r8
            bne   r8,r9,loop  # r8 <= simult�neo WB

    c)  Qual o ganho de desempenho da resposta de (b) com rela��o �
        resposta de (a)?
        
        Enquanto (a) leva 14 ciclos para executar uma itera��o, (b) leva 19, 
        fazendo com que (a) tenha um desempenho de 1.35 de (b).

2.  Considere a execu��o do trecho de programa acima no pipeline original de
    cinco est�gios (com bloqueios e adiantamento). Quantos ciclos ser�o 
    necess�rios para executar este c�digo?  Desenhe um diagrama que mostra 
    as depend�ncias, e outro diagrama que mostra como o c�digo ser� 
    executado (mostrando as bolhas e/ou adiantamento).

    add r5,r6,r7
    lw  r6,100(r2)
    sub r7,r6,r8
    sw  r7,200(r2)

    add r5,r6,r7
    lw  r6,100(r2)
    --  -- --       # aguarda r6 MEM
    sub r7,r6,r8    # r6 <= adiantamento MEM
    sw  r7,200(r2)  # r7 <= adiantamento EX
    
    add r5,r6,r7    # IF ID EX ME WB
    lw  r6,100(r2)  #    IF ID EX ME WB
    --  -- --       #       -- -- -- -- --
    sub r7,r6,r8    #          IF ID EX ME WB
    sw  r7,200(r2)  #             IF ID EX ME WB
                    # 9 ciclos
    
3.  Considere o seguinte programa, ao ser executado no processador do
    Cap�tulo 6.

        mov   $5,$0
    SUM:
        lw    $10,0($20)  # $20 cont�m 0x40000000
        add   $5,$5,$10
        addiu $20,$20,4
        addiu $21,$21,-4  # $21 cont�m 800
        bne   $21,$0,SUM

    a)  Considere a CPU segmentada mais simples, sem nenhuma forma de
        adiantamento e nem bloqueios. Acrescente ao c�digo o que for 
        necess�rio para garantir a execu��o correta deste programa. Qual o 
        n�mero total de ciclos necess�rios para computar a redu��o do vetor 
        apontado por $20?

            addi  $5,$0,$0
        SUM:                  # 200 itera��es
            lw    $10,0($20)
            --    -- --
            --    -- --       # disponibiliza $10
            add   $5,$5,$10   # $10 <= simult�neo WB
            addiu $20,$20,4
            addiu $21,$21,-4
            --    -- --       # disponibiliza $21
            bne   $21,$0,SUM  # $21 <= simult�neo WB

                              # 12 ciclos de itera��o
                              # 200*12 = 2400 ciclos

    b)  Otimize o c�digo para reduzir o n�mero de ciclos.  Qual o novo n�mero
        total de ciclos?  Qual o ganho?

            addi  $5,$0,$0
        SUM:                  # 200 itera��es
            lw    $10,0($20)
            addiu $21,$21,-4
            addiu $20,$20,4
            add   $5,$5,$10   # $10 <= simult�neo WB
            bne   $21,$0,SUM  # $21 <= simult�neo WB

                              # 9 ciclos
                              # 200 * 9 = 1800 ciclos
                              # 2400 / 1800 = 1.33
    
    c)  Repita (a) considerando adiantamento.

            addi  $5,$0,$0
        SUM:                  # 200 itera��es
            lw    $10,0($20)
            --    -- --       # disponibiliza $10
            add   $5,$5,$10   # $10 <= adiantamento MEM
            addiu $20,$20,4
            addiu $21,$21,-4
            bne   $21,$0,SUM  # $21 <= adiantamento EX
                              # 10 ciclos
                              # 200 * 10 = 2000 ciclos
    
    d)  Qual o ganho considerando as respostas de (b) e (c)?

        2000/1800 = 1.11

4.  Projete um circuito eficiente e eficaz de previs�o de desvios, com 
    capacidade para conter informa��es sobre 1024 instru��es de desvio.
    
5.  a)  Enumere e descreva todas as causas para exce��es no processador
        segmentado do Cap6.  Desenhe um diagrama simplificado do processador 
        e mostre os locais onde as exce��es ocorrem e onde s�o detectadas.

    b)  Liste a seq��ncia de eventos associados ao tratamento de uma exce��o,
        considerando todos os efeitos no processador.

6.  Enuncie a Lei de Amdahl.  Como esta lei influencia no projeto de
    processadores com:
    
    a) ciclo longo;
    
    b) multiciclos;
    
    c) segmentados?

7.  Circuito de Adiantamento:

    i   Escreva as equa��es l�gicas que descrevem o controle dos dois 
        seletores nas entradas A e B da ULA.

        --
        Depend�ncia de Dados em EX
        
        Condi��es:
        1.  H� escrita eno banco de registradores para EX/MEM;
        2.  N�o trata-se de uma instru��o nop para EX/MEM;
        3.  O registrador que acaba de ser decodificado (ID/EX) � o mesmo
            que recebe o valor calculado pela ALU (EX/MEM).
        
        Resultado:
        *.  Adianta-se o valor de EX/MEM.RegRd (sa�da da ALU)

        if (EX/MEM.RegWrite &&
            EX/MEM.RegRd != 0 &&
            EX/MEM.RegRd == ID/EX.RegRx)
            ForwardX = 10

        Exemplo:
            add $1, $1, $2   # EX/MEM.RegRd (add $1 -- --)
            add $1, $1, $3   # ID/EX.RegRs  (add -- $1 --)

        --
        Depend�ncia de Dados em MEM

        Condi��es:
        1.  N�o trata-se de uma instru��o nop MEM/WB;
        2.  N�o h� escrita no banco de registradores para EX/MEM;
        3.  N�o trata-se de uma instru��o nop para EX/MEM;
        4.  H� escrita no banco de registradores para MEM/WB;
        5.  O registrador que acaba de ser decodificado (ID/EX) � diferente
            daquele que recebe o valor calculado pela ALU (EX/MEM);
        5.  O registrador que acaba de ser decodificado (ID/EX) � o mesmo
            que recebe o valor buscado na mem�ria (MEM/WB).
            
        Resultado:
        *.  Adianta-se o valor de MEM/WB (sa�da de leitura da mem�ria)

        if (MEM/WB.RegWrite &&
            MEM/WB.RegRd != 0 &&
            MEM/WB.RegRd == ID/EX.RegRx &&
            EX/MEM.RegRd != ID/EX.RegRx)
            ForwardX = 01

        Exemplo:
            add $1, $1, $2   # MEM/WB.RegRd (add $1 -- --)
            add $1, $1, $3   # EX/MEM.RegRd (add $1 -- --)
            add $1, $1, $3   # ID/EX.RegRs  (add -- $1 --)
    
    ii  Escreva as equa��es l�gicas que descrevem o controle do adiantamento 
        para o est�gio de mem�ria.

        1.  A opera��o � de leitura na mem�ria;
        2.  O registrador de destino da opera��o de leitura em EX � tamb�m
            usado na pr�xima instru��o em ID.
        
        if (IF/ID.MemRead &&
            ID/EX.RegRt == IF/ID.RegRs || ID/EX.RegRt == IF/ID.RegRt)
            stall de 1 ciclo
            
    iii Escreva as equa��es l�gicas que descrevem o controle do adiantamento 
        para desvios no est�gio de decodifica��o.

         1. A instru��o � de desvio
         2. Os registradores RegRs e RegRt antes de ID/EX s�o iguais
         3. O buffer de desvio � favor�vel

        // Fase ID
        if (ID.Opcode == I-Type &&
            ID.RegRs == ID.RegRt &&
            branch_buffer > 1)
            branch = true
            flush (IF/ID)
            stall
            PC = PC + 4 + ext(IMM) << 2

        // Fase EX
        if (ID.Opcode == I-Type &&
            branch == false
            branch_condition(RegRs,RegRt))
            if (++branch_buffer_count > 1)
            branch_buffer_count = 0
            flush (IF/ID)
            stall stall stall
            PC = PC + 4 + ext(IMM) << 2 

8.  Controle de Paradas

    i   Escreva as equa��es l�gicas que detectam depend�ncias de dados (ALU, 
        uso dos LDs, desvios).
    
    ii  Escreva as equa��es l�gicas que detectam depend�ncias de controle.

9.  Processadores superescalares: Discuta os problemas ocasionados pela 
    execu��o de duas instru��es por ciclo num processador segmentado de 
    cinco est�gios.

10. Mostre como implementar as instru��es abaixo no processador segmentado
    em cinco est�gios.  Sua implementa��o n�o pode introduzir nenhum risco 
    estrutural e deve ter um custo relativamente baixo.  Indique quaisquer 
    modifica��es necess�rias e mostre a tabela de sinais de controle ativos 
    em cada um dos est�gios de execu��o da instru��o.  SE n�o � poss�vel 
    implementar a instru��o sem a adi��o de riscos estruturais ou custo 
    elevado, justifique.  A v�rgula significa "execu��o simult�nea".  Note 
    que estas instru��es n�o fazem parte do conjunto de instru��es do MIPS 
    para os fins desta disciplina.
    
    lwpi rt, desl(rs) # rt <= M[rs+desl] , rs <- rs+desl
    swpi rt, desl(rs) # M[rs+desl] <- rt , rs <- rs+desl
    ldi rd,rs,rt      # rd <= M[rs+rt]  -- load indexado
    bal desl          # r31 <= PC+8 , PC <- (PC+4) + ext( desl<<2 )
    bgtzal rs,desl    # if(rs >= 0) { r31 <- PC+8 , PC <- (PC+4)+ext(desl<<2) }
    b desl            # PC <= (PC+4) + ext( desl<<2 ) -- branch always

    * Utilizando as figuras 6.27 e 6.36 do livro.
    * Considerando para o sinal de controle RegDst: 00 rt, 01 rd, 10 rs

    a)  lwpi rt, desl(rs)   # rt <- M[rs + desl]
                            # rs <- rs + desl

        Esta instru��o necessita de duas escritas em registradores, portanto 
        precisa ser realizada em um ciclo adicional. Embora o resultado a 
        ser escrito nos registradores seja obtido fundamentalmente atrav�s 
        da mesma soma (rs + desl), n�o � vantajoso tentar calcular este 
        valor com dois somadores, ou mesmo guard�-lo em seu primeiro c�lculo 
        para o pr�ximo ciclo, justamente porque � imposs�vel acessar a 
        mem�ria de registradores simultaneamente, o que leva a um ciclo 
        adicional de qualquer modo.

                            1      2      3      4      5     6
                        -----  -----  -----  -----  -----  ----
                         IF-1   ID-1   EX-1  MEM-1   WB-1
        IF   IF/ID.Wr       1      -      -      -      -
             PCWr           1      -      -      -      -
        EX   ALUSrc         -      -      1      -      -
             OpAlu        ---    ---    010    ---    ---
             RegDst        --      -     00     00     00
        MEM  MemRd          -      -      -      1      -
             MemWr          -      -      -      0      -
        WB   RegWr          -      -      -      -      1
             MemToReg       -      -      -      -      0

                                IF-2   ID-2   EX-2  MEM-2   WB-2
        IF   IF/ID.Wr              0      -      -      -      -
             PCWr                  0      -      -      -      -
        EX   ALUSrc                -      -      1      -      -
             OpAlu               ---    ---    010    ---    ---
             RegDst               --     --     10     10     10
        MEM  MemRd                 -      -      -      0      -
             MemWr                 -      -      -      0      -
        WB   RegWr          -      -      -      -      -      1
             MemToReg              -      -      -      -      1

        - IF/ID.Wr: No segundo ciclo IF a pr�xima instru��o n�o � buscada, 
                    ao inv�s disso, os sinais para o rec�lculo da soma e
                    grava��o do valor em outro registrador s�o ativados.


    b)  swpi rt, desl(rs)   # M[rs + desl] <- rt
                            # rs <- rs + desl

        � poss�vel fazer uma escrita na mem�ria e no registrador em uma 
        mesma instru��o, uma vez que os sinais necess�rios ocorrem em ciclos 
        diferentes. Nesse caso, a opera��o � similar ao store word, com a 
        diferen�a de que o endere�o onde o dado foi gravado � retornado em 
        um registrador.

                            1      2      3      4      5
                        -----  -----  -----  -----  -----
                         IF-1   ID-1   EX-1  MEM-1   WB-1
        IF   IF/ID.Wr       1      -      -      -      -
             PCWr           1      -      -      -      -
        EX   ALUSrc         -      -      1      -      -
             OpAlu        ---    ---    010    ---    ---
             RegDst        --     --     10     10     10
        MEM  MemRd          -      -      -      0      -
             MemWr          -      -      -      1      -
        WB   RegWr          -      -      -      -      1
             MemToReg       -      -      -      -      1

    c)  ldi rd,rs,rt        # rd <- M[rs + rt]

                            1      2      3      4      5      6
                        -----  -----  -----  -----  -----  -----
                         IF-1   ID-1   EX-1  MEM-1  MEM-2   WB-1
        IF   IF/ID.Wr       1      -      -      -      -      -
             PCWr           1      -      -      -      -      -
        EX   ALUSrc         -      -      0      -      -      -
             OpAlu        ---    ---    010    ---    ---    ---
             RegDst         -      -     01     01     01     01
        MEM  MemRd          -      -      -      1      1      -
             MemWr          -      -      -      0      0      -
             Mem2Mem        -      -      -      0      1      -
        WB   RegWr          -      -      -      -      -      1
             MemToReg       -      -      -      -      -      0

    d)  bal desl            # r31 <- PC + 8
                            # PC  <- (PC + 4) + ext (desl << 2)

        Nesse caso � interessante realizar ambas as somas necess�rias em um 
        s� ciclo, pois n�o haver� concomit�ncia de utiliza��o dos 
        componentes. Para isso pode-se utilizar naturalmente o somador de 
        deslocamento do PC para a segunda opera��o, al�m de direcionar a 
        primeira opera��o � ALU. Assim, o est�gio da mem�ria � utilizado 
        para realizar o desvio, enquanto o pr�ximo passo encarrega-se de 
        gravar PC + 8 no registrador $31 enquanto alguma pr�xima instru��o 
        pode acessar a mem�ria de registradores para leitura.

                  MUX                         MUX                   MUX
                 +----+                      +----+                +----+
           rt ---| 00 |                rt ---| 00 |           x ---| 00 |
           rd ---| 01 |--- RegMem     ext ---| 01 |--- ALU   PC ---| 01 |--- ALU
           rs ---| 10 |                 8 ---| 10 |                +----+
           31 ---| 11 |                      +----+                   |
                 +----+                         |                  ALUSrcA
                    |                        ALUSrcB
                 RegDst

                            1      2      3      4      5
                        -----  -----  -----  -----  -----
                         IF-1   ID-1   EX-1  MEM-1   WB-1
        IF   IF/ID.Wr       1      -      -      -      -
             PCWr           1      -      -      -      -
        EX   ALUSrcA       --     --     01     --     --
             ALUSrcB       --     --     10     --     --
             OpAlu        ---    ---    010    ---    ---
             RegDst         -      -     11     11     11
        MEM  MemRd          -      -      -      0      -
             MemWr          -      -      -      0      -
             PCSrc          -      -      -      1      -
        WB   RegWr          -      -      -      -      1
             MemToReg       -      -      -      -      1

    e)  bgtzal rs,desl      # if (rs >= 0) {
                            #   r31 <- PC + 8
                            #   PC  <- (PC + 4) + ext (desl<<2)
                            # }

        Usando a mesma estrutura da quest�o (d), � necess�rio adicionar mais 
        algumas estruturas como portas l�gicas e multiplexadores para 
        funcionar juntamente a um novo sinal de controle que define esta 
        instru��o, al�m de fazer com que a instru��o some PC + 8 em um 
        somador adicional, ou mesmo repetindo um passo atrav�s de mais um 
        ciclo.

        Com isso, em EX, se o sinal zero da ALU responde verdadeiramente � 
        condi��o rs >= 0, e o sinal da instru��o estiver ativo, ent�o o 
        registrador $31 � selecionado.

        Igualmente � condi��o anterior, em MEM, a origem de PC � ajustada 
        para o desvio calculado em EX.

                  MUX                    PCSrc (mux)
                 +---+                        |
       RegDst ---| 0 |                   +---------+
           11 ---| 1 |--- RegDst         |   OR    |
                 +---+                   +---------+
                   |                       |     |
                 +---+                   +---+   |
                 |AND|                   |AND|  PCSrc
                 +---+                   +---+
                 |   |                   |   |
               zero bgtzal             zero bgtzal

    f)  b desl              # PC <- (PC + 4) + ext (desl << 2)

        Como n�o existem condi��es de desvio, as duas instru��es anteriores � 
        essa devem ser descartadas, assim como um sinal de controle deve 
        for�ar PCSrc para 1, fazendo-o receber o valor do deslocamento 
        obrigatoriamente.

        Todos os demais sinais de controle devem ser neutralizados.

11. Enquanto estudamos processadores segmentados (Cap-6), vimos v�rias
    t�cnicas para aumentar o desempenho dos processadores, algumas delas 
    listadas abaixo. Para cada um dos itens abaixo, explique clara e 
    sucintamente como e porqu� este produz ganho de desempenho.
    
    a)  segmenta��o em cinco est�gios;

        A t�cnica de segmenta��o n�o necessariamente melhora o tempo das 
        instru��es, mas sim agiliza bastante a vaz�o delas. Em condi��es 
        ideais e com as otimiza��es necess�rias, um processador segmentado � 
        cinco vezes mais r�pido que sua vers�o equivalente em multiciclo.

    b)  elimina��o de riscos estruturais;

        Procura garantir que cada instru��o ocupar� uma parte do dispositivo de
        processamento em tempos diferentes devido � limita��es de hardware, 
        tais como leitura e escrita simult�neas na mem�ria.

    c)  elimina��o de riscos de dados [depend�ncias de dados];

        Evitar esperas de escrita nos registradores para recupera��o de 
        dados j� processados, fazendo com que os ciclos de clock prossigam 
        sua atividade ininterruptamente.

    d)  elimina��o de riscos de controle [depend�ncias de controle];

        Dados necess�rios � tomadas de decis�o no fluxo de dados tamb�m 
        podem demorar para serem retornados. Nesse caso usa-se uma diretiva 
        de tentativa com probabilidade razo�vel para tomar a decis�o em 
        quest�o, o que na m�dia admite bons resultados e compensam por 
        apresentar em um menor n�vel de interrup��es.

    e)  super-escalaridade;

    f)  execu��o especulativa.

12. Considere um circuito combinacional que n�o � segmentado e t�m lat�ncia
    T; para este circuito, a taxa de computa��o � 1/T. O ganho de 
    desempenho de uma implementa��o segmentada pode ser modelada pela 
    Equa��o que define G, na qual T � a lat�ncia do circuito original sem 
    segmenta��o, S � o atraso adicional introduzido pelos registradores e k 
    � o n�mero de est�gios, supondo que a computa��o possa ser dividida em k 
    est�gios iguais. G=1/(T/k+S) Assim, (T/k+S) � o tempo m�nimo necess�rio 
    para a computa��o em cada est�gio do novo sistema, e 1/(T/k+S) � sua 
    taxa de computa��o (throughput).  A Equa��o � similar � Lei de Amdahl.  
    
    a) Descreva a rela��o entre estas duas equa��es;
    
    b) D� uma explica��o intuitiva para a semelhan�a entre as duas equa��es.
