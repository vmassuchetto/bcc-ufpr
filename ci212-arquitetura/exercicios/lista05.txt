
 1) Por que um compilador transformaria o código da versão 1 (v1) no código
    da versão 2 (v2)? Justifique sua resposta. Note que o contexto da
    resposta deve ser memória virtual.

    /* v1 */
    for (j = 0; j < 1024; j++)
        for (i = 0; i < 100; i++)
            x[i][j] = x[i][j] + k;

    /* v2 */
    for (i = 0; i < 100; i++)
        for (j = 0; j < 1024; j++)
            x[i][j] = x[i][j] + k;

    ----

    Os 100 vetores x[i] possuem 1024 posições cada. Se cada uma das posições 
    deste vetor for um inteiro de 4 bytes, teremos 1024 * 4 = 4 Kbytes por 
    vetor.
    
    Se considerarmos que os vetores são contíguos na memória, e se for usado 
    um padrão de páginas virtuais com também 4K, a recuperação de cada vetor 
    por completo se dará somente em um acesso.
    
    Na versão um do programa será necessário copiar a mesma página diversas 
    vezes para a cache, já que quando x[i] varia, outra página está sendo
    acessada.
    
    Na versão dois, x[i] varia com menor frequência e faz com todo o vetor
    seja copiado para a cache e a operação de soma é feita sem ser necessário
    acessar outra página de memória virtual.


 2) Escreva a expressão do Tempo Médio de Acesso à Memória para uma
    hierarquia com uma cache primária unificada, uma cache secundária, memória
    DRAM e disco.

    ----

    TM = TL1 + FL1 (TL2 + FL2) * TM  +
         TTLB + FTLB * (TM + FMTP * TD)

    TT   Tempo Médio de Acesso aos Dados
    TL1  Tempo de Acesso à Cache L1
    FL1  Taxa de Falhas da Cache L1
    TL2  Tempo de Acesso à Cache L2
    FL2  Taxa de Falhas da Cache L2
    TM   Tempo de Acesso à Memória
    TTLB Tempo de Tradução de um Endereço Virtual
    FTLB Taxa de Falhas dos Endereços na TLB
    FMTP Taxa de Falhas na Tabela de Páginas
    TD   Tempo de Acesso ao Disco


 3) Faça um diagrama detalhado de uma cache de mapeamento de endereços (TLB)
    com 256 blocos e associatividade quaternária. Cada bloco contém um
    mapeamento, o processador emite endereços de 32 bits, e o endereço físico
    possui 36 bits. Páginas virtuais tem 4Kbytes. Qual o tamanho da Tabela de
    Páginas em memória? Indique como um endereço é interpretado pelo
    controlador da TLB. Indique o mecanismo de reposição de blocos num mesmo
    conjunto.

    ----

                    31         18 17        12 11     0
                    +------------+------------+-------+
                    |    TAG     |   INDEX    |  OFF  |
                    +------------+------------+-------+

        V M TAG EF        V M TAG EF        V M TAG EF        V M TAG EF
       +-+-+--+--+       +-+-+--+--+       +-+-+--+--+       +-+-+--+--+
       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|
       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|
           x 64             x 64              x 64               x 64
       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|
       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|       |x|x|xx|xx|
       +-+-+--+--+       +-+-+--+--+       +-+-+--+--+       +-+-+--+--+
        |    |  |         |    |  |         |    |  |         |    |  |
        |    =  |         |    =  |         |    =  |         |    =  |
        +----+  |         +----+  |         +----+  |         +----+  |
             |  |              |  |              |  |              |  |
             +-----------------+----+------------+-----------------+  |
                |                 | |               |                 |
                +-----------------+-|-----+---------+-----------------+
                                    |     |
                                    | 4   | 24
                                    +--- MUX  OFF
                                          |    |
                                          |    | 12
                                    ---------------
                                    ENDEREÇO FÍSICO
                                    ---------------
                                           | 36

    INDEX  = 2^6  = 64 entradas
    OFFSET = 2^12 = 4096 bytes = 4 Kbytes

    Tamanho de cada célula
    [ V + M + TAG + EF ] * 64 entradas * 4 células
      1 + 1 +  14 + 24   * 64          * 4         = 10240 bytes
                                                     10 Kbytes

 4) Mostre como organizar a Tabela de Páginas do exercício anterior em dois
    e em três níveis. Escreva, em pseudocódigo, uma função com o protótipo
    abaixo que percorre uma tabela de páginas de dois níveis e retorna ’1’
    se a página está em memória, ou ’0’ numa falta. O endereço físico é
    atribuído à '*enderfis' num acerto. Explicite quaisquer suposições que
    forem necessárias.

    int buscatp (void *basetp, void* endervirt, void** enderfis);

    ----

    * basetp é um vetor de posições de página em que as propriedades de cada
      posição podem ser acessadas como basetp[n]->propriedade

    * traduzend() retorna o endereço físico de um endereço virtual buscado
      na TLB

    int buscatp (void *basetp, void* endervirt, void** enderfis) {

        int i = 0;
        int t;

        while (basetp[i]) {

            // Acerto em primeiro nível
            if (basetp[i]->tag == endervirt->tag &&
                basetp[i]->valido == 1) {
                t = basetp[i]->end;

                if (basetp[t]->tag == endervirt->tag &&
                    basetp[t]->valido == 1)
                    enderfis = basetp[t]->end;

                return 1;
            }

            // Busca na tabela de páginas da memória
            t = traduzend (endervirt);
            if (mem[t]->end) {
                enderfis = mem[t]->end;
                return 1;
            }

        }

        return 0;
    }


 5) Um sistema de memória virtual tem páginas de 1024 palavras, oito páginas
    virtuais e quatro páginas físicas. Num dado instante, a tabela de
    páginas está na seguinte condição:
    
    Página virtual  0 1 2 3 4 5 6 7
    Página física   3 1 - - 2 - 0 -

    a)  faça uma lista de todos os endereços virtuais (endereço de palavra)
        que podem causar uma falta de página (indique as faixas de endereços);
    
        Faixas  Endereços
        2       2048 a 3071
        3       3072 a 4095
        5       5120 a 6143
        7       7168 a 8191

    b)  quais são os endereços em memória física para os seguintes endereços
        virtuais: 0, 3728, 1023, 1024, 1025, 4096?

           0    0
        3728    3
        1023    0
        1024    1
        1025    1
        4096    4


 6) A seguinte seqüência de números de páginas virtuais ocorre durante a
    execução de um programa: 3 4 2 6 4 7 1 3 2 6 3 5 7 4 Suponha que a
    política de substituição de páginas é LRU. Calcule a taxa de acerto em
    páginas (número de acertos dividido pelo número de referências), para
    uma memória física com quatro páginas.

    ----

    I   3 4 2 6 4 7 1 3 2 6 3 5 7 4
    0   3 3 3 3 3 7 7 7 7 6 6 6 6 4
    1     4 4 4 4 4 4 4 2 2 2 2 7 7
    2       2 2 2 2 1 1 1 1 1 5 5 5
    3         6 6 6 6 3 3 3 3 3 3 3

    2 acertos / 14 acessos = 14,28% de taxa de acertos


 7) Considere o programa de multiplicação de matrizes abaixo. Suponha que as
    matrizes contém 1024x1024 elementos, cada elemento um double (8 bytes).
    O programa é executado num único processador com páginas de 8Kbytes, e
    cache secundária de 2MBytes.

    a) Descreva o comportamento do sistema de memória virtual durante a
    execução deste programa;

    b) Sugira uma ou mais maneiras de melhorar o desempenho da multiplicação
    de matrizes, envolvendo somente paginação.

    ----

    2 Mbytes / 8 kbytes = 250 páginas em cache secundária
                        = 24,44% da capacidade necessária pelo processo

    Considerando que as páginas consistem em sequencializações dos elementos
    da tabela na horizontal, a variável k à medida que é incrementada fará
    com que ocorram 1024 acessos diferentes ao longo das colunas em conjunto
    com j (b[k][j]) para cada acesso a uma linha (a[i][k]).

    Nesse caso a cache será sub-utilizada, principalmente se a política de
    substituição das posições for LRU, pois assim ao final do percurso de
    250 linhas, as próximas 250 as sobreescreverão, e assim por diante,
    fazendo com que haja uma sucessão de falhas de acesso à medida que novas
    linhas vão sendo requisitadas.

    Uma atenuação desta situação é utilizar uma política de alocação
    aleatória, embora isto não seja nada definitivo para solucionar o
    problema.

    O problema mesmo reside no código, que para tornar-se mais eficiente e
    utilizar melhor a cache, pode calcular os elementos resultantes da
    multiplicação de matriz em somas distribuídas, incrementando os valores
    na matriz final à medida que as linhas são percorridas em razões de n^2.
    Desse modo, fazem-se somente 1024 acessos à páginas, utilizando-as
    separadamento, uma de cada vez.


10) Os diagramas ao lado mostram três organizações possíveis para sistemas
    com caches e memória virtual (TLBs). As setas representam somente as
    linhas de endereço.

    a) Para cada um dos modelos A,B,C, indique claramente quais são
    endereços físicos e quais são endereços virtuais.

    b) Para cada um dos modelos, descreva clara e sucintamente a seqüência
    de eventos associados a um *acerto* e a uma *falta* na cache. (2)
    Discuta as vantagens e desvantagens de cada um dos três modelos.

    ----

    A: Cache fisicamente indexada e rotulada

      CPU ----> TLB ----> Cache ----> Memória
            V         F           F

    O acesso à cache inclui também o tempo de tradução de um endereço físico
    para virtual, o que pode acarretar em substancial perda de desempenho.

    Se o endereço virtual está na TLB, o endereço físico é obtido e
    procurado na cache e na memória.

    B: Cache virtualmente indexada e rotulada

      CPU ----> Cache ----> TLB ----> Memória
            V           V         F

    O acesso à cache é feito diretamente através de um endereço virtual,
    assim diminui-se a latência de acesso à TLB, mas gera-se o problema de
    aliasing, em que programas compartilham as mesmas páginas, trazendo
    riscos de segurança na modificação das páginas, sendo necessário que
    normalmente o software trate as permissões de acesso às páginas pelos
    usuários.

    A cache armazena uma série de dados indexados pelos seus endereços
    virtuais, caso haja uma falha este endereço é procurado na TLB, que aí
    sim buscará o dado na memória.

    C: Cache virtualmente indexada e fisicamente rotulada

      CPU ---->  Cache  <--+
       |   F               | F
       |                   |
       +------>  TLB  -----+----> Memória
           V                 F
           
    Usa-se o offset do endereço virtual que não é traduzido, e uma tag 
    física para identificação na cache.

11) Considere um sistema de memória virtual com as seguintes características:

    i   Endereço virtual de 32 bits (endereço de byte);
    ii  Páginas com 4 Kbytes;
    iii Endereço físico com 38 bits.
    
    A tabela deve conter bits de válido, read-only, executável, sujo e usado.
        
    a)  Qual é o tamanho de uma tabela de páginas linear nesta máquina?
    
        Endereço virtual - offset de página = número de páginas virtuais
        2^32 - 2^12 = 2^20
    
    b)  Mostre como implementar a Tabela de Páginas em dois níveis.
    
         31         22 21         12 11            0
        +-------------+-------------+---------------+
        |     N1      |     N2      |     OFFSET    |
        +-------------+-------------+---------------+
    
    c)  Suponha que na sua implementação do item (b), 3/4 dos
        elementos da tabela de primeiro nível sejam nulos. Quais os tamanhos
        máximo e mínimo do espaço de endereçamento utilizado pelo programa?

        Usando 1/4 * 1024 = 256 entradas de primeiro nível
        
        Espaço mínimo = 256 endereços
        256 entradas na tabela principal * 1 entrada em cada tabela filha
        
        Espaço máximo = 256 * 1024 = 262144 endereços
        256 entradas na tabela principal * 1024 entradas em cada tabela filha
