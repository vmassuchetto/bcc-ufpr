library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ent_cache4 is
    port(
        clock:      in  std_logic;
        address:    in  std_logic_vector(31 downto 0);
        timestamp:  in  integer;
        hit:        out std_logic;
        pos1:       out integer;
        pos2:       out integer;
        pos3:       out integer;
        pos4:       out integer
    );
end ent_cache4;

architecture arq_cache4 of ent_cache4 is

    -- Cache de 2Kbytes com 4 vias = 8Kbytes
    constant cache4_size: integer := (2 * 1024);
    type cache4_type
        is array (0 to cache4_size-1)
        of std_logic_vector(16 downto 0);
    type cache4_type_time
        is array (0 to cache4_size-1)
        of integer;

    signal cache41_data: cache4_type;
    signal cache42_data: cache4_type;
    signal cache43_data: cache4_type;
    signal cache44_data: cache4_type;

    signal cache41_time: cache4_type_time;
    signal cache42_time: cache4_type_time;
    signal cache43_time: cache4_type_time;
    signal cache44_time: cache4_type_time;

begin

    process
        variable tag: std_logic_vector(15 downto 0);
        variable index: std_logic_vector(10 downto 0);
        variable n: integer;
    begin

        wait until clock = '1';

        --   31           16 15               5 4       2 1     0
        --  +---------------+------------------+---------+-------+
        --  |    15bits     |      12bits      |  3bits  | 2bits |
        --  +---------------+------------------+---------+-------+
        --        TAG              INDEX          BLOCO
        --                        2Kbytes

        tag := address(31 downto 16);
        index := address(15 downto 5);
        n := conv_integer(index);

        -- Acerto

        -- O dado está na primeira via
        if (cache41_data(n) = "1" & tag) then
            cache41_time(n) <= timestamp;
            hit <= '1';

        -- O dado está na segunda via
        elsif (cache42_data(n) = "1" & tag) then
            cache42_time(n) <= timestamp;
            hit <= '1';

        -- O dado está na terceira via
        elsif (cache43_data(n) = "1" & tag) then
            cache43_time(n) <= timestamp;
            hit <= '1';

        -- O dado está na quarta via
        elsif (cache44_data(n) = "1" & tag) then
            cache44_time(n) <= timestamp;
            hit <= '1';


        -- Falha: Escolha de reposição

        -- A primeira via tem o dado mais antigo ou
        -- As posições estão zeradas
        elsif (cache41_time(n) <= cache42_time(n)) and
            (cache41_time(n) <= cache43_time(n)) and
            (cache41_time(n) <= cache44_time(n)) then
            cache41_data(n) <= "1" & tag;
            cache41_time(n) <= timestamp;
            hit <= '0';
            pos1 <= n;
        
        -- A segunda via tem o dado mais antigo ou
        -- As posições a partir dela estão zeradas
        elsif (cache42_time(n) < cache41_time(n)) and
            (cache42_time(n) <= cache43_time(n)) and
            (cache42_time(n) <= cache44_time(n)) then
            cache42_data(n) <= "1" & tag;
            cache42_time(n) <= timestamp;
            hit <= '0';
            pos2 <= n;

        -- A terceira via tem o dado mais antigo ou
        -- As posições a partir dela estão zeradas
        elsif (cache43_time(n) < cache41_time(n)) and
            (cache43_time(n) < cache42_time(n)) and
            (cache43_time(n) <= cache44_time(n)) then
            cache43_data(n) <= "1" & tag;
            cache43_time(n) <= timestamp;
            hit <= '0';
            pos3 <= n;

        -- A quarta via tem o dado mais antigo
        elsif (cache44_time(n) < cache41_time(n)) and
            (cache44_time(n) < cache42_time(n)) and
            (cache44_time(n) < cache43_time(n)) then
            cache44_data(n) <= "1" & tag;
            cache44_time(n) <= timestamp;
            hit <= '0';
            pos4 <= n;

        end if;
        
    end process;

end arq_cache4;
