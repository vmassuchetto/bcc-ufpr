library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ent_cache2 is
    port(
        clock:      in  std_logic;
        address:    in  std_logic_vector(31 downto 0);
        timestamp:  in  integer;
        hit:        out std_logic;
        pos1:       out integer;
        pos2:       out integer
    );
end ent_cache2;

architecture arq_cache2 of ent_cache2 is

    -- Cache de 4Kbytes com 2 vias = 8Kbytes
    constant cache2_size: integer := (4 * 1024);
    type cache2_type
        is array (0 to cache2_size-1)
        of std_logic_vector(15 downto 0);
    type cache2_type_time
        is array (0 to cache2_size-1)
        of integer;

    signal cache21_data: cache2_type;
    signal cache22_data: cache2_type;
    
    signal cache21_time: cache2_type_time;
    signal cache22_time: cache2_type_time;

begin

    process
        variable tag: std_logic_vector(14 downto 0);
        variable index: std_logic_vector(11 downto 0);
        variable n: integer;
    begin

        wait until clock = '1';

        --   31           17 16               5 4       2 1     0
        --  +---------------+------------------+---------+-------+
        --  |    15bits     |      12bits      |  3bits  | 2bits |
        --  +---------------+------------------+---------+-------+
        --        TAG              INDEX          BLOCO
        --                        4Kbytes

        tag := address(31 downto 17);
        index := address(16 downto 5);
        n := conv_integer(index);
        
        -- Acerto
        
        -- O dado está na primeira via
        if (cache21_data(n) = "1" & tag) then
            cache21_time(n) <= timestamp;
            hit <= '1';
        
        -- O dado está na segunda via
        elsif (cache22_data(n) = "1" & tag) then
            cache22_time(n) <= timestamp;
            hit <= '1';


        -- Falha: Escolha de reposição

        -- A primeira via tem o dado mais antigo ou
        -- As posições estão zeradas
        elsif (cache21_time(n) <= cache22_time(n)) then
            cache21_data(n) <= "1" & tag;
            cache21_time(n) <= timestamp;
            hit <= '0';
            pos1 <= n;
        
        -- A segunda via tem o dado mais antigo
        elsif (cache21_time(n) > cache22_time(n)) then
            cache22_data(n) <= "1" & tag;
            cache22_time(n) <= timestamp;
            hit <= '0';
            pos2 <= n;

        end if;
        
    end process;

end arq_cache2;
