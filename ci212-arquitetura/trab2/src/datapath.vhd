library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_bit.all;
use std.textio.all;

entity datapath is
    port(
        clock: in std_logic;
        reset: in std_logic
    );
end datapath;

architecture arq_datapath of datapath is
  
    signal address:         std_logic_vector(31 downto 0);
    signal timestamp:       integer;

    signal cache1_hit:      std_logic;
    signal cache1_pos:      integer;
    signal cache1_count:    integer;

    signal cache2_hit:      std_logic;
    signal cache2_pos1:     integer;
    signal cache2_pos2:     integer;
    signal cache2_count:    integer;

    signal cache4_hit:      std_logic;
    signal cache4_pos1:     integer;
    signal cache4_pos2:     integer;
    signal cache4_pos3:     integer;
    signal cache4_pos4:     integer;
    signal cache4_count:    integer;

begin

    -- Leitura do arquivo de endereços
    fileread: entity work.ent_fileread port map (
        clock,
        address
    );
    
    -- Cache de mapeamento direto
    cache1: entity work.ent_cache1 port map (
        clock,
        address,
        cache1_hit,
        cache1_pos
    );
    
    -- Cache de associatividade binária
    cache2: entity work.ent_cache2 port map (
        clock,
        address,
        timestamp,
        cache2_hit,
        cache2_pos1,
        cache2_pos2
    );

    -- Cache de associatividade quaternária
    cache4: entity work.ent_cache4 port map (
        clock,
        address,
        timestamp,
        cache4_hit,
        cache4_pos1,
        cache4_pos2,
        cache4_pos3,
        cache4_pos4
    );
   
    process(clock, reset)
        variable inline: line;
    begin
        
        if reset = '1' then
            timestamp <= 0;
            cache1_count <= 0;
            cache2_count <= 0;
            cache4_count <= 0;
        
        elsif clock'event and clock = '1' then
            
            -- Contadores dos acertos
            
            if cache1_hit = '1' then
                cache1_count <= cache1_count + 1;
            end if;
            
            if cache2_hit = '1' then
                cache2_count <= cache2_count + 1;
            end if;
            
            if cache4_hit = '1' then
                cache4_count <= cache4_count + 1;
            end if;

        
            -- Contador de acessos
            timestamp <= timestamp + 1;
        
        
            -- Imprime os resultados
            -- A condição é para deixar o processo mais rápido
       
            if timestamp > 37289000 then
       
                write(inline, string'("T "));
                write(inline, timestamp);
                write(inline, string'(" "));
                
                write(inline, string'("C1 "));
                write(inline, cache1_count);
                write(inline, string'(" "));

                write(inline, string'("C2 "));
                write(inline, cache2_count);
                write(inline, string'(" "));

                write(inline, string'("C4 "));
                write(inline, cache4_count);
                write(inline, string'(" "));

                writeline(output, inline);
            
            end if;
                   
        end if;
        
    end process;

end arq_datapath;
