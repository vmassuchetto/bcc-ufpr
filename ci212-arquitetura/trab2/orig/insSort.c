#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define VECTOR_SIZE 70000

int main (void)  {

	int x[VECTOR_SIZE];

	register int i, j, smallest; //Compiler will try to dedicate a cpu register to these variables, so don't worry about them.
    
	for (i = 1; i < VECTOR_SIZE; i++) {
		
		//1 access here
		printf ("%x\n", &(x[i]));
		smallest = x[i];

		j = i - 1;
		
		//1 access here
		printf ("%x\n", &(x[j]));
		while ((j >= 0) && ( x[j] > smallest)) {
			
			//2 access here
			printf ("%x\n", &(x[j+1]));
			printf ("%x\n", &(x[j]));
			x[j + 1] = x[j];
			j--;
		}

		//1 access here
		printf ("%x\n", &(x[j+1]));
		x[j + 1] = smallest;
	}

}

