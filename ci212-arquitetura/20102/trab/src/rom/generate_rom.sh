#!/bin/bash

echo "" > rom_middle.txt

i=0
while read line
do
	INST=$(echo $line|tr -s "," " ")
	echo "$INST"| egrep "([a-zA-Z]|[0-9])+" > /dev/null
	if (( $? == 0 ))
	then
		echo -e "\t\t\t\t$(./generate_line_rom.sh $INST $i)" >> rom_middle.txt
		i=$((i + 1))
	fi
done < $1


cat rom_up.txt rom_middle.txt rom_bottom.txt > rom32.vhd		
