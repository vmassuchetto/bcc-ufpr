library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.p_MI0.all;

entity alu is
    port (
		ctl    : in  std_logic_vector (2 downto 0);
		a, b   : in  reg32;
		result : out reg32;
	    zero   : out std_logic
    );
end alu;

architecture alu_arq of alu is

	signal intReg  : reg32;
    --! JR Declara o sinal zero que habilita o branch
    signal intZero : std_logic;

begin
	process (a, b, ctl)
	begin

		case ctl is
            --! JR O que deve ser feito na instrução JR
            when JJR  =>
                intReg  <= a;   --! JR Simplesmente repassa o valor do primeiro registrador
                intZero <= '1'; --! JR Habilita o zero para um branch


            when SUBU => intReg <= a - b;
			when AAND => intReg <= a and b;
			when OOR  => intReg <= a or b;
			when ADDU => intReg <= a + b;
			when SLT  =>
				if a < b then
					intReg <= (0 => '1', others => '0');
				else
					intReg <= (others => '0');
				end if;
			when  others => intReg  <= (others => 'X');
		end case;
	end process;

	zero   <= intZero;  --! JR Emite o sinal zero
	result <= intReg;

end alu_arq;
