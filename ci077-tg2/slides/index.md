# Conceitos

## Software Livre

Quatro liberdades: utilizar, estudar, redistribuir e modificar. `(STALLMANN,
2007)`

Livre não é gratuito. `(STALLMAN, 2007)`

Mais seguro, mais inovador, menos monopolista. `(KAMINSKY, 2009)`

## Sociedade da Informação

Revoluções tecnológicas definem-se pela sua penetrabilidade em todos os
domínios da atividade humana. `(CASTELLS, 1999)`

Três grandes momentos da humanidade: a introdução da agricultura, a revolução
industrial e a revolução tecnológica. `(MASUDA, 1980)`

Economia do ciberespaço, &ldquo;todas as músicas como parte de uma só
música&rdquo;.  `(LEVY, 1998)`

Resultante da uma reestruturação do sistema capitalista em prol de sua lógica
de acumulação. `(SODERBERG, 2002; DYER-WITHEFORD; 1999; CASTELLS, 1999)`

## Marxismo

Originado dos estudos sociológicos de Marx e Engels no século XVIII, com
alicerces na filosofia idealista alemã de Hegel, no materialismo filosófico
francês do século XVIII, e na economia política inglesa do começo do século XIX
&mdash; &ldquo;Marx foi um homem de seu tempo&rdquo;.  `(NETTO, 2002)`

&ldquo;Convicção científica da eficácia do materialismo dialético e de sua
expansibilidade sobre as linhas fundantes.&rdquo; `(LUKACS, 1919)`

# Objetivos

Melhor compreensão do momento histórico e do estado tecnológico da sociedade.

Discussão sobre as circunstâncias da inserção tecnológica nas atividades
humanas sob o ponto de vista do determinismo capitalista.

Interpretar alguns dos conceitos da teoria marxista nas dinâmicas do software
livre com o fim de auxiliar o entendimento de suas motivações.

# Metodologia

## Marxismo Como<br/>Modelo Teórico

&ldquo;Marx foi capaz de apreender as tendências estruturais da ordem burguesa
porque encontrou a perspectiva teórico-metodológica para tal.&rdquo; `(NETTO,
2002)`

&ldquo;A visão marxista deve ser capaz de enfrentar as contingências
sócio-históricas mutáveis e inevitáveis, reexaminando as proposições básicas da
teoria original.&rdquo; `(MESZAROS, 2010)`

&ldquo;Se o marxismo é tido como obsoleto pela era da informação, é somente
pelo desenvolvimento informacional é que podemos ver a completa importância de
alguns de seus temas na sociedade atual.&rdquo; `(DYER-WITHEFORD, 1999)`

&ldquo;O sucesso do software livre e sua alternativa paralela à hegemonia
econômica na informação é uma amostra do que foi descrito por Marx há mais de
150 anos.&rdquo; `(SODERBERG, 2002)`

## Marxismo na<br/>Era da Informação

Marginalização sistemática por governos neoliberais, pela mídia e pela moda:
&ldquo;as teses de Marx não foram divulgadas pelos canais que nossa Ciência e
Filosofia empregam normalmente.&rdquo; `(GIANNOTTI, 2011)`

Afirmação hegemônica do capitalismo informacional nas décadas de 80 e 90:
&ldquo;um enorme e competente polvo com tentáculos fortemente agarrados na
tecnologia da informação&rdquo;. `(DUPAS, 2001)`

Influência em diversas militâncias atuais e que não possuem origem
essencialmente operária: ambientalistas, feministas, anti-racistas, cultura
livre... `(GURGEL; MENDES, 2010)`

Crescente adoção de conceitos autonomistas para explicar fenômenos de
aprendizado coletivo que tornam-se uma entidade produtiva. `(SODERBERG, 2002)`
Embora com isso possam ser abandonadas algumas colocações estruturais da teoria
original.  `(DYER-WITHEFORD, 2004)`

<img src="img/marx-right.jpg" /> `(MCLENNAN, 2008)`

# Conceitos Marxistas

## Propriedade

Hegelianismo tecnológico: &ldquo;o real é o ideal&rdquo;, e no ciberespaço é
&ldquo;virtualmente ideal&rdquo;, como uma expressão hegeliana do homem
enquanto promotor da tecnologia, formando um foro único de sua própria
consciência. `(STALLABRAS, 1996)`

&ldquo;A acumulação de conhecimento ocorre como uma oposição ao trabalho e
constitui um atributo do capital fixo à medida que este adentra o processo de
produção como próprio meio de produção.&rdquo; `(MARX, 1939)`

&ldquo;Somente o capital subjugou o progresso histórico em detrimento do seu
enriquecimento&rdquo; &mdash; em análise do &lsquo;gerenciamento
científico&rsquo; descrito por Babbage. `(MARX, 1939)`

O caráter imaterial da informação permite a propriedade somente através da
transparência (percepção de valor) e da artificialização da excludibilidade
(direito de uso) e da rivalidade (direito de cópia). `(DELONG; FROOMKIN, 1997)`

A regulação ocorre através da legislação (Copyright), normas sociais (cultura e
moral), mercado (pagar por informação), arquitetura (permissões via software,
artifícios via hardware). `(LESSIG, 2006)`

O software livre é capaz de subverter a legislação através do Copyleft, propor
normas sociais através da cultura livre, e de estabelecer uma economia
diferenciada baseada em serviços. `(SODERBERG, 2002)`

&ldquo;A &lsquo;única&rsquo; diferença entre propriedade privada e a ideia de
propriedade da GPL é a mudança de foco da exclusão para o
compartilhamento.&rdquo; `(PEDERSEN, 2010)`

## Produção

&ldquo;Em todas as formas de sociedade há um tipo específico de produção que
predomina sobre as demais&rdquo; `(MARX, 1939)`

A real propriedade sobre qualquer meio de produção dá-se quando existe o pleno
poder de fazê-lo exercer sua capacidade produtiva. `(HARNECKER, 1973)`

No software livre há uma certa diluição da capacidade produtiva na sociedade,
mas ainda assim com concentração sobre pequenos grupos de desenvolvedores
&mdash; o processo não é plural `(SODERBERG, 2002)`

Ao mesmo tempo, não é possível legitimar o &lsquo;proprietário&rsquo; de um
software livre, tampouco menosprezar sua capacidade inovativa e socioeconômica.

## Trabalho

&ldquo;A introdução de um sistema de produção em rede expressa-se como um dos
grandes resultados da revolução burguesa que eclodiu do sistema feudal que
tinha antes como base a posse aristocrática da terra.&rdquo; &mdash;
desenvolvimento do comércio `(NETTO, 2002)`

O capitalismo tecnocrático viria a gerar os &lsquo;trabalhadores
científicos&rsquo; e o &lsquo;trabalho universal&rsquo;. `(MARX, 1939)`

O capital nunca será capaz de absorver toda a força produtiva disponível, e
organizações antagônicas ao projeto capitalista podem surgir em uma liberdade
criadora de outros processos de produção &mdash; comunismo como
&lsquo;contra-poder&rsquo;  `(NEGRI, 1991)`

O trabalhador socializado é capaz de usar a extensão das características de seu
trabalho para sobredeterminar os interesses do capital. `(NEGRI, 1991 )`

&ldquo;A invenção intelectual ainda se apresenta como máquina-ferramenta para a
demanda do capitalista.&rdquo; `(GURGEL; MENDES, 2010)`
Recomposição da classe trabalhadora através da afirmação criativa em uma
conversão dos processos de desvalorização. `(CLEAVER, 1992)`

O software livre é uma demanda capitalista, mas é também uma corrente
anti-hegemônica, e sua compreensão requer uma negação do imperativismo teórico.
`(MERTEN, 2001)`
O valor de Negri está em medir os desdobramentos da tecnologia na luta de
classes.  `(DYER-WITHEFORD, 2004)`

## Valor

Os mecanismos para &lsquo;comodificação&rsquo; da informação buscam acima de
tudo a atribuição de escassez à informação, pois somente assim ela pode fazer
parte da lógica de alienação capitalista.

Software livre interpretado como uma expressão da &lsquo;cultura do dom&rsquo;
na atualidade &mdash; troca baseada no valor de uso e não no valor de
mercado. `(RAYMOND, 1998)`

O trabalho nas comunidades de software livre é fortemente relacionado com o
senso de cooperação e exercício da criatividade. `(HARRELSON, 2011)`

Os softwares livres oferecem menores custos e competitividade de
comercialização às produtoras de software. `(SKOK, 2012)`

As empresas se utilizam das dinâmicas nas comunidades de software livre para
alavancar produtos proprietários &mdash; Microsoft, Google, redes sociais e
distribuições Linux comerciais.

Insurgências de software são comuns quando existem divergências entre
comunidade e empresa desenvolvedora &mdash; OpenSSH, LibreOffice, Mageia,
Replicant.

## Considerações Finais

As observações sobre propriedade do conhecimento colocadas por Marx e Engels
podem ser identificadas nas dinâmicas do capitalismo informacional, e a
subversão de seu modelo de acumulação atual pelo software livre pode ser
confirmada na anulação de elementos econômicos que garantem sua
&lsquo;comodificação&rsquo;.

Assumindo o ciberespaço como meio produtivo em si, o software livre promove a
diluição de sua capacidade produtiva, impossibilitando grandes concentrações de
propriedade efetiva.

A cultura livre é uma forte expressão contra-hegemônica via processos de
produção alternativos, e as teses autonomistas das relações de trabalho
auxiliam no entendimento destes novos tipos de organizações sociais com
interesses antagônicos aos do projeto capitalista.

No entanto, esta contra-hegemonia não é absoluta, e não pode ser considerada
como uma instauração social revolucionária do meio digital, pois os softwares
livres também participam dos processos de acumulação de valor no campo
informacional.

O software livre constitui uma força presente somente em uma pequena totalidade
dos processos de produção modernos, e a caracterização de uma revolução digital
depende de uma análise muito mais ampla dos segmentos da sociedade permeados
significativamente pela tecnologia.

O emprego de software livre em si não garante nenhuma pluralidade econômica e
de acesso à tecnologia, porém, uma sociedade que realmente incorpore a
igualdade como princípio fundamental não verá sentido na propriedade de
conhecimento.
