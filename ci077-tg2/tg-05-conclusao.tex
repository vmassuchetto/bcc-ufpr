\chapter{Conclusão}

  A liberdade em suas diferentes formas para com a sociedade é uma longa
  temática trazida por várias correntes de pensadores através da história. O
  Iluminismo foi uma expressão mais moderna e humanizadora neste sentido, e
  trouxe aos autores posteriores uma posição mais focada na expressão do homem
  com o mundo e seus pares. Teve nos direitos humanos boa parte de sua
  incorporação, e à medida que quebravam a lógica medieval do pensar, também
  investigavam as práticas econômicas que estavam em curso, mostrando que a
  propriedade e as relações de trabalho eram eficientes respostas para
  perguntas emancipadoras e de caráter libertário.

  Como herança de toda esta tradição humanista e racionalista, o método
  marxista surge como uma forma bem aceita de análise da lógica de produção
  burguesa, e ainda propunha formas de contraposição através da organização.
  Dado o impressionante crescimento das economias baseadas em informação
  desde a década de 80, alguns autores consideraram esta percepção de análise
  ampla o suficiente para ser passível de utilização em nossa sociedade atual.
  Com a consolidação dos monopólios informacionais e uma primeira evidência
  do software livre durante a década de 90, iniciaram-se os estudos a respeito
  da cultura colaborativa, do monopólio da informação e dos modelos de inovação
  tecnológica, e logo vieram as primeiras interpretações de cunho
  revolucionário do software livre.

  A partir desta necessidade de entender as dinâmicas reguladas pela
  informação, há uma contradição evidente na literatura que aponta o
  alargamento excessivo que as correntes `neomarxistas' teriam dado aos
  conceitos básicos. Ao realizar os exercícios teóricos de uma nova sociedade,
  autores teriam identificado novos conceitos, novas entidades e com isso
  teriam se afastado demais da teoria base. No entanto, entende-se que a
  extrapolação circundante e ponderada é necessária, e pode vir a trazer
  reflexões esclarecedoras para o entendimento de nossa sociedade, e assim
  procurou explorar as relações entre alguns conceitos pontuais: a noção de
  propriedade, meios de produção, trabalho e geração de valor, e com isso
  construir uma análise da inferência do software livre dentro da realidade de
  produção de software na sociedade da informação.

  O aspecto que torna a economia da sociedade da informação mais peculiar
  reside no seu caráter imaterial, e no paradigma de ausência de custo e tempo
  para cópia e reprodução de uma mercadoria. Esta característica traz uma série
  de dinâmicas ainda mais aceleradas e complexas para a sociedade, modificando
  profundamente as relações de trabalho e propriedade. Os meios de produção,
  antes dependentes da propriedade da terra, localização geográfica e de uma
  estrutura industrial, passam a ser baseados cada vez mais em simplórias vias
  de veiculação da informação regidas por software. A produção independe do
  tempo em si, e pode ser gerada e absorvida com um grau pequeno de
  presencialidade, digitalizando a atividade intelectual e tornando-a
  produtiva.

  Estas nova economia fez com que o mercado se adaptasse em diferentes direções
  para fazer valer à informação as suas técnicas tradicionais de
  comercialização -- a chamada `comodificação da informação'. Mas a produção de
  software e também a produção cultural depararam-se com uma completa
  falência de tentativas em instituir um completo regime de \emph{copyright}, e
  impedir que qualquer informação legalmente protegida fosse redistribuída. Os
  artifícios tecnológicos empregados em pouco foram páreos para as redes de
  compartilhamento de arquivos e para a perspicácia técnica de engenharia
  reversa dos \emph{crackers}, condenações legais mostraram-se insuficientes, e
  o discurso de culpabilização moral para que os consumidores fossem justos com
  os artistas e produtoras não pareceu repercutir em favor da propriedade
  intelectual.

  Dentro das próprias condicionantes que a \emph{copyright} coloca, o êxito do
  software livre está justamente em sua capacidade de subvertê-la e utilizar
  seus próprios termos para objetivos antagônicos, e assim garantir que
  softwares derivados de licenças livres devam também ter seu código
  publicado de forma aberta. A principal contradição colocada pelo software
  livre reside nesta característica, que é a de eliminar os artifícios
  reguladores sobre a informação, tornando-a amplamente disponível e
  retirando-a de qualquer economia que venha a condicionar a escassez de
  \emph{commodities}.

  A produção destes softwares livres organiza-se de forma bastante peculiar e
  complexa através da rede de computadores e suas comunidades. Extingue-se a
  figura do proprietário legal, ganhador de \emph{royalties} e detentor das
  decisões. As comunidades de desenvolvimento passam a exercer este papel
  através de seu conhecimento e esforço intelectual, e definem quais os rumos
  de determinado software através de seus próprios critérios e organização,
  atribuindo a seus membros diferentes níveis de gerência e grau influência
  nas decisões. Com naturalidade estas comunidades formam-se, dividem-se, criam
  comunidades menores e dependentes, e dissolvem-se na descontinuação de
  projetos, sem perder a alta flexibilidade e capacidade inovativa.

  As escolhas possíveis para o mercado corporativo em relação ao software livre
  parecem ser o combate direto à sua ideologia -- frente que parece perder
  força com a intensificação da cultura colaborativa, ou o uso deste modelo
  segundo estratégias de distribuição de código -- prática que tem sido cada
  vez mais recorrente. As novas adaptações para tornar a distributividade de
  código rentável por si só normalmente equilibram-se em um limiar entre o
  livre e o proprietário, entre o permissivo e o proibido, entre o
  \emph{copyright} e o \emph{copyleft}. Toda e qualquer retórica revolucionária
  que já foi anteriormente atribuída ao software livre parece esvair-se com a
  adaptação capitalista à sua realidade. Embora a popularização de uma
  tecnologia e seu crescimento sejam pautados nos modelos livres, as práticas
  econômicas tradicionais sempre permanecem de uma maneira ou outra com a
  informação encapsulada em uma fração comodificada.

  Os conflitos tornam-se evidentes quando existem tentativas de proteger o
  aspecto verdadeiramente livre dos softwares ao manter as condições de livre
  redistribuição -- uma iniciativa geralmente tomada pelas comunidades através
  do entendimento de que o monopólio sobre um projeto de software livre é
  prejudicial porque acaba tendendo no atendimento de demandas comerciais
  centradas no negócio do monopolista, e deixam de atender um interesse mais
  amplo e que poderia melhor estimular a inovação e popularização. Neste
  contexto, governos e entidades sem fins lucrativos -- como as fundações e
  cooperativas de desenvolvimento, acabam atuando como importantes reguladores
  na viabilização de interesses mais coletivos.

  O software livre opera tanto `dentro quanto fora' do capitalismo, e é capaz
  de mover diferentes formas mais criativas e dinâmicas de acumulação, como
  também pode promover meios libertadores da inserção tecnológica na sociedade.
  Ao assistir as grandes corporações apropriando-se de seus ideais e
  construindo verdadeiros impérios econômicos, não podemos simplesmente ignorar
  e nem deixar de fomentar o potencial revolucionário de um único desenvolvedor
  autônomo, empregando sozinho sua capacidade intelectual em busca de uma
  alternativa, pois foi nestas mesmas condições que boa parte dos mais
  importantes softwares livres surgiram.

  A tentativa de buscar traços marxistas na totalidade da tecnologia da
  informação não exclui nenhuma outra condição presente nas demais
  complexidades do sistema capitalista. Para que um computador de última
  geração possa executar um aplicativo segundo todas as premissas da
  liberdade de conhecimento, é provável que diversas práticas desumanas tenham
  sido empregadas durante a fabricação deste dispositivo, tal como a geração de
  impactos ambientais irresponsáveis e a utilização de trabalho escravo;
  sempre obedecendo a lógica monopolista e globalizada que permite uma
  maximização da acumulação de valor. O materialismo histórico percorre
  inúmeros escopos e são de incrível diversidade, e acaba revelando no software
  livre somente uma parcela de um senso de igualdade social.

  Embora o software livre não garanta uma economia da informação igualitária
  que pluralize o acesso à tecnologia e que ofereça a todos as mesmas
  condições, uma sociedade que verdadeiramente incorpore estas características
  não somente utilizará códigos livres com exclusividade, mas também não verá
  sentido na propriedade do conhecimento em toda a extensão da atividade
  humana.
