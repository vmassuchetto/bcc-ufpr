\documentclass[a4paper,12pt]{article}

\usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[portrait,a4paper]{geometry}
\usepackage{abntcite}
\usepackage[hyphens]{url}
\usepackage[none]{hyphenat}
\usepackage{indentfirst}

\title{Pré-Projeto: Trabalho de Graduação 1 \\
    Uma Proposta Marxista de Implementação de \\
    Políticas Públicas Para Software Livre}

\author{Vinicius Massuchetto \\
    \emph{vam06@inf.ufpr.br}}

\date{Segundo Semestre de 2011}

\begin{document}

\sloppy

\maketitle

\tableofcontents

\section{Introdução}

  O software da maneira como o conhecemos é um bem bastante peculiar na
  sociedade atual. Ao contrário de outros produtos que são comercializados há
  centenas de anos seguindo a mesma metodologia de troca, o conjunto de
  informações contidas em um programa não obedece à uma série de regras de
  mercado que até então pareciam ser aplicáveis a qualquer outra mercadoria.

  Desta maneira, a comercialização do software -- ou da informação, quando
  tratamos o software de uma maneira mais ampla -- levou à criação de novos
  mecanismos regulatórios respaldados pelo estado e suas instituições
  legislatórias. Há uma tendência destes mecanismos, de alguma forma, para
  aplicar ao escopo da tecnologia da informação determinações já consolidadas
  nas demais relações econômicas.

  O software livre surgiu de uma maneira mais informal para atender demandas
  acadêmicas, e foi aos poucos integrando-se ao mercado e hoje mostra-se como
  uma alternativa viável às mais diversas aplicações. Possui como aspecto
  central a garantia de distribuição das informações que o compõem.

  Hoje, estes softwares acompanham -- via de regra -- uma licença que comumente
  é categorizada como uma \emph{licença copyleft}, que garante a livre cópia e
  redistribuição do software, impedindo que as pessoas ou organizações possam
  apropriar-se dele de maneira exclusiva.

  Se para garantir que o software fosse inserido nas condições tradicionais de
  mercado se fez necessária a criação de mecanismos legais, o que pode-se fazer
  quando estes mesmos mecanismos são utilizados para proteger o livre acesso à
  informação?

  Neste contexto, o aspecto central que torna o software livre especial é
  exatamente a sua capacidade de modificar e redistribuir as relações
  econômicas até então existentes em outros mercados. O paradigma de
  comercialização de software deixa de ser somente fundamentado por premissas
  que se aplicavam até então a produtos materiais.

  A forte ideologia presente nos fundamentos que sustentam a redistributividade
  de código foi rapidamente absorvidas por parcelas da sociedade que se
  identificam com as causas da liberdade de conhecimento, e tão logo os
  discursos em torno destas ideologias tornaram-se pautas permanentes dos mais
  variados movimentos sociais.

  Ao perceber benefícios como redução de custos e garantia do livre trânsito de
  tecnologias, e também impulsionados pelo compartilhamento de ideologias e
  acúmulo das discussões ocorridas nos movimentos sociais, diferentes esferas
  do poder público iniciam um processo de investimentos para desenvolvimento e
  implementação de softwares livres que viessem a atender suas demandas
  administrativas.

  Embora sejam ainda encaradas de maneira sazonal -- dada a rotatividade e
  suscetividade nos três poderes -- estas políticas têm sido implementadas com
  diferentes graus de sucesso por todo o país.

  Para os militantes da área, é evidente a falta de um projeto estruturante a
  nível de país que venha a estabelecer a utilização do software livre de
  maneira efetiva, o que deveria ser viabilizado como uma política pública
  sólida e declarada.

\section{Justificativa}

  As principais premissas do software livre são atendidas mediante ideologias
  que possuem forte correspondência no campo artístico, filosófico, político e
  social. Utilizar estudos históricos com conceitos deixados por pensadores de
  outras épocas é uma prática comum para a formação dos movimentos sociais, e
  consequentemente para a cobrança de políticas públicas.

  Já antes dos primeiros computadores, com a vertente iluminista da filosofia
  da liberdade no século 18, este tema em concepções mais modernas já era
  centro de debates em busca da compreensão da complexidade humana
  \cite{Bristow2011}.

  Em tempos em que as monarquias possuíam alto sigilo sobre o progresso
  tecnológico, suas políticas e informações \cite{Dorigo2005}, o início do
  pensamento sobre o quê de fato seria a liberdade do homem toma grande
  importância no questionamento das lógicas protecionistas. Friedrich Schiller,
  por exemplo, cita entre sua coleção de cartas a seguinte passagem:

  \begin{quote}

    A cultura, longe de nos dar liberdade, somente desenvolve novas
    necessidades ao longo do tempo. As correntes do mundo físico nos apertam
    cada vez mais de modo que o medo da perda extingue até o mais ardente
    impulso em direção à inovação, enquanto a obediência passiva é mantida como
    a maior das sabedorias da vida \cite{SchillerLetters1909}.

  \end{quote}

  Inúmeros filósofos, matemáticos e outros cientistas -- dentre eles os
  precursores da revolução francesa -- viriam a trabalhar sobre as primeiras
  perspectivas do que se conhece sobre direitos humanos. Os trabalhos feitos
  por eles foram importantes para a inserção do uso da razão em temáticas
  relacionadas à livre religião, anti-escravismo, anti-colonialismo e liberdade
  de expressão. Formava-se uma nova concepção do que se entendia pelo papel de
  cada indivíduo na sociedade, e quebrava-se a lógica medieval do pensar
  \cite{Verhoeven2011}.

  É evidente que dentre essa movimentação ideológica estava presente uma
  intensa preocupação intelectual com o que se entende pela liberdade do homem,
  e como ela se expressa e é tratada pela sociedade juntamente com os demais
  aspectos culturais \cite{Bristow2011}.

  Estas concepções diferenciadas culminaram em mobilizações populares bastante
  expressivas politicamente e que vieram a moldar todo o pensar ocidental até
  os dias de hoje. Tem-se que o trabalho destas pessoas é atualmente o ponto de
  partida implicitamente presente em qualquer análise da sociedade que se
  queira realizar \cite{Bristow2011}.

  O amadurecimento das ideias iluministas possibilitou a expansão da aplicação
  de conceitos filosóficos liberais à praticamente todas as áreas de interação
  social existentes na época. Dentre os estudos resultantes está a importante
  obra de Georg Hegel. Embora a integração formal de Hegel à vertente
  iluminista não ser um consenso entre os estudiosos da área, existem fortes
  relações entre os conceitos por ele colocados e a herança dos pensadores
  iluministas \cite{Lessa2007}.

  Apesar das críticas posteriores ao seu trabalho por outros filósofos que
  ganharam notoriedade por suas ideias, um aspecto a ser relevado da obra de
  Hegel são as suas relações entre indivíduo e mundo, até porque mais tarde
  surgiriam interpretações de sentido revolucionário destas relações, e que
  levariam à questionamentos fundamentais no campo político quanto à percepção
  do estado, do trabalho e da produção de bens \cite{Lessa2007}.

  Independentemente, o que ocorreu foi que, com justificativas humanistas à
  mão, o modelo vigente de exploração dos trabalhadores viria a ser questionado
  pelos pensadores, e que estes acabariam por iniciar a formulação de toda uma
  nova gama de interpretações voltadas especificadamente às relações de mercado
  e de trabalho.

  Neste contexto histórico, o economista e filósofo Karl Marx elaborou estudos
  a partir de uma abordagem metodológica que, quando tem seus conceitos
  colocados pela dialética, possuem boa aceitação em relação ao esclarecimento
  das lógicas de mercado do sistema capitalista e acabam por não só subsidiar
  ideologicamente as organizações sociais segundo um método científico, mas
  também por sugerir uma agenda política para a implantação de sistemas de
  poder diferenciados que venham a dividir de maneira justa os bens produzidos
  por uma sociedade \cite{Giannotti1996}.

  Em vida, Marx foi muito mais conhecido como militante do que como filósofo.
  Sua obra foi citada por estudiosos e influenciou movimentos operários de
  maneira substancial somente a partir de sua morte. Embora existam
  argumentações que afirmam não ser mais possível fazer interpretações de fatos
  atuais através dos conceitos de sua obra -- já que a sociedade passou por
  numerosas e intensas mudanças desde sua época, não é difícil identificar as
  correspondências do mundo de hoje com os conceitos centrais deixados por Marx
  \cite{Meksenas2008}.

  Muito discute-se sobre a crise da teoria marxista. Diversos governos que
  atribuíram os nomes "marxismo" e "comunismo" às suas políticas mostraram de
  diferentes formas e por diferentes razões -- assim como os governos
  capitalistas -- uma profunda inaptabilidade à manutenção de uma ordem social
  próspera.  Encaixam-se aí a falência da União Soviética, a abertura econômica
  de forma liberal pela China, e os governos comunistas da Ásia e da África
  \cite{Giannotti2000}.

  Porém, uma teoria não se invalida a partir dos fracassos históricos daqueles
  que se utilizaram de seu nome ou de seus conceitos. José Giannotti --
  professor da USP reconhecido pela notória crítica marxista -- justifica a
  validade deste tipo de estudo em contextos atuais:

  \begin{quote}

    O colapso [dos países socialistas] evidenciaram que a luta contra as
    misérias, instaladas pelo sistema capitalista, não implica qualquer
    compromisso com partidos comunistas de cunho leninista. Esse colapso reduz
    a pó a vulgata marxista, mas não impede que se continue a estudar as
    representações e as relações sociais da ótica do metabolismo que o homem
    mantém com a natureza, em suma, daquela que vê as relações sociais de
    produção imbricadas com o desenvolvimento das forças produtivas.

    (..)

    A obra escrita ilumina-se a partir de certas perspectivas históricas, de
    certos vieses que alimentam modos de pensar e de ver, inscritos em nosso
    cotidiano \cite{Giannotti2000}.

  \end{quote}

  E não só pela validade da elucidação teórica se justifica uma análise
  marxista. Embora o sistema capitalista tenha se reformulado, se globalizado e
  adquirido novas dinâmicas de exercício de poder devido ao avanço tecnológico
  e à sociedade da informação, temos os mesmos elementos conceituais mantendo
  os mesmos padrões: o trabalhador assalariado, a centralização do lucro
  privado, a mais-valia e a grande desigualdade nas relações de produção
  \cite{Meksenas2008}. A correlação destes conceitos nas dinâmicas de mercado
  do software livre podem vir a mostrar interpretações úteis no entendimento
  das mudanças sociais promovidas pela era da informação.

  No entanto, embora o marxismo seja uma forte vertente nos estudos sociais,
  não são muitos os autores que dedicam-se em aplicar os conceitos
  estabelecidos em uma análise da sociedade da informação, quanto menos das
  práticas de proteção da informação e do software livre. Por outro lado, quem
  escreve sobre propriedade intelectual e o impacto da tecnologia na sociedade
  também dificilmente utiliza conceitos puramente marxistas em suas análises.

  A principal suspeita sobre o marxismo em relação à este assunto dá-se pela
  percepção do imaginário coletivo de que os sistemas de informação aos poucos
  substituirão os trabalhadores, e que tão logo não haverá campo para aplicação
  de uma análise marxista. Uma interpretação desta natureza peca em essência
  por deixar de analisar um pouco mais profundamente sobre o que é de fato a
  informação, e quais suas demandas de poder, produção e consumo em relação à
  sociedade \cite{Sodenberg2002}.

  \begin{quote}

    O marxismo oferece um bom modelo teórico para análise das contradições
    inerentes do regime de propriedade intelectual. O sucesso do software livre
    em trabalhar fora do sistema comercial de software é uma amostra do que foi
    descrito por Marx há mais de 150 anos sob as formalizações de força
    produtiva e de intelecto geral.

    (..)

    A história não se resume ao levante das forças produtivas que foram
    convenientemente mapeadas pelos exemplos do materialismo histórico, mas são
    conflitos protagonizados por atores sociais, dentre eles o movimento do
    software livre e sua característica especial de desafiar a dominação do
    capital sobre o desenvolvimento tecnológico \cite{Sodenberg2002}.

  \end{quote}

  Tem-se, portanto, que as perspectivas marxistas são de razoável utilidade
  para a análise da estrutura econômica e social proposta pelo software livre,
  e que a correlação desta teoria com a prática pode vir a mostrar visões
  esclarecedoras sobre o que se entende pela liberdade de conhecimento, e quais
  as mudanças possíveis na sociedade a partir da implementação de políticas que
  valorizem iniciativas relacionadas à cultura da livre informação.

\section{Objetivo}

  O objetivo do trabalho é realizar um estudo sobre a propriedade intelectual e
  seus desdobramentos sociais com base em conceitos marxistas, a fim de
  identificar e organizar uma base teórica que venha a ser utilizada no
  subsídio de escolhas político-estratégicas em relação à adoção de software
  livre pelas diferentes esferas do poder público, e que possam também
  estimular o desenvolvimento aberto de tecnologias tanto no âmbito público
  quanto privado.

  A motivação para efetuar um estudo teórico com viés marxista sobre o software
  livre reside tanto na falta de uma discussão acadêmica interdisciplinar que
  venha a explorar os aspectos políticos e filosóficos da ciência da
  computação, quanto pelo sucesso que alguns autores já obtiveram ao realizar
  esta combinação, interpretando de maneira interessante um novo conjunto de
  interações sociais trazidos pela era da informação.

\section{Metodologia}

\subsection{Plano de Trabalho}

  Não são muitos os autores que dedicaram-se à especificidade do assunto a que
  este trabalho propõe-se. Ao invés disso, encontram-se estudos sobre assuntos
  periféricos e com conceituação marxista implícita.

  Portanto, a referência inicial é tanto a própria obra de Marx quanto aquelas
  de autores que mais tarde vieram a discuti-la. O uso deste segundo conjunto
  de obras é especialmente importante quando se trata de conceitos não muito
  aprofundados nas obras originais, mas que seriam importantes para o estudo da
  propriedade intelectual.

  Existem certas especulações com diferentes vieses a partir de pontos não
  muito explorados na extensa série de publicações de Marx, e estas visões
  tornam-se úteis na interpretação e adaptação de certos conceitos para outras
  realidades, como é o caso daquelas apresentadas pela sociedade da informação.

  Assim, estas leituras serão usadas para a identificação de conceitos e a
  discussão do modelo político-econômico colocado pelo software livre, e como
  este modelo acaba por diferenciar-se do modelo comercial tradicional, além de
  apontar quais são as consequências -- e principalmente os benefícios --
  destas diferenças para o mercado e para a sociedade, sempre procurando
  aproximar o papel do estado em relação às decisões que venham a promover
  melhorias neste sentido.

\subsection{Levantamento Bibliográfico}

  Não somente sobre o software livre propriamente dito se desenvolve a
  literatura no aspecto da liberdade de informação. Uma variedade de temas deve
  ser considerado para encontrar referências úteis sobre o assunto.

  Em relação a um viés mais prático, o software livre encontra-se na literatura
  primeiramente sob um aspecto técnico. Muitos projetos abertos são referência
  científica para o desenvolvimento de estudos, simulações e desempenho de
  aplicações. Trabalhos com estes perfis geralmente não adentram o âmbito
  político, mas servem para demonstrar a importância e consistência prática e
  acadêmica que o software livre possui.

  Em vias mais profissionais, há também trabalhos na área de administração de
  recursos de tecnologia da informação, e que concentram-se principalmente em
  análises de viabilidade a partir de estudos de caso na implantação de
  softwares livres. Muitos destes estudos dão-se com empresas públicas em
  gestões que resolveram adotar tal tipo de software, mas também são feitos com
  empresas privadas que tomaram as mesmas decisões.  Os textos normalmente
  contam com questionários que indicam as principais dificuldades deste tipo de
  implantação em seus aspectos técnicos e de interação com os usuários.

  Um ponto interessante da área de administração são os estudos sobre
  cooperativismo. Como o desenvolvimento aberto de software não deixa de ser
  uma atividade que possui um perfil parecido à uma organização descentralizada
  de indivíduos ou empresas, correlações principalmente teóricas podem ser
  feitas entre estudos desta natureza. Em geral estes trabalhos também
  consistem em estudos de caso, tomando como referência os benefícios obtidos
  pelos associados das cooperativas aos longo do tempo.

  Existem também análises mais amplas na área de economia, e que analisam o
  mercado de software livre a nível de país ou continente. Um grande valor
  destes trabalhos consiste no fornecimento de um panorama dos mercados,
  expondo perfis das empresas desenvolvedoras e dos usuários, assim como a
  significância que eles possuem para o mercado de tecnologia da informação de
  modo geral.

  Ainda no campo da economia, um outro tema interessante e que há de fornecer
  subsídios para discussão são os trabalhos sobre os movimentos de economia
  solidária. A contraposição às lógicas econômicas e de propriedade formais
  seguem um modelo similar ao que o software livre emprega. Desta forma, o
  referencial prático e teórico exposto por estes trabalhos pode fornecer bons
  direcionamentos para análises sobre propriedade intelectual.

  Em ciências jurídicas é comum encontrar discussões acerca de como as
  legislações tratam a proteção da propriedade intelectual, e como o software
  livre é inserido neste contexto. Esta na verdade foi uma das primeiras áreas
  a se desenvolver uma discussão prática do desenvolvimento aberto, já que era
  preciso garantir que os softwares liberados permanecessem livres através de
  licenças específicas, e justamente por causa disso -- e também pelos
  interesses comerciais que residem na atividade -- nota-se um amadurecimento
  das tecnicalidades jurídicas para a cópia, distribuição, combinação e
  propriedade de software, enquanto outras áreas -- como os direitos do
  consumidor em relação à venda casada de softwares proprietários, por exemplo
  -- ainda estão em processo de desenvolvimento.

  Esta área também se expande um pouco para a proteção da cultura livre e
  mobilidade digital, mesclando-se com o ramo artístico e cultural, e
  produzindo estudos que tratam de assuntos mais atuais, como a privacidade na
  internet e o tratamento do livre compartilhamento digital frente à indústria
  do entretenimento. O grupo de licenças \emph{GPL} e \emph{Creative Commons}
  ocupam uma posição de destaque neste espaço, e são comumente citadas ou até
  mesmo objetos exclusivos de estudos centralizados.

  Outra categoria de trabalhos realizados está na área da educação social, que
  por sua vez tratam sobre projetos de inclusão digital e perspectivas da
  educação na era da informação. A literatura desta área é especialmente
  importante por comumente realizar uma aproximação teórica que procura
  subsidiar a adoção de software livre nos projetos em questão, e também
  discutem o direito à comunicação e o papel do poder público em relação ao
  fornecimento de políticas que o favoreçam. A produção brasileira destaca-se
  especialmente neste assunto, já que a implantação de telecentros e de
  políticas de acessibilidade digital têm sido incentivadas de diferentes
  formas pelo poder público nos últimos anos.

  Este campo também tem abrangido novas temáticas que surgem a partir de
  demandas pedagógicas como softwares educacionais, ou mesmo questões de
  interatividade com o ambiente escolar, como é o caso do cyberbullying.

  Há um outro número de trabalhos que são uma intersecção entre as áreas da
  educação, da comunicação e da psicologia social que tratam do fenômeno de
  construção do conhecimento em rede. Este debate intensificou-se bastante a
  partir da criação da internet, e tem parte de seus estudos direcionado à
  interpretação da propriedade do conhecimento, ou o \emph{Commons}, como já
  é definido.

  Sendo o objetivo do trabalho a construção de um referencial teórico para
  soluções práticas com base em conceitos deixados por autores do século 19,
  obviamente não se pode excluir a literatura clássica e principalmente obras
  posteriores que as discutem.

\pagebreak

\bibliography{pre}

\end{document}
