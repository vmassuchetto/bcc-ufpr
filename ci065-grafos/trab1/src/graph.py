#!/usr/bin/python
# -*- coding: UTF-8 -*-

class Node:
    ''' Classe dos vértices (ou nós) de um grafo. É basicamente um 
    identificador inteiro e seus atributos. '''

    def __init__(self, id = 0):
        import decimal
        self.id = decimal.Decimal(id)
        self.visited = 0
        self.parent = 0
        self.start_timestamp = 0
        self.stop_timestamp = 0

class Edge:
    ''' Classe que representa as arestas de um grafo. Uma instância possui 
    somente um par de vértices e seus atributos. '''

    def __init__(self, nodes = list()):
        self.nodes = nodes
        self.visited = 0

class Graph:
    ''' Classe que compõe um grafo. É composto de uma lista de vértices e 
    arestas, dentre outros atributos. '''

    def __init__(self):
        self.nodes = list()
        self.edges = list()
        self.states = list()

    def insert_node(self, id = 0):
        ''' Fornece um identificador inteiro para inserir um nó no Grafo. '''

        if id in [node.id for node in self.nodes]:
            return False

        node = Node(id = id)
        self.nodes.append(node)
        self.nodes = sorted(self.nodes, key = lambda node:node.id)

    def insert_edge(self, nodes = list()):
        ''' Fornece-se um par de objetos de instanciação de nós para
        constituir uma aresta. '''

        nodes = sorted(nodes,
                       key = lambda node:node.id if node != None else Node(0))

        if len(nodes) != 2:
            return False

        if nodes[0].id == nodes[1].id:
            return False

        if len(self.edges) > 0:
            for edge in self.edges:
                if (nodes[0].id, nodes[1].id) == \
                   (edge.nodes[0].id, edge.nodes[1].id):
                    return False
        
        edge = Edge(nodes = nodes)
        self.edges.append(edge)

    def reset(self):
        ''' Zera todas marcações no Grafo. '''
        
        self.reset_node_marks()
        self.reset_edge_marks()
    
    def reset_node_marks(self):
        ''' Zera as marcações em nós. '''
        
        for n in self.nodes:
            n.visited = 0
            n.parent = 0
            n.start_timestamp = 0
            n.stop_timestamp = 0

    def reset_edge_marks(self):
        ''' Zera as marcações em arestas. '''
        
        for e in self.edges:
            e.visited = 0

    def get_node(self, id):
        ''' Seleciona um nó de acordo com um número inteiro fornecido. '''
        
        import decimal
        for n in self.nodes:
            if n.id == decimal.Decimal(id):
                return n
        return None

    def get_edge(self,nodes = list()):
        ''' Seleciona uma aresta de acordo com um par de nós fornecidos, 
        serve também para verificar se uma aresta existe ou não. '''
        
        edge = sorted(nodes, key = lambda node:node.id)
        for e in self.edges:
            if (e.nodes[0].id, e.nodes[1].id) == (edge[0].id, edge[1].id):
                return e
        return False

    def create_graph(self, n):
        ''' Dado um inteiro, cria os nós de um grafo. '''
        
        import decimal
        for i in range(1,decimal.Decimal(n) + 1):
            self.insert_node(i)

    def read_edgelist(self):
        ''' Lê lista de arestas da entrada padrão. '''
        
        import sys
        n = sys.stdin.readline()
        self.create_graph(n)

        for line in sys.stdin.readlines():
            ids = line.rstrip('\n').split(' ')
            self.insert_edge(nodes = [self.get_node(ids[0]),
                                      self.get_node(ids[1])])

    def get_node_edges(self, node):
        ''' Seleciona todas as arestas que ligam um dado nó. '''
        
        edges = list()
        for edge in self.edges:
            if node.id in [n.id for n in edge.nodes]:
                edges.append(edge)
        return sorted(edges, key = lambda edges:edges.nodes)

    def get_node_neighbours(self, node):
        ''' Seleciona todos os vizinhos de um nó. '''
        
        neighbours = list()
        for edge in self.edges:
            if node.id in [n.id for n in edge.nodes]:
                for e in edge.nodes:
                    if e.id != node.id:
                        neighbours.append(e)
        return sorted(neighbours, key = lambda node:node.id)

    def render_animation(self, outfile):
        ''' Durante uma busca, se houver a opção de gerar uma saída animada 
        para as operações no Grafo, este método utiliza o atributo `states`, 
        que são os passos da busca representados em linguagem Graphviz. '''
        
        import sys
        import pydot
        import subprocess
        import decimal
       
        i = 0
        statefiles = list()
        total = len(self.states)
        decimal.getcontext().prec = 3
        
        for s in self.states:
            i += 1
            p = i/decimal.Decimal(total)*100
            print 'Gerando animação...', p,"%          \r",
            sys.stdout.flush()
            
            tempfile = 'tmp.graph'
            f = open(tempfile, 'w')
            f.write(s)
            f.close()
            
            statefile = 'tmp.graph.%s.jpg' % (i)           
            cmd = 'dot -Tjpg -Kfdp %s -o %s' % (tempfile, statefile)
            subprocess.call(cmd, shell = True)           
            
            out = '' if i == 1 else outfile
            cmd = 'convert -delay 50 -loop 0 -density 200 \
                   %s %s %s' % (out, statefile, outfile)
            subprocess.call(cmd, shell = True)
            subprocess.call('rm %s' % (statefile), shell = True)

        subprocess.call('rm ' + tempfile, shell = True)
        print ''

    def render_graphviz(self, parent = Node(0)):
        ''' Para um dado grafo, gera a representação em Graphviz de seu atual 
        estado. '''
        
        import pydot
        import subprocess
        
        graph = pydot.Graph(size = '5,5')
        graph.set_strict(False)
        
        for n in self.nodes:
            node = pydot.Node(str(n.id))
            if n.visited == 1 or n.id == parent.id:
                node.set_fontcolor('white')
                node.set_style('filled')

            if n.visited == 1:
                node.set_fillcolor('#FF4545')
            
            if n.id == parent.id:
                node.set_fillcolor('#007032')
            
            graph.add_node(node)
        
        for e in self.edges:
            edge = pydot.Edge(pydot.Node(str(e.nodes[0].id)),
                              pydot.Node(str(e.nodes[1].id)))
            edge.set_arrowhead('none')
            if e.visited == 1: edge.set_color('#CF0000')
            else: edge.set_color('#A1A1A1')
            graph.add_edge(edge)
        
        return graph.to_string()

    def render_text(self):
        ''' Gera a saída padrão da árvore em forma de texto em colunas, 
        segundo a especificação do trabalho.'''
        
        for n in self.nodes:
            print "%d %d %d %d" % (n.id, n.parent.id if n.parent != 0 else 0,
                                   n.start_timestamp, n.stop_timestamp)

class Search:
    
    def __init__(self, graph, start, stop = None):

        self.graph = graph
        self.start = start
        self.stop = stop

    def breadth(self):
        ''' Busca em largura. '''

        timestamp = 0
        node = { 'node': self.graph.get_node(self.start), 'parent': Node(0)}
        queue = [node]

        while len(queue) > 0:
           
            ''' Para busca em largura trata-se a lista como fila. '''
            q = queue.pop(0)
            
            if 'stop' in q:
                timestamp += 1
                q['stop'].stop_timestamp = timestamp
                continue

            if q['node'].visited == 1:
                continue

            node, node.parent = q['node'], q['parent']
            node.visited = 1
            timestamp += 1
            node.start_timestamp = timestamp

            if self.stop != None:
                if node.id == self.stop:
                    return node

            edge = sorted((node, node.parent), key = lambda node:node.id)
            edge = self.graph.get_edge(edge)
            if edge: edge.visited = 1
                       
            q = list()
            for n in self.graph.get_node_neighbours(node):
                q.append({'node': n, 'parent': node})
            q.append({'stop': node})
            queue.extend(q)
            
            if options.animation != None:
                self.graph.states.append(
                    self.graph.render_graphviz(parent = node.parent))

        return True

    def depth(self):
        ''' Busca em profundidade. '''

        timestamp = 0
        node = { 'node': self.graph.get_node(self.start), 'parent': Node(0)}
        stack = [node]

        while len(stack) > 0:

            ''' Para busca em profundidade trata-se a lista como pilha. '''
            s = stack.pop()

            if 'stop' in s:
                timestamp += 1
                s['stop'].stop_timestamp = timestamp
                continue

            if s['node'].visited:
                continue

            node, node.parent = s['node'], s['parent']
            node.visited = 1
            timestamp += 1
            node.start_timestamp = timestamp

            if self.stop != None:
                if node.id == self.stop:
                    return node

            edge = sorted((node, node.parent), key = lambda node:node.id)
            edge = self.graph.get_edge(edge)
            if edge: edge.visited = 1
                      
            s = list()
            for n in self.graph.get_node_neighbours(node):
                s.append({'node': n, 'parent': node})
            stack.append({'stop': node})
            s.reverse()
            stack.extend(s)
            
            if options.animation != None:
                self.graph.states.append(
                    self.graph.render_graphviz(parent = node.parent))

        return True


if __name__ == "__main__":

    import optparse
    parser = optparse.OptionParser()
    parser.add_option('--depth', action = 'store_true', dest = 'depth',
                      help = u'Efetua uma busca em profundidade')
    parser.add_option('--breadth', action = 'store_true', dest = 'breadth',
                      help = u'Efetua uma busca em largura')
    parser.add_option('--animation', action = 'store_true', dest = 'animation',
                      help = u'Renderiza animação de busca no grafo')
    (options, args) = parser.parse_args()
    if not (options.depth or options.breadth):
        parser.print_help()
        exit()

    g = Graph()
    g.read_edgelist()
    s = Search(g, 1)
    
    if options.breadth:
        s.breadth()
    elif options.depth:
        s.depth()

    g.render_text()
    if options.animation != None:
        if options.breadth: outfile = 'breadth.gif'
        elif options.depth: outfile = 'depth.gif'
        g.render_animation(outfile)

