/**
 * @mainpage Segundo Trabalho Prático de Algoritmos e Teoria dos Grafos
 * 
 *           Determinação de k-conexidade e da cobertura de vértices.
 * 
 * @authors GRR20000000 Diego Trevisan         <br>
 *          GRR20000000 Michel                 <br>
 *          GRR20063784 Vinicius Massuchetto   <br>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Estrutura de representação de um grafo.
 */
typedef struct _graph {
    int **m;     /**< Matriz de adjacência **/
    int *v;      /**< Vetor de vértices    **/
    int n;       /**< Dimensão da matriz   **/
} *graph;
