#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "biblioteca_k-conexo.h"



/*função que vai verficar se é k-conexo, recebe uma matriz, a ordem dessa matriz, e um inteiro k*/
int kconexo(int **mat, int n, int k)
{
	int comb[k];
	int i,j;
	int *graus;
	int **mat_aux;

	mat_aux = (int **) malloc(n*sizeof(int *));
	for(i=0; i<n; i++)
		mat_aux[i] = (int *) malloc(n*sizeof(int ));
	
	graus = (int *) malloc(n*sizeof(int));

	if ( k >= n)
		return (0);
	if ( k == n-1)
		return (0);
	else
	{
		/*cria a primeira combinação com k elementos*/
		for (i = 0; i < k; i++)
			comb[i] = i;
		
		for (i = 0; i < n; i++)
			for (j = 0; j < n; j++)
				mat_aux[i][j] = mat[i][j];
		
		/*seta com 0 os k vertices na matriz*/
		for (i = 0; i < k; i++)
			for (j = 0; j < n; j++)
				mat_aux[ (comb[i]) ][j] = 0;

		if ( (verifica_caminhos(graus, mat_aux, n, k, comb) ) == 1)
			return (1);
	
		/*calcula as proximas combinações com k elementos, seta com 0 os k vertices na matriz e
		verifica (depois de tirar a combinação de k vertices do grafo) se existe caminho 
		de qualquer vertice para qualquer vertice atraves da busca em profundidade */
		while ( proxima_comb (comb, k, n) )
		{
			for (i = 0; i < n; i++)
				for (j = 0; j < n; j++)
					mat_aux[i][j] = mat[i][j];
		
			for (i = 0; i < k; i++)
				for (j = 0; j < n; j++)
					mat_aux[ (comb[i]) ][j] = 0;

			if ( (verifica_caminhos(graus, mat_aux, n, k, comb) ) == 1)
			return (1);
	
		}	
		return(0);
	}
}

/*busca em profuncidade. Guarda todos os vertices visitados no vetor vertices_v (n_vertices tem o tamanho desse vetor*/
void busca_profundidade(int **mat, int v, int n, int vertices_v[], int *n_vertices)
{
	int i;
	int w;
	int vertices[n];
	for (i=0; i<=n; i++)
		vertices[i] = 0;
	
	for (w=1;w<=n;w++)
	{
		if ( mat[v][w]==1)
		{
			if (vertices[w]==0)
			{
				vertices[v]=1;
				vertices[w]=1;
				mat[v][w]=2;
				mat[w][v]=2;
				vertices_v[(*n_vertices)] = v;
				vertices_v[(*n_vertices)+1] = w;
				*n_vertices = (*n_vertices)+2; 
				busca_profundidade(mat, w, n, vertices_v, n_vertices);
			}
			else
			{
				mat[v][w]=3;
				mat[w][v]=3;
				vertices_v[(*n_vertices)] = v;
				vertices_v[(*n_vertices)+1] = w;
				*n_vertices = (*n_vertices)+2; 
			}
		}
	}
}

/*função que faz a combinação de k elementos*/
int proxima_comb(int comb[], int k, int n) 
{
	int i = k - 1;
	++comb[i];
	while ((i > 0) && (comb[i] >= n - k + 1 + i)) 
	{
		i--;
		++comb[i];
	}

	if (comb[0] > n - k)
		return 0;

	for (i = i + 1; i < k; ++i)
		comb[i] = comb[i - 1] + 1;

	return (1);
}

/*lê a matriz de adjacencias*/
int **mapeia_matriz(int **mat, FILE *arq, int *n)
{
	char buffer[100];
	char aux[2];
	int tam;
	int i;
	int j;
	int ver = 0;
	aux[1] = '\0';

	fseek(arq, 1, SEEK_CUR);	
	fgets(buffer, 100, arq);
	tam = strlen(buffer);
  
	fseek(arq, -tam, SEEK_CUR);
  
	if(buffer[tam-1] == '\n')
		tam = tam-1;
	
	mat = (int **) malloc(tam*sizeof(int *));

  for(i=0; i<tam; i++)
		mat[i] = (int *) malloc(tam*sizeof(int ));

	for(i=0; i<tam; i++)
  	{
		fgets(buffer, 100, arq);
    		for(j=0; j<tam; j++)
    		{
    			aux[0] = buffer[j];
      			mat[i][j] = atoi(aux);
			if ( mat[i][j] == 1 )
			ver++;
		}
	}

	*n = tam;
	return mat;
}

/*verifica (depois de tirar a combinação de k vertices do grafo) se existe caminho 
de qualquer vertice para qualquer vertice atraves da busca em profundidade */
int verifica_caminhos(int *v, int **m, int tam, int k, int comb[])
{
  int i;
  int j;
	int l;
	int n_vertices;
	int vertices[tam+1];
	int vertices_v[tam*tam];
	
	for (i = 0; i < tam; i++)
	{
		for (j = 0; j < tam; j++)
		{		
			for ( l = 0; l < k; l++)		
				if ( (comb[l] == i) || ( comb[l] == j) )
					m[i][j] = 0;
		}
	}

	for (i=0; i < tam; i++)
		vertices[i] = 0;

	for (i=0; i < tam*tam; i++)
		vertices_v[i] = 0;
	n_vertices = 0;
	
	i=0;
	while ( i < tam )
	{ 
		if ( i == comb[i] )
			i++;
		else
		{
			busca_profundidade(m, i, tam, vertices_v, &n_vertices);
			break;
		}
	}

	/*verfica se dentro do vetor vertices_v existe algum vertice do grafo não
	foi encontrado na busca em profundidade (quer dizer que o grafo ficou desconexo*/
	for (l = 0; l < tam; l++)
	{
		for (i=0; (i < n_vertices) && ( l != vertices_v[i] ); i++)
			;
		if ( i == n_vertices)
		{
			for (j = 0; (j < k) && (l != comb[j]) ; j++)
				;
			if ( j == k )
				return (1);
		}
	}
	return (0);		

}


