#include "biblioteca_isomorfos.h"

main(int argc,char **argv)
{
  FILE *f=fopen(argv[1],"r");
  int **adjacente1;
  int **adjacente2;
  int *funcao;
  int n1;
  int n2;
  int i;

  adjacente1=mapeia_matriz(adjacente1,f,&n1);
  fseek(f,1,SEEK_CUR);
  adjacente2=mapeia_matriz(adjacente2,f,&n2);
  if(n1==n2)
    if(num_arestas(adjacente1,n1)==num_arestas(adjacente2,n1))
      if(teste_dos_graus(adjacente1,adjacente2,n1))
	{
	  funcao=(int *)malloc(n1*sizeof(int));
	  for(i=0;i<n1;i++)
	    funcao[i]=i;
	  permuta_vertices(adjacente1,adjacente2,n1,funcao,0);
	  printf("0\n");
	}
      else
        printf("0\n");
    else
      printf("0\n");
  else
    printf("0\n");
}
