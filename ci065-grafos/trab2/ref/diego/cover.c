#include "main.h"

void cover_read (graph g, int argc, char** argv) {
    
    FILE *input = stdin;
    if (argc > 1) {
        if (!(input = fopen (argv[1], "r"))) {
            printf ("Erro na leitura do arquivo");
            exit(1);
        }
    }

    int i, j;
    char c;
    char *l = malloc (1 * sizeof(int));
    
    g->n = 0;

    c = getc (input);
    while (c != '\n') {
        g->n++;
        l = realloc (l, g->n * sizeof(int));
        l[g->n - 1] = c - 48;
        c = getc (input);
    }

    g->m = malloc (g->n * sizeof(int));
    for (i = 0; i < g->n; i++)
        g->m[i] = malloc (g->n * sizeof(int));
    
    for (i = 0; i < g->n; i++)
        g->m[0][i] = l[i];

    for (i = 1; i < g->n; i++) {
        for (j = 0; j < g->n; j++)
            g->m[i][j] = getc(input) - 48;
        getc(input);
    }

    g->v = malloc (g->n * sizeof(int));
    for (i = 0 ; i <= g->n ; i++)
        g->v[i] = 1;

    free(l);
    return;
}

void cover_rmnodes (graph g, graph g1, graph g2, int n) {
    
    int i, j, k;
    
    // Aloca e copia g1 para g2
    g2->n = g1->n;
    g2->m = malloc (n * sizeof (int));
    for (i = 0 ; i < n; i ++) {
        g2->m[i] = malloc (n * sizeof (int));
        for (j = 0; j < n; j++)
            g2->m[i][j] = g1->m[i][j];
    }

    for (i = 0; (i < n) && !(g->v[i-1]); i++);
    i--;

    for (j = 0 ; j < n; j++) {
        if (!g->m[i][j]) continue;
        g2->v[j] = 0;
        for (k = 0 ; k < n ; k++) {
            g2->m[j][k] = 0;
            g2->m[k][j] = 0;
        }            
        g2->n--;
    }
    return;
}

void cover_rmnode (graph g1, graph g2, int n) {
    
    int i, j;
    
    // Aloca e copia g1 para g2
    g2->n = g1->n - 1;
    g2->m = malloc (n * sizeof (int));
    for (i = 0 ; i < n; i ++) {
        g2->m[i] = malloc (n * sizeof (int));
        for (j = 0; j < n; j++)
            g2->m[i][j] = g1->m[i][j];
    }

    // Remove nó de g2
    for (i = 0 ; i < n; i++) {
        if (!g1->v[i]) continue;
        g2->v[i] = 0;
        for (j = 0 ; j < n ; j++)
            g2->m[i][j] = g2->m[j][i] = 0;
        return;
    }
    return;
}

int *cover_test (graph g, int n) {
    
    if (!g->n || g->n ==1)
        return g->v;

    int i;
    int *si, *so;

    // Aloca e copia o grafo g para go
    graph go = malloc (sizeof(struct _graph));
    go->n = g->n;
    go->v = malloc (g->n * sizeof (int));
    for (i = 0; i < n; i++)
        go->v[i] = g->v[i];
    
    cover_rmnode (g, go, n);

    // Aloca e copia o grafo go para gi
    graph gi = malloc (sizeof(struct _graph));
    gi->n = go->n;
    gi->v = malloc (go->n * sizeof (int));
    for (i = 0; i < n; i++)
        gi->v[i] = go->v[i];

    cover_rmnodes (g, go, gi, n);

    so = cover_test (go, n);
    si = cover_test (gi, n);

    // Adiciona o vértice em si
    for (i = 0 ; i < n; i++) {
        if (g->v[i]) {
            si[i] = 1;
            break;
        }
    }
    
    // Compara e retorna o maior conjunto
    int si_size, so_size;
    for (i = 0; i < n; i++) {
        if (si[i]) si_size++;
        if (so[i]) so_size++;
    }

    if (si_size > so_size)
        return si;
    return so;
}

int main (int argc, char **argv) {

    graph g = malloc (sizeof(struct _graph));
    cover_read (g, argc, argv);
    
    int i;
    
    int *s = cover_test (g, g->n);

    for (i = 0 ; i < g->n ; i++)
        if (!s[i])
            printf ("%i\n", i+1);    
    
    return 0;
}
