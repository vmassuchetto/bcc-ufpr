/**
 * @mainpage Segundo Trabalho Prático de
 *           Algoritmos e Teoria dos Grafos
 * 
 * @authors GRR20000000 Diego Trevisan
 *          GRR20000000 Michel
 *          GRR20063784 Vinicius Massuchetto
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _graph {
    int **m;
    int *v;
    int n;
} *graph;


