#include <stdio.h>
#include <stdlib.h>
#include "estrutura.h"
#include "leitura.h"

/* Função de leitura com especialização para k-conexo */
unsigned int leitura_k(FILE *arq, grafo *g)
{
    unsigned int k;
    
    fscanf(arq, "%u%*c", &k);
    leitura(arq, g);
    
    return k;
}

/* Função verifica se existe um caminho entre dois vertices */
unsigned int caminho(grafo *g, unsigned char *visitados, unsigned int *nconexo, int v1, int v2)
{
    int i;

    if (visitados[v1] == 1)
        return 0;

    visitados[v1] = 1;
    if (v1 == v2) {
        (*nconexo)++;
        return 1;
    }
    
    for (i = 0; i < g->nVertices; i++)
        if (g->adjacencia[v1][i] == 1)
            if (caminho(g, visitados, nconexo, i, v2) == 1) {
                g->adjacencia[v1][i]=0;
                return 1;
            }

    return 0;
}

/* Função para verificar nível de conexidade de um grafo */
unsigned int conexo(grafo *g)
{
    unsigned int nconexo = 0, avalia, i;
    unsigned char *visitados = (unsigned char *)calloc(g->nVertices, sizeof(unsigned char));

    avalia = caminho(g, visitados, &nconexo, 0, 1);

    while (avalia != 0) {
        for (i = 0; i < g->nVertices; i++)
            visitados[i]=0;
        avalia = caminho(g, visitados, &nconexo, 0, 1);
    }

    free(visitados);
    return nconexo;
}

/* Função geradora de executável de k-conexo */
int main (int argc, char **argv)
{
    grafo g;
    unsigned int k;
    FILE *arq = stdin;
    
    if (argc > 1)
        if ((arq = fopen (argv[1], "r")) == NULL) {
            fprintf(stderr,"Erro na abertura do arquivo");
            exit(1);
        }
    
    k = leitura_k(arq, &g);

    if (conexo(&g) == k)
        printf("1\n");
    else
        printf("0\n");

    fclose(arq);
    return 0;
}
