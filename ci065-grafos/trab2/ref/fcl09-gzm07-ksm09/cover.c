#include "estrutura.h"
#include "leitura.h"
#include <malloc.h>
#include <stdio.h>

#define TRUE 1
#define FALSE 0

void leituraCobertura ( FILE *arq, grafo *g )
{
    leitura ( arq, g );
}

void imprimeCoberturaMinima ( unsigned short n_vertices, unsigned short *mis )
{
    unsigned short i;
    for ( i = 0 ; i < n_vertices ; i++ )
	// Quando o "mis[i] = 1", o vértice faz parte do conjunto independente
    // máximo, logo, não faz parte da Cobertura Mínima
        if ( !( mis[i] ) )
	        printf ( "%hu\n", i + 1 );
}

void aloca ( grafo *g, unsigned short n )
{
	unsigned short i;
	g -> adjacencia = malloc ( n * sizeof ( unsigned char* ) );
	for ( i = 0 ; i <= n; i ++ )
		g -> adjacencia[i] = malloc ( n * sizeof ( unsigned char ) );
}

void uniao ( unsigned short n_vertices, unsigned short *verticesG, unsigned short *s_in )
{
    unsigned short i;

    for ( i = 0 ; i < n_vertices ; i++ )
    {
		if ( verticesG[i] )
		{
			s_in[i] = 1;
			return;
		}
    }
    return;
}

unsigned short *maxsize ( unsigned short n_vertices, unsigned short *l1, unsigned short *l2 )
{
	unsigned short i;
	unsigned short tam_l1 = 0;
	unsigned short tam_l2 = 0;
	for ( i = 0; i < n_vertices; i++ )
	{
		if ( l1[i] )
			tam_l1++;
		if ( l2[i] )
			tam_l2++;
	}

	if ( tam_l1 > tam_l2 )
		return l1;
	else
		return l2;
}

void copiaG1_G2 ( unsigned short n_vertices, grafo *g1, grafo *g2 )
{
	unsigned short i, j;
	for ( i = 0 ; i < n_vertices ; i++ )
		for ( j = 0 ; j < n_vertices ; j++ )
			g2 -> adjacencia [i][j] = g1 -> adjacencia [i][j];
			
}

void copiaVetorVertices ( unsigned short n_vertices, unsigned short *origem, unsigned short *destino )
{
	unsigned short i;
	for ( i = 0; i < n_vertices; i++)
		destino[i] = origem[i];
	return;
}

void alocaVetorVertices ( unsigned short n_vertices, grafo *g )
{
	g -> vertices = malloc ( n_vertices * ( sizeof ( unsigned short ) ) );
}

void iniciaVetorVertices ( unsigned short n_vertices, grafo *g )
{
	unsigned short i;
	alocaVetorVertices ( n_vertices, g );
	for ( i = 0 ; i <= n_vertices ; i++ )
		g -> vertices [i] = 1;
}

void retiraVertice ( unsigned short n_vertices, grafo *g1, grafo *g2 )
{
	unsigned short i, j;
	aloca ( g2, n_vertices );
	g2 -> nVertices = g1 -> nVertices - 1;
	
	copiaG1_G2 ( n_vertices, g1, g2 );

	for (i = 0 ; i < n_vertices; i++ )
	{
		if ( g1 -> vertices[i] )
		{	
			g2 -> vertices[i] = 0;
			for ( j = 0 ; j < n_vertices ; j++ )
			{
				//zera linha
				g2 -> adjacencia [i][j] = 0;
				//zera coluna
				g2 -> adjacencia [j][i] = 0;
			}
			//esse return irá controlar quando parar, ou seja, zerou UM vértice pare;
			return;							
		}
	}
	return;
}

void retiraVizinhanca ( unsigned short n_vertices, grafo *g, grafo *g1, grafo *g2 )
{
	unsigned short i, j, k, linha;
	aloca ( g2, n_vertices );
	g2 -> nVertices = g1 -> nVertices;

	copiaG1_G2 ( n_vertices, g1, g2 );

	i = 0;
	linha = 0;
	do
	{
		linha = i;
		i++;
	}
	while( (i < n_vertices) && !(g->vertices[i-1]) );


	for (j = 0 ; j < n_vertices; j++ )
	{
		if ( g -> adjacencia[linha][j] )
		{
			g2 -> vertices[j] = 0;
			for ( k = 0 ; k < n_vertices ; k++ )
			{
				//zera linha
				g2 -> adjacencia [j][k] = 0;
				//zera coluna
				g2 -> adjacencia [k][j] = 0;
			}			
			g2 -> nVertices--;
			//aqui não temos o return como no retiraVertice, pois temos que zerar todos os vizinhos do vertice
		}
	}
	return;
}

unsigned short *calculaConjuntoIndependenteMaximo ( unsigned short n_vertices, grafo *g )
{
	if ( !(g -> nVertices) )
		return g -> vertices;

	if ( g -> nVertices == 1 )
		return g -> vertices;

	grafo g_in, g_out;
	unsigned short *s_in, *s_out;
	
	alocaVetorVertices ( n_vertices, &g_out );
	copiaVetorVertices ( n_vertices, g->vertices, g_out.vertices);

    retiraVertice ( n_vertices, g, &g_out );

    alocaVetorVertices ( n_vertices, &g_in );
    copiaVetorVertices ( n_vertices, g_out.vertices, g_in.vertices);

    retiraVizinhanca ( n_vertices, g, &g_out, &g_in );

    s_out = calculaConjuntoIndependenteMaximo ( n_vertices, &g_out );
    s_in = calculaConjuntoIndependenteMaximo ( n_vertices, &g_in );


    uniao ( n_vertices, g -> vertices, s_in );

    return ( maxsize ( n_vertices, s_in, s_out ) );
}

/* Idéia : Achar o conjunto independente máximo
 * Os vertices que não estão contidos nele serão a cobertura mínima */
void calculaCoberturaMinima ( grafo *g )
{
	iniciaVetorVertices ( g->nVertices, g );
	unsigned short *mis = calculaConjuntoIndependenteMaximo ( g -> nVertices, g );
	imprimeCoberturaMinima ( g-> nVertices, mis );
}

int main ( int argc, char **argv )
{
    grafo g;
    FILE *arq = stdin;
    
    if ( argc > 1 )
        if ( !( arq = fopen ( argv[1], "r" ) ) )
        {
            fprintf ( stderr,"Erro na abertura do arquivo" );
            exit (1);
        }
        
    leituraCobertura ( arq, &g );
    
    calculaCoberturaMinima ( &g );
    
    fclose (arq);
    return 0;
}
