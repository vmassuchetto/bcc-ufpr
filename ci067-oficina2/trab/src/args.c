/**
 * @file args.c
 * @brief Leitura de argumentos da linha de comando.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <libgen.h>
#include <err.h>
#include "args.h"
#include "err.h"

/**
 * @brief Imprime ajuda e interrompe execução do programa.
 * @param exit_code Código de saída do programa.
 */
static void
usage_help (int exit_code)
{
    fprintf (stderr,
"Uso: %s [OPÇÕES] AÇÃO [PARÂMETROS]\n"
"\n"
"Mosaico de imagens\n"
"\n"
" Ações:\n"
"  -a ID ARQUIVO COLUNA LINHA   Adiciona uma imagem\n"
"  -m ID COLUNA LINHA           Move uma imagem\n"
"  -r ID                        Remove uma imagem\n"
"  -R                           Remove todas imagens\n"
"  -p ARQUIVO                   Cria o mosaico em arquivo\n"
"  -l                           Lista imagens\n"
"\n"
" Opções:\n"
"  -b FUNDO                     Cor de fundo do mosaico\n"
"  -f ARQUIVO                   Arquivo de banco de dados\n"
"  -i                           Confirma antes de remover\n"
#ifndef NDEBUG
"  -x                           Ativa modo de debug para desenvolvimento\n"
#endif
"  -h                           Imprime esta ajuda\n"
"\n"
"ID: Identificação numérica da imagem\n"
"COLUNA, LINHA: Posição da imagem no mosaico\n"
"\n"
"As ações são mutuamente exclusivas.\n"
"\n" "Informe bugs para <kletoz@gmail.com>.\n", program_name);

    exit (exit_code);
}

/**
 * @brief Imprime dica de ajuda e interrompe execução do programa.
 */
static void
usage_try (void)
{
    fprintf (stderr, "Tente `%s -h' para mais informações.\n",
             program_name);
    exit (EXIT_FAILURE);
}

/**
 * @brief Imprime erro sobre opções mutuamente exclusivas.
 */
static void
usage_exclusive_actions (void)
{
    fprintf (stderr,
             "%s: -a, -m, -r, -R, -p: Opções mutuamente exclusivas\n",
             program_name);
    usage_try ();
}

/**
 * @brief Define variável global com o nome do programa, @c program_name, e o
 *        nome pelo qual o programa foi executado, @c program_path. Define
 *        valores padrão para os argumentos.
 * @param argc Número de argumentos de linha de comando.
 * @param argv Argumentos de linha de comando.
 * @param args Estrutura de argumentos.
 */
void
args_init (int argc, char **argv, struct args *args)
{
    program_path = err_strdup (argv[0]);
    program_name = basename (argv[0]);
    argv[0] = program_name;

    /* Valor padrão para algumas opções. */
    args->action = ACTION_NOTHING;
    args->database = NULL;
    args->bg = -1;
    args->list = 0;
    args->force = 1;
    args->debug = 0;
}

/**
 * @brief Finaliza estrutura de argumento e libera espaço alocado para nome do
 *        programa.
 * @param args Estrutura de argumentos.
 */
void
args_finish (struct args *args)
{
    free (program_path);
}

/**
 * @brief Processa argumentos da linha de comando.
 * @param argc Número de argumentos da linha de comando.
 * @param argv Vetor de argumentos da linha de comando.
 * @param args Estrutura com as opções passadas por linha de comando.
 */
void
args_parse (int argc, char **argv, struct args *args)
{
    int c;

    /* Fazer um pré-parser para ativar o modo debug. Dessa forma, se no parser
     * principal (que vem logo em seguida) houver algum erro, as mensagens de
     * debug são impressas.  Isso implica em reiniciar o índice OPTIND. A opção
     * de debug `-x' precisa ser incluída no parser principal para evitar
     * mensagens de erro. 
     *
     * O uso de `0123456789' em OPTSTRING, terceiro argumento de getopt(), é
     * para permitir que sejam passados números negativos para o idenficador da
     * imagem ID e a posição (COL e ROW). Um uso um pouco incomum, senão errado,
     * de getopt(). */

#ifndef NDEBUG
    opterr = 1;

    while ((c = getopt (argc, argv, "0123456789amrpb:fRlihx")) != -1)
    {
        if (c == 'x')
        {
            args->debug = 1;
            break;
        }
        else if (c == '?')
        {
            usage_try ();
            break;
        }
    }

    debug_init (args->debug, "stderr");
#endif

    opterr = 1;
    optind = 0;

#ifndef NDEBUG
    while ((c = getopt (argc, argv, "0123456789amrpb:fRlihx")) != -1)
#else
    while ((c = getopt (argc, argv, "0123456789amrpb:fRlih")) != -1)
#endif
    {
        if (optind > argc)
            break;

        switch (c)
        {
        case 'a':
            if (args->action != ACTION_NOTHING)
                usage_exclusive_actions ();

            if (argc - optind < 4)
            {
                fprintf (stderr, "Uso: %s -a ID ARQUIVO COLUNA LINHA\n",
                         program_name);
                usage_try ();
            }

            args->action = ACTION_ADD;
            args->id = err_atoi (argv[optind++]);
            args->filename = argv[optind++];
            args->col = err_atoi (argv[optind++]);
            args->row = err_atoi (argv[optind++]);
            break;
        case 'm':
            if (args->action != ACTION_NOTHING)
                usage_exclusive_actions ();

            if (argc - optind < 3)
            {
                fprintf (stderr, "Uso: %s -m ID COLUNA LINHA\n", program_name);
                usage_try ();
            }

            args->action = ACTION_MOVE;
            args->id = err_atoi (argv[optind++]);
            args->col = err_atoi (argv[optind++]);
            args->row = err_atoi (argv[optind++]);
            break;
        case 'r':
            if (args->action != ACTION_NOTHING)
                usage_exclusive_actions ();

            if (argc - optind < 1)
            {
                fprintf (stderr, "Uso: %s -r ID\n", program_name);
                usage_try ();
            }

            args->action = ACTION_REMOVE;
            args->id = err_atoi (argv[optind++]);
            break;
        case 'R':
            if (args->action != ACTION_NOTHING)
                usage_exclusive_actions ();

            args->action = ACTION_CLEAN;
            break;
        case 'p':
            if (args->action != ACTION_NOTHING)
                usage_exclusive_actions ();

            if (argc - optind < 1)
            {
                fprintf (stderr, "Uso: %s -p ARQUIVO\n", program_name);
                usage_try ();
            }

            args->action = ACTION_PRINT;
            args->filename = argv[optind++];
            break;
        case 'b':
            args->bg = err_atoi (optarg);
            break;
        case 'f':
            if (argc - optind < 1)
            {
                fprintf (stderr, "Uso: %s -f ARQUIVO\n", program_name);
                usage_try ();
            }

            args->database = argv[optind++];
            break;
        case 'l':
            args->list = 1;
            break;
        case 'i':
            args->force = 0;
            break;
        case 'h':
            usage_help (EXIT_SUCCESS);
            break;
        case '?':
            usage_try ();
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if (args->action == ACTION_NOTHING)
                usage_help (EXIT_FAILURE);
            break;
#ifndef NDEBUG
        case 'x':
            break;
#endif
        default:
            errx (EXIT_FAILURE, "ERRO DE PROGRAMA: opção `%c' não tratada.", c);
        break;
            break;
        }
    }

    if (argc == 1 || optind != argc)
        usage_help (EXIT_FAILURE);

    if (args->action == ACTION_NOTHING && !args->list)
        usage_help (EXIT_FAILURE);
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
