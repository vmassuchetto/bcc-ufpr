/**
 * @file mos.h
 * @brief Manipulação do mosaico.
 */

#ifndef __MOS_H__
#define __MOS_H__

#ifndef __IMG_H__
# include "img.h"
#endif

#ifndef __DB_H__
# include "db.h"
#endif

/** @brief Posição no arquivo de saída do mosaico. */
#define MOS_POS(row,col,mos) \
    (((row) * (mos)->img.header.cols + (col)) * (mos)->img.bytes_per_pixel + (mos)->shift)

/**
 * @brief Nó da lista de buracos.
 */
struct bgholes
{
    /** @brief Início e fim do buraco. */
    long inf, sup;
    /** @brief Ponteiro para o próximo nó. */
    struct bgholes *next;
    struct bgholes *prev;
};

/**
 * @brief Estrutura do mosaico.
 */
struct mos
{
    /** @brief Cor do fundo. */
    unsigned short bg;
    /** @brief Tamanho do cabeçalho, em bytes. */
    long shift;
    /** @brief Estrutura de imagem do mosaico. */
    struct img img;
    /** @brief Vetor com as informações das imagens que fazer parte do mosaico. */
    struct img *imgs;
    /** @brief Ponteiro para banco de dados. */
    struct db *db;
    /** @brief Lista de buracos no fundo, ou, vendo de outra forma, lista de
     *         pixels onde há imagens. */
    struct bgholes *bgholes;
};

extern void mos_init (struct mos *, struct db *, const char *, unsigned short);
extern void mos_finish (struct mos *);
extern void mos_header_print (struct mos *);
extern void mos_imgs_print (struct mos *);
extern void mos_bg_print (struct mos *);

#endif /* __MOS_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
