/**
 * @file err.h
 * @brief Funções empacotadoras de erros.
 */

#ifndef __ERR_H__
#define __ERR_H__

#ifndef _STDIO_H
# include <stdio.h>
#endif

#ifndef __DEBUG_H__
# include "debug.h"
#endif

extern void *err_malloc (__FLF_PROTO__, size_t);
extern void *err_realloc (__FLF_PROTO__, void *, size_t);
extern char *err_strdup (__FLF_PROTO__, const char *);
extern FILE *err_fopen (__FLF_PROTO__, const char *, const char *);
extern long err_ftell (__FLF_PROTO__, FILE *);
extern void err_fseek (__FLF_PROTO__, FILE *, long, int);
extern size_t err_fwrite (__FLF_PROTO__, const void *, size_t, size_t,
                          FILE *);
extern size_t err_fread (__FLF_PROTO__, void *, size_t, size_t, FILE *);
extern int err_fileno (__FLF_PROTO__, FILE *);
extern void err_ftruncate (__FLF_PROTO__, int, off_t);
extern unsigned int err_atoui (__FLF_PROTO__, const char *);
extern int err_atoi (__FLF_PROTO__, const char *);

#define err_malloc(args...) err_malloc(__FLF__,args)
#define err_realloc(args...) err_realloc(__FLF__,args)
#define err_strdup(args...) err_strdup(__FLF__,args)
#define err_fopen(args...) err_fopen(__FLF__,args)
#define err_ftell(args...) err_ftell(__FLF__,args)
#define err_fseek(args...) err_fseek(__FLF__,args)
#define err_fwrite(args...) err_fwrite(__FLF__,args)
#define err_fread(args...) err_fread(__FLF__,args)
#define err_fileno(args...) err_fileno(__FLF__,args)
#define err_ftruncate(args...) err_ftruncate(__FLF__,args)
#define err_atoui(args...) err_atoui(__FLF__,args)
#define err_atoi(args...) err_atoi(__FLF__,args)

#endif /* __ERR_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
