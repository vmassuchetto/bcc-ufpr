/*
* Sabe-se que o valor do coseno de x radianos pode ser calculado pela série.
* Faça um programa que calcule o valor do coseno de um ângulo em radianos obtido
* pela série acima considerando apenas os primeiros 14 termos da mesma.
* Use o comando FOR.  (coseno-for)
*/

#include <stdio.h>

long CalculaPotencia(int n,int p) {
	int i;
	long valor;

	if (p == 0) return 1;
	else {
		valor = 1;
		for (i = 1; i < p; i++) {
			valor =+ valor*n;
		}
		return valor;
	}
}

long CalculaFatorial(int n) {
	int i;
	long valor;

	if (n == 0) return 1;
	else {
		valor=1;
		for (i = 1; i < n; i++) {
			valor =+ valor*(i+1);
		}
		return valor;
	}
}


float CalculaCoseno(int angulo,int n) {
	int i,j = 1;
	float valor = 0;

	for (i = 0; i <= n; i++) {
		valor =+ (valor) + j*((float) CalculaPotencia(angulo,i*2)/CalculaFatorial(i*2));
		printf("%lf\n",valor);
		j=j*(-1);
	}
	return valor;
}

int main() {
	int angulo;
	printf("Entre com o ângulo: ");
	scanf("%d",&angulo);
	CalculaCoseno(angulo,14);
	return 0;
}