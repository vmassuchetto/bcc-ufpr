7 Espaços cognitivos no uso de sistemas
computacionais
Implicações da Abordagem Cognitiva no Projeto de Interfaces Gráficas
Características das interfaces por Manipulação Direta – MD (Schneiderman):
• Representação contínua do objeto de interesse;
• Ações físicas ou pressão de botões ao invés de uso de sintaxe complexa;
• Operações incrementais e reversíveis com impacto imediatamente visível.
A promessa da MD está em que a programação é feita graficamente, de uma forma que
vai ao encontro do jeito como o homem pensa no problema.
Nestas interfaces, erros de sintaxe são eliminados, pois não se pode fazer referência a
objetos inexistentes.
Vantagens das interfaces por MD (Schneiderman):
• Usuários novatos conseguem aprender a funcionalidade da interface
rapidamente, através de uma simples demonstração feita por um usuário
experiente;
• Usuários experientes podem executar uma grande faixa de tarefas
rapidamente;
• Usuários intermitentes conseguem manter os conceitos das operações;
• Mensagens de erro são quase desnecessárias;
• Usuários percebem facilmente se as ações estão atingindo (ou não) seus
objetivos;
• Usuários ficam menos ansiosos pois o sistema é compreensível e as ações
reversíveis.
O que dá a sensação de “directness” nas interfaces por manipulação direta?
A necessidade do uso de recursos cognitivos na utilização da interface indica o contrário:
“indirectness”.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Conceitos associados à “directness”
Distância entre o pensamento e a capacidade física do sistema;
Quanto menor for esta distância, mais direta será a tradução entre intenção e solicitação,
mais fácil será a interpretação e maior será a sensação de “directness”.
Envolvimento (sensação) do usuário na tarefa.
Metáforas
Metáfora da Conversação:
Interface é o meio lingüístico onde o intercâmbio ocorre.
Exemplos: interfaces “conversacionais” (de comandos, em linguagem
natural)
Metáfora do Mundo Modelo:
Interface é, em si mesma, o mundo sobre o qual o usuário atua, e cujo
estado muda em resposta às ações;
O usuário tem a sensação de estar agindo sobre o próprio domínio
(realidade).
Exemplos: interface do Mac®, parte da interface do WINDOWS®
Engenharia Cognitiva (Norman 86)

Engenharia

Psicologia

de Sistemas

Cognitiva

Figura 7.1: A Engenharia Cognitiva como interseção da Psicologia Cognitiva e da
Engenharia de Sistemas
A Engenharia Cognitiva é a aplicação de resultados de Ciências Cognitivas ao projeto e à
construção de máquinas.
Objetivos:
1. Compreensão dos princípios subjacentes à ação humana relevante no processo
de design;
2. Construção de interfaces fáceis de serem usadas.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Análise de complexidade de tarefas
Dado um problema, a relação entre o número de variáveis do mesmo e o número de
controles necessários não é direta.
Variáveis psicológicas são as variáveis relacionadas com a meta e as intenções do
usuário.
Variáveis físicas são os controles associados à resolução do problema na máquina.
Estágios envolvidos na interação usuário-sistema para a execução de uma tarefa
simples
1.

Estabelecimento da meta;

2. Determinação da intenção;
3. Especificação da ação;
4. Execução da ação;
5. Percepção do estado do sistema;
6. Interpretação do estado do sistema;
7. Avaliação da resposta em relação à meta.
Estes estágios se propõem a descrever a distância entre o modelo do usuário e a imagem
do sistema, na ida (execução) e na volta (avaliação).
Os “golfos de execução” e “de avaliação” são a representação das discrepâncias entre
as variáveis psicológicas e as variáveis físicas.
Eles constituem o espaço principal a ser endereçado no projeto, na análise e no uso dos
sistemas de interface homem-máquina.
Golfo de execução
intenção
texto sem um
certo
parágrafo

+

especific.
da tarefa
apagar parágrafo

+

execução
marcar trecho e
selecionar
operação
apagar

+

entrada
física
realizar isso +
OK (ou ENTER)

Figura 7.2: Diferentes níveis de ação envolvidos na execução de uma tarefa
Esta taxonomia não é fixa. Seus separadores são função da granularidade da tarefa e dos
dispositivos de entrada e saída.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Golfo de avaliação
No golfo de avaliação o problema inicial consiste na determinação do estado do sistema.
Uma saída (output) apropriada pode auxiliar a esta tarefa.
Um problema comum consiste nos vários níveis de saída, que devem ser mapeados
para os diferentes níveis de intenções.
Outro problema comum consiste em a mudança no estado do sistema ocorrer bem
depois da execução da tarefa. Isto dificulta a avaliação.
A maior responsabilidade do projetista está em auxiliar ao usuário na compreensão do
sistema.
Isto leva à necessidade de proporcionar um modelo de projeto bom, e uma imagem do
sistema consistente.

golfo de
execução
Metas do usuário

Sistema físico

K


golfo de
avaliação

Figura 7.3: os Golfos de Execução e Avaliação (Norman 1986)

Assim, existem dois espaços distintos:
Homem:
metas, intenções
Máquina:
meio físico, mecanismos que viabilizam a solicitação / resposta da tarefa

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Entre esses dois espaços existem dois golfos a serem atravessados:
Golfo de execução:
É cruzado quando os comandos e demais mecanismos do sistema casam
com metas e intenções do usuário, ou seja, quando a solicitação da tarefa
é facilitada.
Golfo de avaliação:
É cruzado quando a saída do sistema apresenta um bom modelo
conceitual que é percebido, interpretado e avaliado, ou seja, quando a
avaliação do resultado é facilitada.
Os golfos de execução e avaliação estão diretamente relacionados com o esforço
cognitivo necessário à utilização da interface.
Quanto mais fáceis forem de se transpor os golfos, menor o esforço cognitivo necessário
e maior a sensação de “directness”.
Uma boa interface por MD dá a sensação de envolvimento direto no controle da
tarefa.
Tradicionalmente e em interfaces conversacionais, o usuário está em contato direto com
estruturas lingüísticas que podem ser interpretadas como referências aos objetos, mas
não são os próprios objetos (nem mesmo uma representação próxima dos mesmos).
Nas interfaces por MD ao invés de descrever as ações, o usuário as “executa”.

intenção

especificação da tarefa

execução

:

K
avaliação

interpretação

percepção

Figura 7.4: Ciclo cognitivo na interação usuário-sistema (Norman 1986)
Exemplo: edição de texto
intenção: texto sem certo parágrafo;
especificação: apagar parágrafo;
execução: marcar, selecionar cortar, disparar.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Distâncias semântica e articulatória

d. semântica
d. articulatória
metas
significado da expressão
forma da expressão
(nível da
intenção)

(informação e possibilidades
presentes na interface)

(adequação da
expressão da
informação e das
possibilidades)

Figura 7.5: Distâncias possíveis entre o modelo mental e a interface (Norman 1986)

Distância semântica
É possível expressar o que se quer na linguagem de interface?
A linguagem expressa de forma exata conceitos e distinções no domínio em
questão?
Os objetos do domínio podem ser expressos de forma concisa?
No golfo de execução, a distância semântica reflete quanto da estrutura requerida é
expressa pelo sistema e quanto pelo usuário. Quanto mais o usuário precisar realizar
esforço cognitivo na elaboração da solicitação da tarefa, maior a distância semântica.
No golfo de avaliação, a distância semântica se refere à quantidade de processamento
que o usuário precisa realizar para determinar se a meta foi atingida. Se o usuário
precisar traduzir a saída em expressões compatíveis com a meta, a distância será grande.
A distância semântica tem relação com:
quantidade (completude),
relevância,
natureza e
granularidade (níveis de estruturação)
da informação que pode ser extraída da representação (linguagem de interface).
O projeto de interfaces envolve um compromisso generalidade X distância semântica
nula.
Linguagens de alto nível, que expressam solicitações na linguagem de decomposição do
problema têm o custo de serem muito específicas;
Linguagens de propósito geral, pela sua caraterística intrínseca, não são próximas da
linguagem do usuário;
Na exibição da saída de processos proporcionar todas as informações que completam o
contexto auxilia à meta de distância nula (ex.: noção de movimento e valor corrente).
O comportamento automatizado não é sinônimo de distância semântica nula!
Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Quando o projetista não fez um bom trabalho, o usuário pode modificar a sua visão do
problema, o seu modelo conceitual, aprender a linguagem do sistema, se adaptar à
representação.
Exemplo: editor VI do UNIX®
Seus comandos parecem diretos para usuários experientes, que compensam as
deficiências do sistema por meio de prática e automatização de comportamento.

Distância Articulatória
A distância articulatória se refere à distância entre significado e forma.
Tanto no golfo de execução quanto no de avaliação, a distância articulatória pode ser
minimizada através de qualquer relação não-arbitrária entre a semântica e a forma.
Técnicas:
ícones de resemblance;
mímicas da ação (através, por ex., de mouse e cursor);
acompanhamento de ações no mundo real modelado (ex.: para exibir mudança
de velocidade, mostrar a curva em movimento).
comandos e rótulos de menus mnemônicos (dependente língua!);
onomatopéia (a imitação do som facilita a interpretação).
Editores do tipo “WYSWYG” (What you see is what you get) expressam conceitos
semânticos de forma direta e explícita.
As interfaces devem explorar semelhanças articulatórias por meio de diferentes
tecnologias.
“Directness” articulatória é fortemente dependente de tecnologias de entrada e saída.
Linguagens de interface icônicas (MD), em geral, têm uma forma de expressão mais
próxima do significado (ou seja, têm distância articulatória próxima de 0).
Observação: Estes conceitos estão intimamente relacionados entre si e qualquer um dos
dois pode comprometer o outro!

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Exemplos
No golfo de execução:
Distância semântica
A distância semântica é pequena quando a solicitação de tarefas casa com a
expectativa do usuário em relação ao que ele espera que o sistema disponibilize como
possibilidades.
Exemplo 1
Do ponto de vista do usuário, as tarefas de edição que ele espera encontrar são:
Eliminação de um trecho de texto
Movimentação de um trecho de texto
Cópia de um trecho de texto
Estas tarefas não estão disponíveis no menu de edição, que exibe, diretamente, as ações
do sistema que efetivam as tarefas em questão:
Corte
Cópia
Cola
ou
Portanto, a distância semântica na solicitação da tarefa para um usuário não familiarizado
com o ambiente é significativa.
Neste caso, isto ocorre pela discrepância no nível de tarefas disponibilizado ao usuário.
Exemplo 2
A solicitação de destaque de um trecho de texto no Wordstar é enorme, pois a digitação
da seqüência:
CTRL + P + B + trecho de texto + CTRL + P + B
Não tem rigorosamente nenhuma relação de significado com a tarefa de destacar texto.
Neste caso, a distância semântica existe pela discrepância entre o processo utilizado
para a solicitação da tarefa e a concepção que o usuário tem do destaque de texto.
Exemplo 3
Outra possibilidade de distância semântica significativa é a de omissão de parte da
informação ou tipo (natureza) de informação necessária à tomada de decisões para a
solicitação de tarefas.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Isto ocorre, por exemplo, quando um rótulo de comandos não deixa claros todos os eixos
semânticos relativos à tarefa à que ele se refere, ou, então, quando ele é ambíguo em
relação ao nível de atuação (mais genérico ou mais específico).
Exemplo 4
Uma escolha inadequada do rótulo de uma ação determina uma distância semântica
significativa.
No exemplo abaixo, o rótulo “salvar” deixa o usuário desconcertado.

Este tipo de problema ocorre, também, na escolha de rótulos ambíguos (como
“Endereço”) nos campos de formulários.
Problema semelhante de dificuldade de interpretação (semântica) ocorre quando rótulos
parecidos são oferecidos simultaneamente, causando confusão na sua interpretação.
Nos teclados dos caixas automáticos e nos sistemas de pagamento online são oferecidas,
em geral, as opções Cancelar e Anular, confundindo o usuário que não tem consciência
de que a diferença reside no escopo da ação a ser interrompida.

Distância articulatória
No golfo de execução a distância articulatória é pequena quando a forma como as tarefas
são representadas é próxima ou natural à forma como o usuário as conhece.
Exemplo 5
O comando delete tem distância articulatória nula para um falante da língua inglesa e
enorme (comprometendo a semântica!) para um falante da Língua Portuguesa que
desconhece a palavra.
Exemplo 6
A unidade adequada para uma cultura (por exemplo, quilômetros) expressa por uma
abreviação arbitrária
“kilomets.” Como rótulo de um campo de formulário
determina uma distância articulatória grande.

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

Exemplo 7
Qualquer ícone de tipo arbitrário usado como botão de função tem, por definição,
distância articulatória grande.
Exemplo 8
Mensagens de orientação para a solicitação de uma tarefa numa língua diferente da
língua mãe do usuário potencial determinam distância articulatória grande (tão
significativa quanto diferentes forem as línguas entre si).
Exemplo 9
Qualquer orientação que se refira a botões ou rótulos de forma diferente a como eles
aparecem de fato na interface (por meio de formas verbais diferentes ou utilizando
indistintamente Maiúsculas e minúsculas, entre outras possibilidades) incorre numa
distância articulatória relevante.
A orientação de ajuda “Pressione o botão de cancelamento...” seria melhor resolvida, do
ponto de vista articulatório, por “Pressione o botão
Cancelar

No golfo de avaliação:
Distância Semântica
No golfo de avaliação, a distância semântica é pequena quando a avaliação do
resultado da solicitação da tarefa é facilitado.
Exemplo 10
Uma resposta do sistema em uma unidade de medida diferente daquela da cultura do
usuário potencial (por exemplo, milhas para um brasileiro) exige um esforço cognitivo
importante (de cálculo a partir de fórmulas de equivalência) na sua interpretação,
determinando uma distância semântica decorrente.
O problema, neste caso, se refere ao processo necessário do usuário na interpretação
da resposta.
Exemplo 11
A apresentação dos objetos de um sistema de arquivos de forma desestruturada, isto é,
indicando apenas o nome e a extensão determina uma distância semântica significativa,
pois omite a informação relativa à organização dos mesmos.
Exemplo 12
Uma relação não-estruturada de links como resultado a uma busca de informação
configura a mesma situação, pois omite informação relevante para o processo de decisão
posterior.
Estes dois exemplos mostram distância semântica decorrente da omissão de parte da
informação esperada pelo usuário para a avaliação da resposta.
Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

De maneira geral, a apresentação linear ou sequencial de qualquer informação que
guarde um estrutura incorre neste tipo de erro, pois omite parte da resposta (a forma
como a informação está organizada), levando a um processo desnecessário de
reestruturação (se e quando isso for possível).
Exemplo 13
No sistema de controle da pressão arterial, a omissão do valor corrente, por exemplo, ou
do histórico que permite a interpretação da tendência (omissão de algum tipo ou
natureza de informação) determinaria uma distância semântica significativa.
Distância Articulatória
No golfo de avaliação, a distância articulatória é nula quando a resposta tem
representação (forma, expressão) natural para o usuário.
Exemplo 14
No sistema de controle da pressão arterial, a escolha do recurso de um gráfico para
exibir a tendência minimiza a distância articulatória em relação, por exemplo, a uma
tabela de histórico de valores, que apresenta toda a informação mas de maneira indireta,
exigindo inferência. (Neste caso, o esforço envolve também aspectos semânticos).
O uso da palavra “gráfico” ao invés da inclusão da própria representação visual determina
uma distância articulatória significativa.
Em termos gerais, qualquer descrição textual (e/ou numérica) de aspectos visuais ou
de processos dinâmicos determina uma distância articulatória significativa.
Objetos e/ou processos de natureza visual devem ser assim exibidos. Processos
dinâmicos devem ser apresentados por meio de recursos de animação, entre outros.
Exemplo 15
Mensagens de erro numa língua diferente da língua mãe do usuário determinam uma
distância articulatória significativa (tão relevante quanto for a diferença entre as línguas).
Exemplo 16
O feedback do Wordstar para o itálico:
^ p i trecho de texto ^ p i
tem uma distância articulatória enorme em relação ao que o usuário pretendia com a
solicitação:
trecho de texto

Interação Humano-Computador – Profa. Laura Sánchez García – UFPR – 2011

