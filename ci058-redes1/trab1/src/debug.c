#include "main.h"

void debug_raw (unsigned char* r) {
	int i = 0;
	printf ("r[0]\tr[1]\tr[2]\tr[3]\n");
	printf ("%x\t%x\t%x\t%c\n", r[0], r[1], r[2], r[3]);

	printf ("r[4]: ");
	for (i = 0; i <= r[1] - 3; i++);
		printf ("%c", r[i + 4]);
	printf ("\n");

	printf ("r[%d]: %x\n", i + 4, r[i + 4]);
}

void debug_packet (packet p) {

	int i;
	printf ("%2x | %3d | %d | %d | %c",
        p->init, p->size, p->seq, p->par, p->type);

	if (DEBUG > 1) {
		printf ("\n--------------\n");
		for (i = 0; i < p->size - 2; i++)
			printf ("%x", p->data[i]);
		printf ("\n--------------\n");
	}
}
