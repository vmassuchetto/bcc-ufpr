#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include "aux.h"
#include "fila.h"

int main(int argc, char **argv) {
	int sock, n;
	struct sockaddr_in server, from;
	char *msgin, *msgout;
	struct fila *f;

	checksyntax_client(argc, argv);

	if ( !(f = cria_fila()) ) return -1;
	sock = cria_socket();
	servidor_remoto(&server, argv[1], argv[2]);

	do {
		if ( !enfila(f, msgout = proxima_mensagem()) )
			printf("Fila cheia! Aguarde liberacao da fila.");
		else printf("Enfilado: %s\n", msgout);
	} while ( strcmp(msgout, "mexit") != 0 );

	while ( !vazia_fila(f) ) {
		msgout = desenfila(f);
		printf("Desenfilado: %s\n", msgout);
		n = envia(sock, msgout, &server);
		msgin = recebe(sock, &from);
	}

	destroi_fila(f);
	return 0;
}
