struct fila {
	int tamanho;
	struct no *primeiro;
};

struct no {
	char *msg;
	struct no *prox;
};

struct fila *cria_fila (void);
int enfila (struct fila *f, char *msg);
char *desenfila (struct fila *f);
void destroi_fila (struct fila *f);
int vazia_fila (struct fila *f);
void imprime_fila (struct fila *f);
