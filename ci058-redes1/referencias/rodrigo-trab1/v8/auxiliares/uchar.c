# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM_BUFFER 256

typedef unsigned char uchar;

int main ()
{
	int tamanho, tipo, paridade, i;
	char buffer[TAM_BUFFER], *dados;
	uchar *mensagem;

	// Le dados de entrada
	printf ("Digite o tamanho: ");
	scanf ("%d", &tamanho);
	printf ("Digite o tipo: ");
	scanf ("%d", &tipo);
	printf ("Digite a paridade: ");
	scanf ("%d", &paridade);
	printf ("Digite os dados: ");
	__fpurge (stdin);												// Limpa a quebra de linha do buffer de leitura
	fgets (buffer, TAM_BUFFER, stdin);
	dados = strdup (buffer);
	dados[strlen (dados) - 1] = '\0';									// Elimina a quebra de linha dos dados lidos
	// Monta a mensagem
	mensagem = (uchar*) calloc (3 + strlen (dados), sizeof (uchar));			// Aloca espaco para a mensagem
	mensagem[0] = tamanho;
	mensagem[1] = tipo;
	mensagem[2] = paridade;
	printf ("Tamanho: %d, tipo: %d, paridade: %d, dados: ", mensagem[0], mensagem[1], mensagem[2]);
	for (i = 0; i < strlen (dados); i++)								// Coloca os dados na mensagem
	{
		mensagem[i + 3] = dados[i];
		printf ("%c", mensagem[i + 3]);
	}
	puts (".");
}
