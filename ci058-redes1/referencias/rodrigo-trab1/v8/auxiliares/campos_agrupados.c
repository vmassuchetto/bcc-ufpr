/*
Campo_Duplo = Tamanho + Sequencia
Tamanho = Tipo + Dados + Paridade
Paridade = Campo_Duplo + Tipo + Dados
*/
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM_CONVERSAO 10
# define TAM_BUFFER 256

int main ()
{
	int campo_duplo = 192, tipo = 4, i, paridade = 0;
	char s_campo_duplo[TAM_CONVERSAO], s_tipo[TAM_CONVERSAO], *dados = strdup ("BDFH."), buffer[TAM_BUFFER], *campos_agrupados;

	snprintf (s_campo_duplo, TAM_CONVERSAO, "%d", campo_duplo);			// Converte o inteiro campo_duplo para a string s_campo_duplo
	snprintf (s_tipo, TAM_CONVERSAO, "%d", tipo);					// Converte o tipo para a string s_tipo

	// Agrupa os campos
	strcpy (buffer, s_campo_duplo);
	strcat (buffer, s_tipo);
	strcat (buffer, dados);
	campos_agrupados = strdup (buffer);

	for (i = 0; i < strlen (campos_agrupados); i++)					// Calcula a paridade dos campos agrupados
		paridade = paridade ^ campos_agrupados[i];

	printf ("Paridade dos campos agrupados, que formam a string abaixo: %d.\n\t%s\n", paridade, campos_agrupados);
}
