/*
Erro 4. Diretorio inexistente
*/
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM 256

int main (int argc, char **argv)
{
	char *diretorio = strdup (argv[1]);

	// Verifica erros do comando cd, usando funcao chdir
	printf ("> Executando comando cd %s\n", diretorio);
	if (chdir (diretorio) != 0)									// Muda de diretorio
		fprintf (stderr, "> Erro no comando cd. Diretorio %s/ nao existe\n", diretorio);
	else
		printf ("> Diretorio %s/ acessado com sucesso\n", diretorio);
}
