# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM_BUFFER 2048

int main (int argc, char** argv)
{
	FILE *fp_entrada, *fp_inicio, *fp_fim, *fp_saida;
	int i = 1, cont = 0, erro = 0, linha = atoi (argv[1]) - 1, linhas_saida;
	char *entrada, *inicio, *fim, *saida, **matriz_inicio, **matriz_fim, **matriz_saida, buffer[TAM_BUFFER], *comando = strdup ("mv saida.txt ");

	// Inicializa nome dos arquivos auxiliares, e matrizes que gerarao estes arquivos
	entrada = strdup ("entrada.txt");
	inicio = strdup ("inicio.txt");
	fim = strdup ("fim.txt");
	saida = strdup ("saida.txt");
	matriz_inicio = (char**) calloc (1, sizeof (char*));					// Aloca 1 linha para matriz_inicio
	matriz_fim = (char**) calloc (1, sizeof (char*));						// Aloca 1 linha para matriz_fim
	matriz_saida = (char**) calloc (1, sizeof (char*));					// Aloca 1 linha para matriz_saida

	// Faz o que o programa se propoe
	if (fp_entrada = fopen (entrada, "a+b"))							// Acrescenta dados ou cria o arquivo para leitura e escrita
	{
		// Cria matriz que gerara o arquivo inicio.txt
		while (cont != linha && !erro)								// Enquanto nao cheguei a linha especificada, e nao ocorreu erro
		{
			if (fgets (buffer, TAM_BUFFER, fp_entrada) != NULL)
			{	
				matriz_inicio[cont] = strdup (buffer);										// Aloca espaco para a linha atual de matriz_inicio
				matriz_inicio = (char**) realloc (matriz_inicio, (i++ + 1) * sizeof (char*));		// Realoca mais 1 linha para matriz_inicio
				cont++;																// Incrementa o contador da linha
			}
			else
			{
				printf ("\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", entrada);
				erro = 1;
			}
		}

		// Cria arquivo inicio.txt
		if (!erro)
		{
			if (fp_inicio = fopen (inicio, "a+b"))						// Acrescenta dados ou cria o arquivo para leitura e escrita
			{
				for (i = 0; i < cont; i++)							// Copia o conteudo de entrada.txt, ate a linha especificada, para inicio.txt
					fprintf (fp_inicio, "%s", matriz_inicio[i]);
				printf ("\tArquivo %s criado com sucesso.\n", inicio);
			}
			else
				printf ("\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", inicio);
		}

		// Cria matriz que gerara o arquivo fim.txt
		i = 1;
		cont = 0;
		fgets (buffer, TAM_BUFFER, fp_entrada);							// Le a linha especificada, e nao faz nada com ela, pois ela sera editada
		while (!(feof (fp_entrada)))									// Enquanto nao cheguei ao fim do arquivo
		{
			if (fgets (buffer, TAM_BUFFER, fp_entrada) != NULL)
			{	
				matriz_fim[cont] = strdup (buffer);										// Aloca espaco para a linha atual de matriz_fim
				matriz_fim = (char**) realloc (matriz_fim, (i++ + 1) * sizeof (char*));			// Realoca mais 1 linha para matriz_fim
				cont++;																// Incrementa o contador da linha
			}
		}

		// Cria arquivo fim.txt
		if (fp_fim = fopen (fim, "a+b"))								// Acrescenta dados ou cria o arquivo para leitura e escrita
		{
			for (i = 0; i < cont; i++)								// Copia o conteudo de entrada.txt, apos a linha especificada, para fim.txt
				fprintf (fp_fim, "%s", matriz_fim[i]);
			printf ("\tArquivo %s criado com sucesso.\n", fim);
		}
		else
			printf ("\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", fim);

		// Cria matriz que gerara o arquivo saida.txt
		for (i = 0; i < linha; i++)
		{	
			matriz_saida[i] = strdup (matriz_inicio[i]);
			matriz_saida = (char**) realloc (matriz_saida, (i + 2) * sizeof (char*));				// Realoca mais 1 linha para matriz_saida
		}
		matriz_saida[i] = strdup ("\tEDITANDO A LINHA! EDITANDO A LINHA! EDITANDO A LINHA! EDITANDO A LINHA! EDITANDO A LINHA!\n");
		matriz_saida = (char**) realloc (matriz_saida, (i + 2) * sizeof (char*));
		linhas_saida = i + 1;
		for (i = 0; i < cont; i++)
		{
			matriz_saida[linhas_saida] = strdup (matriz_fim[i]);
			matriz_saida = (char**) realloc (matriz_saida, (linhas_saida++ + (i + 2)) * sizeof (char*));
		}

		// Cria arquivo saida.txt
		if (fp_saida = fopen (saida, "a+b"))							// Acrescenta dados ou cria o arquivo para leitura e escrita
		{
			for (i = 0; i < linhas_saida; i++)							// Gera o arquivo saida.txt
				fprintf (fp_saida, "%s", matriz_saida[i]);
			printf ("\tArquivo %s criado com sucesso.\n", saida);
		}
		else
			printf ("\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", saida);
	}
	else
		printf ("\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", entrada);

	// Remove os arquivos temporarios, e renomeia o arquivo de entrada
	printf ("\tRenomeando arquivo %s para %s.\n", saida, entrada);
	strcat (comando, entrada);
	system (comando);
	printf ("\tRemovendo arquivo %s.\n", inicio);
	remove (inicio);
	printf ("\tRemovendo arquivo %s.\n", fim);
	remove (fim);
}
