/*
SEEK_SET -> Move a partir do inicio do arquivo
SEEK_CUR -> Move a partir da posicao atual
SEEK_END -> Move a partir do final do arquivo
*/
# include <stdio.h>
# include <stdlib.h>

int main (int argc, char **argv)
{
	FILE *fp_in, *fp_out;
	long tam;	
	char *dado;

	if (argc == 3)
	{
		fp_in = fopen (argv[1], "rb");
		fp_out = fopen (argv[2], "wb");

		fseek (fp_in, 0, SEEK_CUR);									// Movimenta a posicao corrente de leitura do arquivo para seu fim
		tam = ftell (fp_in);										// Pega a posicao corrente de leitura no arquivo
		dado = (char*) calloc (tam, sizeof (char));
		rewind (fp_in);											// Movimenta a posicao corrente de leitura do arquivo para seu inicio

		fread (dado, sizeof (char), 1, fp_in);							// Efetua a copia do arquivo original para o arquivo de saida
		while (!(feof (fp_in)))
		{
			fwrite (dado, sizeof (char), 1, fp_out);
			fread (dado, sizeof (char), 1, fp_in);
		}
		fclose (fp_in);
		fclose (fp_out);
	}
	else
		printf ("Uso: %s <arquivo_original> <arquivo_copiado>\n", argv[0]);
}
