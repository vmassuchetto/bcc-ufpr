# include <stdio.h>

# define TAM 24
# define SEQ 6

int main ()
{
	unsigned char byte, aux, tamanho = TAM, sequencia = SEQ;

	aux = tamanho << 3;												// aux é uma variável auxiliar, correspondente ao passo 1. do arquivo shift.pdf
	byte = aux | sequencia;											// byte corresponde ao passo 2. do arquivo shift.pdf
	printf ("Aux: %d, Byte gerado: %d\n", aux, byte);

	return 0;
}
