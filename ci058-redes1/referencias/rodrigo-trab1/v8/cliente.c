# include "main.h"

/* Executa as acoes do cliente. Retorna 1 em caso de sucesso, e 0 caso contrario. */
int Modo_Cliente (int c, int modo, int sequencia, char *entrada, int linha, char *dados)
{
	printf ("\t\t\t>>>>> Modo_Cliente\n");
	switch (modo)
	{
		case 1:												// Mostra na tela conteudo de um arquivo
			printf ("\t\tOpcao %d do Modo_Cliente foi escolhida.\n", modo);
			if (Opcao1_Cliente (c, sequencia, entrada))
				return 1;
			break;
		case 2:												// Edita linha de um arquivo
			printf ("\t\tOpcao %d do Modo_Cliente foi escolhida.\n", modo);
			if (Opcao2_Cliente (c, sequencia, entrada, linha, dados))
				return 1;
			break;
		case 3:												// Acrescenta dados a um arquivo
			printf ("\t\tOpcao %d do Modo_Cliente foi escolhida.\n", modo);
			if (Opcao3_Cliente (c, sequencia, entrada, dados))
				return 1;
			break;
		case 4:												// Entra em um diretorio
			printf ("\t\tOpcao %d do Modo_Cliente foi escolhida.\n", modo);
			if (Opcao4_Cliente (c, sequencia, entrada))
				return 1;
			break;
		case 5:												// Lista conteudo de um diretorio
			printf ("\t\tOpcao %d do Modo_Cliente foi escolhida.\n", modo);
			if (Opcao5_Cliente (c, sequencia, entrada))
				return 1;
			break;
	}

	return 0;
}

/* Manda um pacote de aviso ao servidor. Retorna 1 caso este pacote seja entregue com sucesso, e 0
caso contrario. */
int Manda_Pacote_Ao_Servidor (int c, int sequencia, int tipo, char *entrada)
{
	printf ("\t\t\t>>>>> Manda_Pacote_Ao_Servidor\n");
	protocolo pacote;

	pacote = Monta_Pacote (sequencia++, tipo, entrada);				// Monta pacote e incrementa o numero da sequencia
	if (!Envia_Pacote (c, pacote))								// Se pacote foi enviado com sucesso
		return 0;

	return 1;
}

/* Recebe pacote do servidor (tipos Y, N, E, Z). Retorna 3 caso o pacote recebido seja do TIPO_E;
2, caso seja do TIPO_N; 1, caso seja do TIPO_Y; 0, caso seja do TIPO_Z. */
int Recebe_Pacote_Do_Servidor (int c)
{
	printf ("\t\t\t>>>>> Recebe_Pacote_Do_Servidor\n");
	protocolo pacote;
	ssize_t recebidos;
	int tamanho, sequencia, tipo, paridade;
	char *dados;

	if (!PACOTE_RECEBIDO)
		fprintf (stderr, "\t\tErro na funcao recv. Nenhum pacote foi recebido pelo servidor.\n");
	PACOTE_RECEBIDO = 1;										// Reseta variavel global
	pacote = Recebe_Pacote (c);									// Recebe pacote de controle (atributo) do protocolo
	dados = strdup (Desmonta_Pacote (pacote, &tamanho, &sequencia, &tipo, &paridade));	// Desmonta o pacote recebido
	switch (tipo)												// Verifica o tipo recebido
	{
		case TIPO_E:
			return 3;
		case TIPO_N:
			return 2;
		case TIPO_Y:
			return 1;
		case TIPO_Z:
			return 0;
	}
}

/* Envia ao servidor um pacote informando que o conteudo de um arquivo deve ser mostrado na tela.
Retorna 1 caso seja recebido do servidor uma confirmacao de que o pacote informativo foi recebido
com sucesso, e 0 caso contrario. */
int Opcao1_Cliente (int c, int sequencia, char *arquivo)
{
	printf ("\t\t\t>>>>> Opcao1_Cliente\n");
	int tipo1, tipo2;

	if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_F, arquivo))	//->->->->->->->->->-//
	{
		tipo1 = Recebe_Pacote_Do_Servidor (c);
		switch (tipo1)
		{
			case 3:										// Devido a ocorrencia de erro, programa sera abortado
				return 0;
			case 2:										// Como pacote recebido nao foi recebido corretamente, manda de novo
//				MANDA_DE_NOVO
			case 1:										// Prossegue com a execucao
				if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_X, arquivo))	//->->->->->->->->->-//
				{
					tipo2 = Recebe_Pacote_Do_Servidor (c);
					switch (tipo2)
					{
						case 3:
							return 0;
						case 2:
//							MANDA_DE_NOVO
						case 1:
							return 1;
					}
				}
				else
					return 0;
		}
	}
	else
		return 0;
}

/* Envia ao servidor um pacote informando que deve-se editar uma linha de um arquivo do servidor.
Retorna 1 caso seja recebido do servidor uma confirmacao de que o pacote informativo foi recebido
com sucesso, e 0 caso contrario. */
int Opcao2_Cliente (int c, int sequencia, char *arquivo, int linha, char *dados)
{
	printf ("\t\t\t>>>>> Opcao2_Cliente\n");
	int enviou = 0, i, j = 0, numero_pacotes, tipo1, tipo2, tipo3, tipo4;
	char **matriz, s_linha[TAM_CONVERSAO];

	while (!enviou)
	{
		if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_F, arquivo))	//->->->->->->->->->-//
		{
			tipo1 = Recebe_Pacote_Do_Servidor (c);
			switch (tipo1)
			{
				case 3:										// Devido a ocorrencia de erro, programa sera abortado
					return 0;
				case 2:										// Como pacote recebido nao foi recebido corretamente, manda de novo
//					MANDA_DE_NOVO
				case 1:										// Prossegue com a execucao
					if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_W, arquivo))	//->->->->->->->->->-//
					{
						tipo2 = Recebe_Pacote_Do_Servidor (c);
						switch (tipo2)
						{
							case 3:
								return 0;
							case 2:
//								MANDA_DE_NOVO
							case 1:
								snprintf (s_linha, TAM_CONVERSAO, "%d", linha);	// Converte o tipo para a string s_tipo
								if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_G, s_linha))	//->->->->->->->->->-//
								{
									tipo3 = Recebe_Pacote_Do_Servidor (c);
									switch (tipo3)
									{
										case 3:
											return 0;
										case 2:
//											MANDA_DE_NOVO
										case 1:
											numero_pacotes = strlen (dados) / TAM_PACOTE + 1;
											matriz = (char**) calloc (numero_pacotes, sizeof (char*));
											for (i = 0; i < numero_pacotes; i++)
											{
												matriz[i] = (char*) calloc (TAM_PACOTE, sizeof (char));
												matriz[i] = strdup (Divide_Pacote (&dados));
											}
											if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_D, matriz[j]))	//->->->->->->->->->-//
											{
												tipo4 = Recebe_Pacote_Do_Servidor (c);
												switch (tipo4)
												{
													case 3:
														return 0;
													case 2:
//														MANDA_DE_NOVO
													case 1:				// Envia proximo pacote
														j++;
														break;
													case 0:				// Envio da sequencia de pacotes terminou
														enviou = 1;
														break;
												}
											}
											else
												return 0;
									}
								}
								else
									return 0;
						}
					}
					else
						return 0;
			}
		}
		else
			return 0;
	}
}

/* Envia ao servidor um pacote informando que deve-se acrescentar dados a um arquivo do servidor.
Retorna 1 caso seja recebido do servidor uma confirmacao de que o pacote informativo foi recebido
com sucesso, e 0 caso contrario. */
int Opcao3_Cliente (int c, int sequencia, char *arquivo, char *dados)
{
	printf ("\t\t\t>>>>> Opcao3_Cliente\n");
	int enviou = 0, i, j = 0, numero_pacotes, tipo1, tipo2, tipo3;
	char **matriz;

	while (!enviou)
	{
		if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_F, arquivo))	//->->->->->->->->->-//
		{
			tipo1 = Recebe_Pacote_Do_Servidor (c);
			switch (tipo1)
			{
				case 3:										// Devido a ocorrencia de erro, programa sera abortado
					return 0;
				case 2:										// Como pacote recebido nao foi recebido corretamente, manda de novo
//					MANDA_DE_NOVO
				case 1:										// Prossegue com a execucao
					if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_A, arquivo))	//->->->->->->->->->-//
					{
						tipo2 = Recebe_Pacote_Do_Servidor (c);
						switch (tipo2)
						{
							case 3:
								return 0;
							case 2:
//								MANDA_DE_NOVO
							case 1:
								numero_pacotes = strlen (dados) / TAM_PACOTE + 1;
								matriz = (char**) calloc (numero_pacotes, sizeof (char*));
								for (i = 0; i < numero_pacotes; i++)
								{
									matriz[i] = (char*) calloc (TAM_PACOTE, sizeof (char));
									matriz[i] = strdup (Divide_Pacote (&dados));
								}
								if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_D, matriz[j]))	//->->->->->->->->->-//
								{
									tipo3 = Recebe_Pacote_Do_Servidor (c);
									switch (tipo3)
									{
										case 3:
											return 0;
										case 2:
//											MANDA_DE_NOVO
										case 1:				// Envia proximo pacote
											j++;
											break;
										case 0:				// Envio da sequencia de pacotes terminou
											enviou = 1;
											break;
									}
								}
								else
									return 0;
						}
					}
					else
						return 0;
			}
		}
		else
			return 0;
	}
}

/* Envia ao servidor um pacote informando que deve-se entrar em um diretorio do servidor. Retorna 1
caso seja recebido do servidor uma confirmacao de que o pacote informativo foi recebido com
sucesso, e 0 caso contrario. */
int Opcao4_Cliente (int c, int sequencia, char *diretorio)
{
	printf ("\t\t\t>>>>> Opcao4_Cliente\n");
	int tipo;

	if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_C, diretorio))		//->->->->->->->->->-//
	{
		tipo = Recebe_Pacote_Do_Servidor (c);
		switch (tipo)
		{
			case 3:											// Devido a ocorrencia de erro, programa sera abortado
				return 0;
			case 2:											// Como pacote recebido nao foi recebido corretamente, manda de novo
//				MANDA_DE_NOVO
			case 1:											// Prossegue com a execucao
				return 1;
		}
	}
	else
		return 0;
}

/* Envia ao servidor um pacote informando que deve-se listar o conteudo de um diretorio do
servidor. Retorna 1 caso seja recebido do servidor uma confirmacao de que o pacote informativo
foi recebido com sucesso, e 0 caso contrario. */
int Opcao5_Cliente (int c, int sequencia, char *diretorio)
{
	printf ("\t\t\t>>>>> Opcao5_Cliente\n");
	int tipo;

	if (Manda_Pacote_Ao_Servidor (c, sequencia, TIPO_L, diretorio))		//->->->->->->->->->-//
	{
		tipo = Recebe_Pacote_Do_Servidor (c);
		switch (tipo)
		{
			case 3:											// Devido a ocorrencia de erro, programa sera abortado
				return 0;
			case 2:											// Como pacote recebido nao foi recebido corretamente, manda de novo
//				MANDA_DE_NOVO
			case 1:											// Prossegue com a execucao
				return 1;
		}
	}
	else
		return 0;
}
