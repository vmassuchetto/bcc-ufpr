# include "main.h"

/* Acrescenta dados a um arquivo. Retorna 2 caso o fim de arquivo seja atingido, 1 caso a funcao
seja executada sem erros, e 0 caso ocorra erro na funcao. */
int Acrescenta_Dados (char *arquivo, char *dados)
{
	FILE *fp;
	int sem_erro = 1;

	if (fp = fopen (arquivo, "a+b"))								// Acrescenta dados ou cria o arquivo para leitura e escrita
	{
		fprintf (fp, "\n", dados);
		fprintf (fp, "%s", dados);
	}
	else
		Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
	fclose (fp);
	if (strlen (dados) < TAM_PACOTE)								// Verifica se o fim de arquivo sera atingido
		return 2;

	return sem_erro;
}

/* Divide pacote original em um sub-pacote de tamanho TAM_PACOTE. Retorna este sub-pacote e,
indiretamente, o conteudo do pacote original (sem o conteudo do sub-pacote). */
char *Divide_Pacote (char **dados)
{
	int i = 0;
	char *copia = strdup (dados[0]), *pacote = strdup ("");

	strncat (pacote, dados[0], TAM_PACOTE);							// Cria a string que sera retornada pela funcao
	do dados[0][i] = copia[i++ + TAM_PACOTE];
	while (dados[0][i]);

	return pacote;
}

/* Detecta se o arquivo lido existe. Retorna 0 caso ele exista (ou seja, se nao ocorreu erro), e 1
caso contrario (ou seja, se ocorreu erro). */
int Erro_1 (char *arquivo)
{
	printf ("\t\t\t>>>>> Erro_1\n");
	int arquivo_existe;
	char *comando;

	comando = (char*) calloc (TAM_PACOTE, sizeof (char));
	strcpy (comando, "test -e ");
	strcat (comando, arquivo);									// Verifica se arquivo lido existe
	arquivo_existe = system (comando);								// Se arquivo existe, system retorna 0
	if (!arquivo_existe)										// Testa se arquivo existe
		return 0;
	else
		fprintf (stderr, "\t\tErro detectado. Arquivo %s nao existe.\n", arquivo);

	return 1;
}

/* Detecta se arquivo ou diretorio tem permissoes validas. Retorna 1 em caso de permissoes
invalidas, e 0 caso contrario. */
int Erro_2 (char *entrada)
{
	printf ("\t\t\t>>>>> Erro_2\n");
	FILE *fp_entrada, *fp_saida;
	struct stat status;
	int i, tipo = 2, tamanho, sem_erro = 1;
	char *permissoes, buffer[7];

	if (fp_entrada = fopen (entrada, "rb"))						 	// Detecta erro na abertura do arquivo
	{
		if (!stat (entrada, &status))
		{
			fp_saida = fopen ("permissoes.txt", "wb");				// Cria um arquivo auxiliar para armazenar a permissao do arquivo de entrada (com o
			fprintf (fp_saida, "%o", status.st_mode);				// intuito final de converte-lo de octal para string)
			fclose (fp_saida);
			fp_saida = fopen ("permissoes.txt", "rb");				// Abre o arquivo criado anteriormente e armazena o conteudo da permissao do arquivo de
			fscanf (fp_saida, "%s", buffer);						// entrada na variavel buffer
			fclose (fp_entrada);
			fclose (fp_saida);
			permissoes = (char*) calloc (4, sizeof (char));			// Aloca espaco para a string permissao
			tamanho = strlen (buffer);
			if (tamanho == 6)									// Se tamanho = 6, permissoes sao de um arquivo. Caso contrario, de diretorio
				tipo = 3;
			for (i = tipo; buffer[i]; i++)						// Cria a string permissoes
				permissoes[i - tipo] = buffer[i];
			remove ("permissoes.txt");							// Remove o arquivo temporario
			if (permissoes[0] == '6' || permissoes[0] == '7')			// Verifica se arquivo (ou diretorio) possui permissao de leitura e escrita
				sem_erro = 0;
		}
		else
			fprintf (stderr, "\t\tErro na funcao stat. Nao foi possivel checar as permissoes do arquivo %s.\n", entrada);
	}
	else
		Erro_Leitura_Arquivo (&sem_erro, 1, entrada);

	return sem_erro;
}

/* Verifica se diretorio existe. Retorna 1 se ocorreu erro, e 0 caso contrario. */
int Erro_4 (char *diretorio)
{
	printf ("\t\t\t>>>>> Erro_4\n");
	if (chdir (diretorio) != 0)									// Verifica se diretorio existe
		return 1;

	return 0;
}

/* Executa o comando CD no diretorio passado como argumento. Retorna 1 caso o comando seja
executado com sucesso, e 0 caso contrario. */
int Executa_CD (char *diretorio)
{
	int resultado;
//	char *comando = strdup ("cd");

//	strcat (comando, diretorio);
//	resultado = system (comando);
	resultado = system ("cd");
	if (resultado)									// Muda de diretorio
	{
		fprintf (stderr, "\t\tErro no comando cd. Diretorio especificado nao existe.\n");
		return 0;
	}
	else
		printf ("\t\tDiretorio %s adentrado com sucesso!", diretorio);

	return 1;
}

/* Retorna 1 caso o comando seja executado com sucesso, e 0 caso contrario. */
int Executa_LS (char *diretorio)
{
	int resultado;
//	char *comando = strdup ("ls -lh ");

//	strcat (comando, diretorio);									// Cria string com o comando que sera executado
//	resultado = system (comando);									// Executa o comando acima
	resultado = system ("ls -lh");
	if (resultado == -1)
	{
		fprintf (stderr, "\t\tErro no comando ls.\n");
		return 0;
	}

	return 1;
}

/* Mostra o conteudo do arquivo. Retorna 1 caso a funcao seja executada com sucesso, e 0 caso
contrario. */
int Mostra_Conteudo (char *arquivo)
{
	int retorno;
	char *comando = strdup ("less ");

	strcat (comando, arquivo);
	retorno = system (comando);

	if (retorno)												// Se der erro de leitura, o comando less retorna 1. Ou seja, esta funcao retorna 0
		return 0;

	return 1;
}

/* Edita a linha especificada. Retorna 2 caso o fim de arquivo seja atingido, 1 caso a funcao
seja executada sem erros, e 0 caso ocorra erro na funcao. */
int Edita_Linha (char *arquivo, char *dados, int linha)
{
	FILE *fp_arquivo, *fp_inicio, *fp_fim, *fp_saida;
	int i = 1, cont = 0, sem_erro = 1, linhas_saida;
	char *inicio, *fim, *saida, **matriz_inicio, **matriz_fim, **matriz_saida, buffer[TAM_BUFFER], *comando = strdup ("mv saida.txt ");

	// ----- Inicializa nome dos arquivos auxiliares, e matrizes que gerarao estes arquivos -----
	inicio = strdup ("inicio.txt");
	fim = strdup ("fim.txt");
	saida = strdup ("saida.txt");
	matriz_inicio = (char**) calloc (1, sizeof (char*));				// Aloca 1 linha para matriz_inicio
	matriz_fim = (char**) calloc (1, sizeof (char*));					// Aloca 1 linha para matriz_fim
	matriz_saida = (char**) calloc (1, sizeof (char*));				// Aloca 1 linha para matriz_saida
	// ----- Faz o que o programa se propoe -----
	if (fp_arquivo = fopen (arquivo, "a+b"))						// Acrescenta dados ou cria o arquivo para leitura e escrita
	{
		// Cria matriz que gerara o arquivo inicio.txt
		while (cont != linha && sem_erro)							// Enquanto nao cheguei a linha especificada, e nao ocorreu erro
		{
			if (fgets (buffer, TAM_BUFFER, fp_arquivo) != NULL)
			{
				matriz_inicio[cont] = strdup (buffer);										// Aloca espaco para a linha atual de matriz_inicio
				matriz_inicio = (char**) realloc (matriz_inicio, (i++ + 1) * sizeof (char*));		// Realoca mais 1 linha para matriz_inicio
				cont++;																// Incrementa o contador da linha
			}
			else
				Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
		}
		// ----- Cria arquivo inicio.txt -----
		if (sem_erro)
		{
			if (fp_inicio = fopen (inicio, "a+b"))					// Acrescenta dados ou cria o arquivo para leitura e escrita
				for (i = 0; i < cont; i++)						// Copia o conteudo de entrada.txt, ate a linha especificada, para inicio.txt
					fprintf (fp_inicio, "%s", matriz_inicio[i]);
			else
				Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
		}
		// ----- Cria matriz que gerara o arquivo fim.txt -----
		i = 1;
		cont = 0;
		fgets (buffer, TAM_BUFFER, fp_arquivo);						// Le a linha especificada, e nao faz nada com ela, pois ela sera editada
		while (!(feof (fp_arquivo)))								// Enquanto nao cheguei ao fim do arquivo
		{
			if (fgets (buffer, TAM_BUFFER, fp_arquivo) != NULL)
			{
				matriz_fim[cont++] = strdup (buffer);										// Aloca espaco para a linha atual de matriz_fim
				matriz_fim = (char**) realloc (matriz_fim, (i++ + 1) * sizeof (char*));			// Realoca mais 1 linha para matriz_fim
			}
		}
		// ----- Cria arquivo fim.txt -----
		if (fp_fim = fopen (fim, "a+b"))							// Acrescenta dados ou cria o arquivo para leitura e escrita
			for (i = 0; i < cont; i++)							// Copia o conteudo de entrada.txt, apos a linha especificada, para fim.txt
				fprintf (fp_fim, "%s", matriz_fim[i]);
		else
				Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
		// ----- Cria matriz que gerara o arquivo saida.txt -----
		for (i = 0; i < linha; i++)
		{	
			matriz_saida[i] = strdup (matriz_inicio[i]);
			matriz_saida = (char**) realloc (matriz_saida, (i + 2) * sizeof (char*));				// Realoca mais 1 linha para matriz_saida
		}
		strcpy (matriz_saida[i], dados);
		matriz_saida = (char**) realloc (matriz_saida, (i + 2) * sizeof (char*));
		linhas_saida = i + 1;
		for (i = 0; i < cont; i++)
		{
			matriz_saida[linhas_saida] = strdup (matriz_fim[i]);
			matriz_saida = (char**) realloc (matriz_saida, (linhas_saida++ + (i + 2)) * sizeof (char*));
		}
		// ----- Cria arquivo saida.txt -----
		if (fp_saida = fopen (saida, "a+b"))						// Acrescenta dados ou cria o arquivo para leitura e escrita
			for (i = 0; i < linhas_saida; i++)						// Gera o arquivo saida.txt
				fprintf (fp_saida, "%s", matriz_saida[i]);
		else
				Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
	}
	else
				Erro_Leitura_Arquivo (&sem_erro, 0, arquivo);
	// ----- Libera ponteiros dos arquivos na memoria, remove os arquivos temporarios e renomeia o arquivo de entrada -----
	strcat (comando, arquivo);									// Cria string que renomeara arquivo gerado para arquivo de entrada
	system (comando);											// Renomeia arquivo de entrada
	remove (inicio);
	remove (fim);
	fclose (fp_arquivo);										// Fecha arquivos
	fclose (fp_inicio);
	fclose (fp_fim);
	fclose (fp_saida);

	if (strlen (dados) < TAM_PACOTE)								// Verifica se o fim de arquivo sera atingido
		return 2;

	return sem_erro;
}
