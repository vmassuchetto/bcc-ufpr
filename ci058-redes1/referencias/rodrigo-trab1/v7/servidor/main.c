# include "main.h"

void Modo_Servidor (char **argv)
{
	int c;
	char* mensagem = (char*) malloc (sizeof (char) * TAM_BUFFER);

	c = Rawsocket ("eth0");
	if (!Recebe_Mensagem (c, mensagem, argv))
		printf ("\nEscrita finalizada!\n");
}

int Recebe_Mensagem (int c, char *mensagem, char **argv)
{
	ssize_t recebidos;
	FILE *fp;
	int fim_while = 0, tamanho_mensagem, i, indice;
	char matriz[TAM_BUFFER][TAM_BUFFER], buffer[TAM_BUFFER], *nome_arquivo;

	nome_arquivo = strdup (argv[2]);									// Copia a 2a entrada da linha de comando para o nome do arquivo
	fp = fopen (nome_arquivo, "a+b");									// Abre o arquivo de texto para leitura e escrita. Caso o arquivo ja exista, o
																// conteudo sera anexado ao final do arquivo. Caso nao exista, este arquivo sera
	indice = 0;													// criado
	do															// Recebe mensagens ate que um ENTER seja dado, no editor de texto do cliente
	{
		recebidos = recv (c, buffer, TAM_BUFFER, 0);						// Recebe uma mensagem que contem o tamanho da mensagem principal
		if (recebidos < 0)
		{
			perror ("Erro na funcao \"recv\".\n");
			return 0;
		}
		
		tamanho_mensagem = atoi (buffer);								// Obtem o tamanho da mensagem principal que sera recebida
		recebidos = recv (c, buffer, TAM_BUFFER, 0);						// Recebe a mensagem de fato
		if (recebidos < 0)
		{
			perror ("Erro na funcao \"recv\".\n");
			return 0;
		}
		printf ("%s", buffer);
		strcpy (matriz[indice], buffer);								// Copia para a enesima linha da matriz a mensagem enviada

		if (strcasecmp (buffer, "fim\n") == 0)							// Detecta o final de transmissao, a fim de sair do WHILE
			fim_while = 1;
		else
			indice++;
	} while (!fim_while);

	for (i = 0; i < indice; i++)
		fprintf (fp, "%s", matriz[i]);								// Escreve cada linha lida no arquivo

	fclose (fp);													// Fecha o arquivo
	if (strcasecmp (buffer, "fim\n") == 0)								// Detecta o final de transmissao, a fim de retornar 0
		return 0;

	return 1;
}

int main (int argc, char **argv)
{
	if (strcmp (argv[1], "--servidor") == 0)
		Modo_Servidor (argv);
	
	return 0;
}
