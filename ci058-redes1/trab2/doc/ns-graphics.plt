set xlabel "Mensagens por Segundo "
set ylabel "Taxa de Entrega (%)"
set term png
set output "graphics/ns-tax.png"
set grid
set xrange [-1:5]
set yrange [30:105]
plot "ns-tax.dat" \
	using 2:xticlabels(1) \
	notitle \
	with lines \
	lw 3

set xlabel "Mensagens por Segundo "
set ylabel "Tempo Médio"
set term png
set output "graphics/ns-time.png"
set grid
set xrange [-1:5]
set yrange [0.025:0.04]
plot "ns-time.dat" \
	using 2:xticlabels(1) \
	notitle \
	with lines \
	lw 3
