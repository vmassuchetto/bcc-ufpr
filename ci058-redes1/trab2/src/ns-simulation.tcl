
# Arquivo Tcl do trabalho: Simulação de Redes Ethernet no NS-2
# UFPR - Bacharelado em Ciência da Computação
# Vinicius Massuchetto

if { $argc < 2 } {
	puts "Modo de uso: ns ns.tcl \$semente \$taxa \$arquivo"
	puts "  \$semente  Inteiro gerador de números randômicos"
	puts "  \$taxa     Mensagens por segundo das origens"
	puts "  \$arquivo  Prefixo dos arquivos .tr e .nam a serem gerados"
    exit
}

set ns [new Simulator]

set seed [lindex $argv 0]
set rate [lindex $argv 1]
set filename [lindex $argv 2]

$ns color 0 red
$ns color 1 blue
$ns color 2 green

set tracefile [open $filename.tr w]
$ns trace-all $tracefile

set namfile [open $filename.nam w]
$ns namtrace-all $namfile

# Cria os nós segundo especificação
# Ligação entre switches de 1Gb
# Ligação entre switches e máquinas de 10Mb

set k 11
set node(0) [$ns node]

for { set i 1 } { $i < 11 } { incr i } {

	set node($i) [$ns node]
	$ns duplex-link $node(0) $node($i) 1Gb 10ms DropTail

	for { set j 1 } { $j <= 7 } { incr j } {
		set node($k) [$ns node]
		$ns duplex-link $node($i) $node($k) 10Mb 10ms DropTail
		incr k
	}

}

# Seleciona aletoriamente origens e destinos
# $SRC Vetor de origens
# $DST Vetor de destinos

for { set i 0 } { $i < 10 } { incr i } {

	set srand [expr $seed + $i]

	set SRC($i) [expr int(rand()*81)]
	if { $i < 2 } {
		set DST($i) [expr int(rand()*81)]
	}

}

# Cria agentes nulos para os destinos

for { set i 0 } { $i < 2 } { incr i } {

	set dst $DST($i)

	set null($dst) [new Agent/Null]
	$ns attach-agent $node($dst) $null($dst)

}

# Cria tráfegos das origens para os destinos
# Agentes UDP
# Tráfego CBR

for { set i 0 } { $i < 10 } { incr i } {

	for { set j 0 } { $j < 2 } { incr j } {

		set src $SRC($i)
		set dst $DST($j)

		set udp($src) [new Agent/UDP]
		$ns attach-agent $node($src) $udp($src)

		set cbr($src) [new Application/Traffic/CBR]
		$cbr($src) set type_ CBR
		$cbr($src) set packet_size_ 512
		$cbr($src) set rate_ [expr 512 * 8 * $rate]
		$cbr($src) attach-agent $udp($src)

		$ns connect $udp($src) $null($dst)
		$ns at 0 "$cbr($src) start"

	}

}

$ns at 15.0 "end"

proc end {} {
	global ns tracefile namfile filename
	$ns flush-trace
	close $tracefile
	close $namfile
	exit 1
}

$ns run
