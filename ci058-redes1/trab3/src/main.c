#include "main.h"

int main (int argc, char** argv) {

    if (argc < 4) {
        printf ("Modo de uso: dgram [localhost port] ");
        printf ("[host name] [host port]\n");
        return 0;
	}

    int sock_local = server_mount_socket ();
    struct sockaddr_in local;
    server_mount_local (&local, argv[1], sock_local);

    int sock_host = server_mount_socket ();
	struct sockaddr_in host;
    server_mount_host (&host, argv[2], argv[3]);

    int token = (argv[4] != NULL) ? 1 : 0;

    server_loop (sock_local, &local, sock_host, &host, token);

    return 0;
}
