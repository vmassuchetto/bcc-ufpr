#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int value = 5;

int main() {
    pid_t pid;
    pid = fork();

    printf("PID: %d - value = %d\n", pid, value);

    if (pid == 0) {
        value += 15;
    } else if (pid > 0) {
        wait(NULL);
        printf("PARENT: value = %d\n", value); /* A */
    }

    return 0;
}
