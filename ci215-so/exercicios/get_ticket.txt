CI215 - Sistemas Operacionais

Exercicios sobre semaforos (monitores tamb´em)
--------------------------

----------------------------------------------------------------------------
5a) Faca duas funcoes em linguagem C, que possam ser usada por THREADS 
    COOPERANTES ou por PROCESSOS COOPERANTES. A funcao get_ticket() retornar 
    um numero inteiro positivo UNICO a qualquer thread ou processo 
    cooperante que chama-la.  Prototipo:

		int get_ticket();

    A funcao return_ticket( int ) deve receber "de volta" um ticket (numero 
    inteiro), supostamente obtido anteriormente via get_ticket, de modo 
    que esse ticket possa ser "reutilizado" por get_tickets subsequentes.
    Prototipo:  
	       void return_ticket( int t );

5b) Mostre (explique) brevemente porque sua get_ticket retorna inteiros 
    UNICOS i.e. nunca retorna valores repetidos.
    OBS: Caso seja necessario, sua get_ticket pode ter um numero maximo de 
    tickets (NMAX), nesse caso, ela deve retornar -1 para indicar que nao 
    pode mais retornar valores novos.

6) Idem de 5) mas com complexidade O(1) de get/return tickets, 
   considerando O(1) a complexidade do sem´aforo 
   (ou condition variable/monitores)
-----------------------------------------------------------------------------
