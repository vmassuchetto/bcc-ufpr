sucessor(L1, L2) :-
    L = [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z],
    sucessor2(L, _, L1, L2).

sucessor2([H|_], E, L1, L2) :-
    H == L2,
    E == L1.
sucessor2([H|T], _, L1, L2) :-
    sucessor2(T, H, L1, L2).

codificada([H|T], N, _) :-
    codificada2([H|T], 0, N, _).

codificada2([_|T], I1, N, _) :-
    I is I1 + 1,
    write(I),nl,
    I < N,
    codificada2(T, I, N, _).
