:- dynamic(next_bound/1).
:- dynamic(dest/1).

successor(a, s, 140).
successor(a, z, 75).
successor(a, t, 118).
successor(z, a, 75).
successor(z, o, 71).
successor(o, z, 71).
successor(o, s, 151).
successor(s, a, 140).
successor(s, o, 151).
successor(s, r, 80).
successor(s, f, 99).
successor(t, a, 118).
successor(t, l, 111).
successor(l, t, 111).
successor(l, m, 70).
successor(m, l, 70).
successor(m, d, 75).
successor(d, m, 75).
successor(d, c, 120).
successor(c, d, 120).
successor(c, r, 146).
successor(c, p, 138).
successor(r, c, 146).
successor(r, s, 80).
successor(r, p, 97).
successor(p, c, 138).
successor(p, r, 97).
successor(p, b, 101).
successor(f, s, 99).
successor(f, b, 211).
successor(b, f, 211).
successor(b, p, 101).
successor(b, g, 90).
successor(b, u, 85).
successor(g, b, 90).
successor(u, b, 85).
successor(u, h, 98).
successor(u, v, 142).
successor(h, u, 98).
successor(h, e, 86).
successor(e, h, 86).
successor(v, u, 142).
successor(v, i, 92).
successor(i, v, 92).
successor(i, n, 87).
successor(n, i, 87).

coord(a, 17, 162).
coord(z, 28, 190).
coord(o, 44, 210).
coord(t, 21, 94).
coord(l, 65, 75).
coord(m, 78, 36).
coord(d, 91, 16).
coord(s, 98, 136).
coord(r, 115, 105).
coord(c, 147, 17).
coord(f, 164, 136).
coord(p, 170, 72).
coord(g, 217, 16).
coord(b, 229, 45).
coord(u, 254, 60).
coord(h, 292, 60).
coord(e, 293, 20).
coord(v, 276, 119).
coord(i, 242, 185).
coord(n, 193, 205).


reverse([X|Y],Z,W):-	
	reverse(Y,[X|Z],W).
reverse([],X,X).

%reverse([X|Y],Z,W):- reverse(Y,[X||\




imprime([X|Y],[]):-
	write('Estado Inicial: '),
	writeln(X),
	imprime(Y,X).
imprime([X|Y],Z):-
	write('vai para a cidade: '),
	writeln(X),
	imprime(Y,[X|Z]).
imprime([],_).

value(A, H):-
	dest(B),
	coord(A, X1, Y1),
	coord(B, X2, Y2),
	H is floor(sqrt((( X2 - X1 )*( X2 - X1 )) + (( Y2 - Y1 )*( Y2 - Y1 )))).


ida(Start, Destiny):-
	retractall(dest),
	asserta(dest(Destiny)),
	!,
	ida_star(Start, Soln),!,
write(Soln),
	reverse(Soln,[],W),!,
	%write(Soln),
	imprime(W,[]),!.

ida_star(Start, Soln) :- 
	retractall(next_bound), % in ALS prolog abolish(next_bound,1) 
	value(Start, H), 
	asserta(next_bound(infinity)), % set bound for next search. 
	ida_star(_,[], Start/0,H,Start, Soln).

ida_star(_,Path, Node/_, _,_, [Node|Path]) :- 

	value(Node,H),
	H == 0,!.
	%write('oioioi').
	%reverse([Node|Path],[],W),!,
	%write([Node|Path]),
	%imprime(W,[]).

ida_star(_,Path, Node/G, Bound, Start,Soln) :- 
	successor(Node, Node1, C), 
	not(member(Node1, Path)), % no looping. 
	G1 is G + C, 
	value(Node1, H1), 
	F1 is G1 + H1, 
	revise_next_bound(F1, Bound), 
	lesseq(F1,Bound),!, % acceptable node 
	ida_star(slave,[Node|Path], Node1/G1, Bound,Start,Soln).

ida_star(Parent,_,_/_, Bound, Start, Soln) :- 
	next_bound(Next), 
	(Next = Bound -> (write('Search has failed'),nl ) 
	; 
	( 
	% Do deeper search 
	reset_next_bound, 
	ida_star(Parent,[], Start/0, Next, Start, Soln) 
	))
	.
revise_next_bound(Val, Current) :- lesseq(Val, Current), !. 
revise_next_bound(Val,_ ) :- 
	next_bound(Next), 
	(better(Val, Next) -> 
	retract(next_bound(Next)), 
	asserta(next_bound(Val)); 
	true).

reset_next_bound :- retract(next_bound(_)), asserta(next_bound(infinity)).

better(_, infinity) :- !. 
better(Val, V1) :- 
	Val < V1, !.

lesseq(_, infinity) :- !. 
lesseq(F, Bd) :- F =< Bd.
