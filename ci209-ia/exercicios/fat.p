fat(1,1).
fat(X, FAT_X) :-
    Y is X-1,
    fat(Y, FAT_Y),
    FAT_X is X * FAT_Y.
