
  Otimização de Consultas
  -----------------------

  * Seja a consulta:
  
    SELECT nome, curso
    FROM aluno, matricula, disciplina
    WHERE 1
        AND aluno.grr = matricula.grr
        AND matricula.media > 3.9
        AND matricula.media < 7.0
        AND aluno.ano < 2008
  
    Para o seguinte esquema relacional:
  
    ALUNO       (nome, curso, sexo, endereço, telefone, ano, grr)
    MATRICULA   (aluno.grr, disciplina.codigo, media)
    DISCIPLINA  (codigo, nome, ementa, descrição)
    
    a) Construa a expressão algébrica equivalente;

        SELE (aluno, <aluno.ano < 2008>)
        PROD ( * , matricula)
        SELE ( * , <aluno.grr = matricula.aluno.grr>,
            <matricula.media > 3.9>, <matricula.media < 7.0>)
        PROD ( * , disciplina)
        SELE ( * , <matricula.disciplina.codigo = disciplina.codigo>)
        PROJ ( * , [aluno.nome, aluno.curso])


    b) Construa a árvore de execução otimizada;

        Regras equivalentes de álgebra relacional:
    
        * Seleções conjuntivas
          SELE (R, <a>, <b>, <c>) = SELE (SELE (SELE (R, <c>), <b>), <a>)

        * Seleções comutativas
          SELE (SELE (SELE (R, <c>), <b>), <a>) =
            SELE (SELE (SELE (R, <a>), <b>), <c>)

        * Projeções conjuntivas
          PROJ (R, [a]) = PROJ (PROJ (PROJ (R, [a,b,c]), [a,b]), [a])

        * Projeção e seleção comutativas
          SELE (PROJ (R, [l]), <a>) = PROJ (SELE (R, <a>), [l])

        * Seleção e produto comutativas
          SELE (PROD (R, S), <c>) = PROD (SELE (R, <c>), S)

        * Projeção e produto comutativas
          PROJ (PROD (R, S), [c]) = PROD (PROJ (R, [c1]), PROJ (S, [c2]))

        * Associatividade do produto
          PROD (A, PROD (B, C)) = PROD (PROD (A, B), C)

        Algoritmo de otimização de árvores:

        1. quebrar as seleções por conjuntividade;
        2. antecipar as seleções por comutatividade;
        3. arrumar as folhas por associatividade do produto;
        4. agrupar produtos;
        5. antecipar projeções por conjuntividade e comutatividade;
        6. identificar as sub-árvores.

        A = SELE (aluno, <aluno.ano < 2008>)
        A = PROJ (A, [nome, curso, ano, grr])

        M = SELE (matricula , <matricula.media > 3.9>)
        M = SELE (M , <matricula.media < 7.0>)
        M = PROJ (M, [aluno.grr, disciplina.codigo])

        D = PROJ (disciplina, [disciplina.codigo])

        PROD (A, M)
        SELE ( * , <aluno.grr = matricula.aluno.grr>)
        PROJ ( * , [aluno.nome, aluno.curso, disciplina.codigo])
        PROD ( * , D)
        SELE ( * , <matricula.disciplina.codigo = disciplina.codigo>)
        PROJ ( * , [aluno.nome, aluno.curso])

        Árvore cacônica:

                           PROJ
                            /
                         SELE
                          /
                        PROD
                       /    \
                     PROD   disciplina
                    /    \
                 aluno  matricula


        Árvore otimizada:

                          PROJ
                      [nome,curso]
                           |
                          SELE
                     codigo=codigo
                           |
                          PROD
                         /    \
                     PROJ      PROJ
                 [nome,curso  [codigo]
                 disciplina]    |
                      |       disciplina
                     SELE
                   grr=grr
                      |
                     PROD
                   /      \
               SELE        PROJ
             ano<2008    [aluno.grr,
                |       disciplina.codigo]
                |           |
               PROJ        SELE
          [nome,curso,   media<3.9
             ano,grr]    media>7.0
                |           |
              aluno      matricula


    c) Determine as subárvores de execução.
    
       Subárvores A, M e D da expressão acima.


  Controle de Concorrência
  ------------------------

  * O lock binário ou compartilhado garantem a seriabilidade das transações?

    Protocolo de Lock Binário
    
    - Lock antes e unlock depois de cada r(x) e w(x);
    - Uma transação não pede lock nem faz unlock se já o possui;
    - Não há diferença entre r(x) e w(x);
    - Um usuário pode travar um registro por muito tempo.
    
    T1 | ----- l(x) -- r(x) ------- w(x) -- u(x) -----------
    T2 | ------------- p(x) --------------------- l(x) -----
    
    
    Protocolo de Lock Compartilhado
    
    - Só se faz lock de escrita quando não há nenhum lock;
    - Locks de leitura são feitos somente quando não há nenhum lock ou só há
      locks de leitura ativos;

    write_lock(x):
        se lock(x) == NULL
            lock(x) = 'w'
        senão
            espera
    
    read_lock(x):
        se lock(x) == NULL
            lock(x) = 'r'
            numreads = 1
        senão se lock(x) == 'r'
            numreads++
        senão
            espera

    unlock(x):
        se lock(x) == 'w'
            lock(x) = NULL
        senão se lock(x) == 'r'
            numreads--
            se numreads == 0
                lock(x) = NULL


    Em ambos estes casos, a liberação dos elementos ocorre antes dos locks,
    o que pode levar à formação de ciclos.
    
    T1 | -- wl(x) - r(x) - w(x) - ul(x) - wl(y) - r(y) - w(y) - ul(y)
    T2 | -- wl(y) - r(y) - w(y) - ul(y) - wl(x) - r(x) - w(x) - ul(x)


  * Prove que o lock de duas fases garante a seriabilidade.
  
    Uma vez feito o lock, não se pode mais fazer outra coisa com aquele item.
    
    T1 | -- wl(x) ------- ul(x) --------------------------------------
    T2 | ---------------------- wl(x) ---------- ul(x) ---------------
    T3 | --------------------------------------------- wl(x) --- ul(x)
  

  Recuperação de Falhas
  ---------------------

  * Descreva a estrutura mínima de um log em alteração imediata e alteração
    tardia.

    Em ambientes de alteração imedata desfaz-se as transaçõs que estavam
    ativas durante a parada do sistema, e para isso é necessário guardar todo e
    qualquer valor que tenha sido substituído.

    Alteração Imediata  id, timestamp, registro, ação,
                        valor novo, valor antigo
    Alteração Tardia    id, timestamp, registro, ação, valor novo


  * Proponha um mecanismo de recuperação de falhas que dispensa a memorização
    dos itens no log.

    Um modo de se evitar a memorização dos valores a serem gravados no log é
    usar cópias de páginas no disco, ou como é chamado: "shadow paging".
    Gravam-se os registros em uma página diferente da original, e somente
    quando ela estiver pronta substitui-se a referência da página original.


  * Seja o seguinte log:

    <start, t1>
    <r, 10, 30, x, t1>
    <start, t2>
    <r, 20, 35, y, t1>
    <start, t3>
    <r, 45, 75, w, t2>
    <r, 14, 27, m, t3>
    <w, 35, 20, y, t1>
    <commit, t1>
    <w, 42, 29, w, t2>
    <checkpoint>
    <w, 75, 25, m, t3>
    <commit, t2>
    <r, 27, 47, n, t3>
    /* crash */

    a) Mostre a estratégia de recuperação para alteração tardia e imediata.

        T1 | * r(x) - r(y) ------------ w(y) -- *        |                |
        T2 |        * ------- r(w) -------------- w(w) --|------ *        |
        T3 |               * ----- r(m) -----------------|- w(m) - r(n) --|
                                                     checkpoint         crash

        Para ambos os ambientes, nada é feito para T1, pois o checkpoint
        verificou sua terminação correta.

        T2 é refeita em ambos os ambientes para garantir que os dados foram
        escritos corretamente no disco.

        T3 é abortada em ambiente de alteração tardia - pois nada é escrito
        em disco antes do commit, e em alteração imediata ela será desfeita
        - pois dados inconsistentes podem ter sido gravados durante sua
        execução.

    b) Determine se pode haver cascata de rollbacks.

        Haverá rollback em cascata se T3 depender de T2 ou de T1.


  Processamento Distribuído
  -------------------------

  * Sejam as seguintes regras:
   
    AC1: Todos os nós devem chegar à mesma decisão;
    AC2: Após tomar uma decisão um nó não pode revertê-la;
    AC3: Um commit só pode ser aceito se todos tiverem decisão positiva;
    AC4: Se as falhas forem reparadas, então todos os nós podem chegar
         à uma decisão;
    
    Proponha uma alteração no protocolo 2PC para viabilizar AC4.
    
    AC4: Se menos da metade dos nós aceitarem o commit, então aborta-se;
    AC5: Se mais da metade dos nós e não todos os commits forem aceitos,
         entra-se em timeout;
    AC6: Se o limite de timeout for atigido, aborta-se a operação.
    
    Esquema das operações:
        [1] Inicia uma nova operação
        [2] Requere preparação da transação aos nós
            [2] Em caso de falha
            [3] Em caso de sucesso
        [3] Efetua o commit
            [1] Em caso de sucesso
            [2] Se menos da metade efetuar o commit
            [4] Se mais da metade efetuar o commit
        [4] Timeout
            [1] Se todos os nós retornarem sucesso
            [2] Se atingir o limite do timeout


  * Dados dois sites S1 e S2, com 20.000 tuplas em S1, e 100.000 em S2:

    S1: chave [2b], nome [1k], nacionalidade [30b], endereço [2k]
    S2: chave [2b], telefone [4b], escolaridade [10b], histórico [2k]

    Com query executada em S1, defina a quantidade de dados a ser transferida
    para executar:   
  
    a)  
        SELECT nome
        FROM s1, s2
        WHERE 1
            AND s1.nacionalidade = 'brasileira'
            AND s2.escolaridade = 'médio'

        Seleção ocorre em S1, e manda somente as chaves para S2. Faz-se a
        junção em S2 e retorna-se somente o resultado.
       
        Supondo que a seguinte query retorna 100 resultados:
        
            SELECT nome
            FROM s1
            WHERE nacionalidade = 'brasileira'
       
        Envio:          400 bytes
        Recebimento:    134 bytes
        Total:          534 bytes


    b)
        SELECT nome, escolaridade
        FROM s1, s2
        WHERE 1
            AND s1.k = s2.k
        
        É preciso enviar todas as chaves de s1 para s2, fazer a junção em s2
        e então retornar o resultado para exibição em s1.
        
        Envio:          20.000 tuplas * 2 bytes por chave = 40k
        Recebimento:    20.000 tuplas * 10 bytes da escolaridade = 200k
                        20.000 tuplas * 2 bytes por chave = 40k
        Total:          280k
