##   O objetivo deste trabalho é implementar um gerenciador de memória da bss. Este gerenciador 
##deverá ser inteiramente implementado em assembly
##
##   Escrito por Fernando S. Ishii (fernando.ishii at gmail.com)
##

.section .data

inicio_heap:    .long 0
brk_atual:      .long 0

.equ    TAM_HDR, 8              # Tamanho do cabeçalho
.equ    HDR_DISPONIVEL_POS, 0   # Posicao da flag 'disponivel' no cabeçalho
.equ    HDR_TAM_POS, 4          # Posicai do 'tamanho' no cabeçalho

.equ    OCUPADO, 0  # Constante para o status de memoria ocupada
.equ    LIVRE, 1    # Constante para o status de memoria livre

#Strings
InicioBss:      .string "\nInicio da bss: %p\n"
Seg1:           .string "Segmento %d: %d bytes ocupados\n"
Seg2:           .string "Segmento %d: %d bytes livres\n"
SegOcupados:    .string "Segmentos Ocupados: %d / %d bytes\n"
SegLivres:      .string "Segmentos Livres: %d / %d bytes\n----------------------------- \n"

.section .text

######################################
##                                  ##
## meuAlocaMem: ALOCADOR DE MEMORIA ##
##                                  ##
######################################
.globl  meuAlocaMem
.type   meuAlocaMem, @function
.equ    TAM_MEM, 8

#REGISTRADORES
# %eax - região corrente de memória sendo examinada
# %ebx - posição corrente da brk
# %ecx - TAM_MEM
# %edx - tamanho do segmento de memoria corrente


meuAlocaMem:
    pushl   %ebp
    movl    %esp, %ebp

    ## Se a heap esta vazia ##
    ##     aumenta a brk    ##
    if:
        cmpl    $0, inicio_heap			
        jne     end_if
        cmpl    $0, brk_atual
        jne     end_if
        movl    $45, %eax
        movl    $0, %ebx
        int     $0x80
        incl    %eax
        # Salva os novos valores nas variaveis globais
        movl    %eax, brk_atual
        movl    %eax, inicio_heap

    end_if:
        movl    inicio_heap, %eax
        movl    brk_atual, %ebx
        movl    TAM_MEM(%ebp), %ecx

    ## Loop buscando pela memoria
    alloc_loop_begin:

        ##   Se o inicio e o fim da brk estiverem   ##
        ## na msm altura, precisamos alocar memoria ##
        cmpl    %ebx, %eax
        je      move_break
        movl    HDR_TAM_POS(%eax), %edx

        ## Se o espaço estiver ocupado ##
        ##    procura pelo proximo     ##
        cmpl    $OCUPADO, HDR_DISPONIVEL_POS(%eax)
        je      next_location

        ## Se o espaco for exatamente o ##
        ##         que procuramos       ##
        cmpl    %edx, %ecx
        je      alloca_aqui
		
        ## Se tiver espaço em um segmento  ##
        ## livre			   ##
        subl		$TAM_HDR, %edx
        cmpl		%edx, %ecx
        jl		alloca_livre
					 
        jmp next_location
	##
	##
    alloca_livre:
        movl	$OCUPADO,HDR_DISPONIVEL_POS(%eax)
        movl	HDR_TAM_POS(%eax), %edx
        movl	%ecx, HDR_TAM_POS(%eax)
        addl  $TAM_HDR,%eax
        addl	%ecx,%eax
        movl	$LIVRE,HDR_DISPONIVEL_POS(%eax)
        subl	%ecx,%edx
        subl	$TAM_HDR,%edx
        movl	%edx, HDR_TAM_POS(%eax)
        movl 	%ebp, %esp
        popl	%ebp
        ret
		
		
    ## Pula para o proximo ##
    ## segmento de memoria ##
    next_location:
        addl    $TAM_HDR, %eax
        addl    %edx, %eax
        jmp     alloc_loop_begin

    ## Este segmento tem exatamente ##
    ##   o tamanho que procuramos   ##
    alloca_aqui:
        movl    $OCUPADO, HDR_DISPONIVEL_POS(%eax)
        addl    $TAM_HDR, %eax
        movl    %ebp, %esp
        popl    %ebp
        ret

    ## Se nao existe nenhum segmento vazio de tamanho ##
    ##           suficiente, aumenta a brk            ##
    move_break:
        addl    $TAM_HDR, %ebx
        addl    %ecx, %ebx
        # Salva os registradores necessarios
        pushl   %eax
        pushl   %ecx
        pushl   %ebx
        movl    $45, %eax
        int     $0x80

        ## Se a syscall retorna 0 em ##
        ##    %eax, pula para erro   ##
        cmpl    $0, %eax
        je      error
        # Restaurar os registradores salvos
        popl    %ebx
        popl    %ecx
        popl    %eax
        movl    $OCUPADO, HDR_DISPONIVEL_POS(%eax)
        movl    %ecx, HDR_TAM_POS(%eax)
        addl    $TAM_HDR, %eax
        movl    %ebx, brk_atual
        movl    %ebp, %esp
        popl    %ebp
        ret

    ## Se der erro retornamor 0 ##
    error:
        movl    $0, %eax
        movl    %ebp, %esp
        popl    %ebp
        ret


##########################################
##                                      ##
## meuLiberaMem: DESALOCADOR DE MEMORIA ##
##                                      ##
###$$$$###################################
.globl meuLiberaMem
.type meuLiberaMem, @function
.equ PARAMETRO, 4

meuLiberaMem:

    ## Libera o endereco passado como parametro ##
    movl    PARAMETRO(%esp), %eax
    subl    $TAM_HDR, %eax
    movl    $LIVRE, HDR_DISPONIVEL_POS(%eax)

    ## Junta dois blocos livres de memoria consecutivos ##
    merge:
        movl inicio_heap, %eax
        movl %eax, %ebx

    while2:
        addl $TAM_HDR, %ebx
        addl HDR_TAM_POS(%eax), %ebx
        cmpl brk_atual, %ebx
        jge fim_while2

    if1:
        cmpl $LIVRE, HDR_DISPONIVEL_POS(%eax)
        jne fim_if1

        cmpl brk_atual, %ebx
        jge fim_if1

        cmpl $LIVRE, HDR_DISPONIVEL_POS(%ebx)
        jne fim_if1

        movl HDR_TAM_POS(%ebx), %ecx
        addl %ecx, HDR_TAM_POS(%eax)
        addl $TAM_HDR, HDR_TAM_POS(%eax)

    fim_if1:
        movl %ebx, %eax
        jmp while2

    fim_while2:
        ## Diminui o valor de brk se o ultimo segmento for liberado
        pos_final_livre:
            movl inicio_heap, %eax
            movl %eax, %ebx

    while3:
        addl $TAM_HDR, %ebx
        addl HDR_TAM_POS(%eax), %ebx
        cmpl brk_atual, %ebx
        jge fim_while3
        movl %ebx, %eax
        jmp while3
		
    fim_while3:
        cmpl $LIVRE, HDR_DISPONIVEL_POS(%eax)
        je diminui
        jmp fim_pos_final_livre

    diminui:
        movl %eax, %ebx
        movl $45, %eax
        int $0x80
        movl %eax, brk_atual

    fim_pos_final_livre:
        ret


############################
##                        ##
## imprMapa: IMPRIME MAPA ##
##                        ##
############################
.globl imprMapa
.type imprMapa, @function

imprMapa:
    pushl   %ebp
    movl    %esp, %ebp

    ## Empilha o endereco de inicio da heap
    ## e a string para a funcao printf
    pushl   inicio_heap
    pushl   $InicioBss
    call    printf

    # Restaura o tamanho da pilha
    addl    $8, %esp
    subl    $29, %esp

    movl    $1, -4(%ebp)            # i = -4(%ebp)
    movl    $0, -8(%ebp)            # o = -8(%ebp)
    movl    $0, -12(%ebp)           # l = -12(%ebp)
    movl    $0, -16(%ebp)           # somaO = -16(%ebp)
    movl    $0, -20(%ebp)           # somaL = -20(%ebp)
    movl    inicio_heap, %eax
    movl    %eax, -24(%ebp)         # comeco = -24(%ebp)

    ## Loop por toda memoria alocada ##
    while:
        cmpl    brk_atual, %eax
        jge     end_while
    IF:
        cmpl    $OCUPADO, 0(%eax)       # if (0(%eax) == UNAVAILABLE)
        jne     ELSE                    # Pula para ELSE se a memória não estiver indisponível
        pushl   %eax                    # Salva %eax
        pushl   4(%eax)                 # Empilha o total de bytes do segmento corrente
        pushl   -4(%ebp)                # Empilha a variável i que tem o nº do segmento atual de memória
        pushl   $Seg1              	# Empilha a string que será impressa na tela
        call    printf                  # Já vimos como que tem que "linkar"
        addl    $12, %esp               # Restaura a pilha
        popl    %eax                    # Restaura %eax
        addl    $1, -8(%ebp)            # Incrementa o de 1, que é a quantidade de segmentos ocupados
        movl    4(%eax), %ebx           # Poe a quantidade de memória do segmento corrente em ebx
        movl    -16(%ebp), %ecx         # Poe o valor da quantidade de memória ocupada em %ecx
        addl    %ecx, %ebx              # Soma a quantidade de memória do segmento atual com o total de memória ocupada
        movl    %ebx, -16(%ebp)         # Move o total de memória ocupada para somaO
        jmp     END_IF
    ELSE:
        cmpl    $LIVRE, 0(%eax) 	# else if (0(%eax) == AVAILABLE)
        jne     END_IF
        pushl   %eax                    # Salva %eax
        pushl   4(%eax)                 # Empilha o total de bytes do segmento corrente
        pushl   -4(%ebp)                # Empilha a variável i que tem o nº do segmento atual de memória
        pushl   $Seg2              	# Empilha a string que será impressa na tela
        call    printf                  # Já vimos como que tem que "linkar"
        addl    $12, %esp               # Restaura a pilha
        popl    %eax                    # Restaura %eax
        addl    $1, -12(%ebp)           # Incrementa l de 1, que é a quantidade de segmentos livres
        movl    4(%eax), %ebx           # Poe a quantidade de memória do segmento corrente em ebx
        movl    -20(%ebp), %ecx         # Poe o valor da quantidade de memória ocupada em %ecx
        addl    %ecx, %ebx              # Soma a quantidade de memória do segmento atual com o total de memória livre
        movl    %ebx, -20(%ebp)         # Soma o total de memória livre
    END_IF:
        addl    4(%eax), %eax
        addl    $TAM_HDR, %eax
                                        # Incrementa comeco de HDR_SIZE + o tamanho do bloco de memória
                                        # deslocando %eax para o próximo bloco de memória
        movl    %eax, -24(%ebp)         # Coloca o valor de %eax em comeco
        addl    $1, -4(%ebp)            # Incrementa i de 1
        jmp     while
    end_while:
        pushl   %eax
        pushl   -16(%ebp)               # Empilha o total de bytes ocupados
        pushl   -8(%ebp)                # Empilha a variável o que tem o total de segmentos ocupados
        pushl   $SegOcupados            # Empilha a string que será impressa na tela
        call    printf                  # Já vimos como que tem que "linkar"
        addl    $12, %esp               # Restaura a pilha das variáveis usadas para o printf
        popl    %eax

        pushl   %eax
        pushl   -20(%ebp)               # Empilha o total de bytes livres
        pushl   -12(%ebp)               # Empilha a variável l que tem o total de segmentos livres
        pushl   $SegLivres              # Empilha a string que será impressa na tela
        call    printf                  # Já vimos como que tem que "linkar"
        addl    $12, %esp               # Restaura a pilha das variáveis usadas para o printf
        popl    %eax
        addl    $29, %esp               # Restaura a pilha inteira
        movl    %ebp, %esp
        popl    %ebp
        ret                             # Termina a função


#######################
##                   ##
## meuCalloc:        ##
##                   ##
#######################
.globl meuCalloc
.type meuCalloc, @function
.equ NMEMB, 12
.equ SIZE, 8

meuCalloc:	
	pushl 	%ebp
	movl	%esp, %ebp		#Local do Erro
		
	movl 	NMEMB(%ebp),%eax
	movl	SIZE(%ebp), %ebx
	imul	%eax, %ebx

	pushl	%ebx
	call	meuAlocaMem
	popl	%ebx
	
	movl    %ebp, %esp
	popl	%ebp
	ret
