Software Básico - Prova 2:

1. Considere a execução de um programa executável e de um programa interpretado
   em Linux. Deferencie-os no que se refere a:

a) Localização das variáveis globais;

  Basicamente um programa interpretado possui um modelo de espaço dinâmico para
  as variáveis globais que - ao contrário de um programa executável - podem
  mudar em tempo de execução.

  Este é um aspecto da maioria das linguagens interpretadas, que geralmente
  gerenciam uma tabela de variáveis que é modificada e consultada mediante
  mecanismos próprios à medida que o programa necessita.

b) Registros de ativação

  Para um código interpretado, o programa a ser exeutado residirá na seção
  `.data`, e o interpretador na seção `.text`. Já um código executado usará a
  seção `.data` para armazenar algumas informações, e terá na seção `.text`
  instruções específicas para o processador e a arquitetura em questão.

c) Execução

  A execução de um programa executável dá-se mediante intruções de máquina
  nativamente projetadas para determinado processador e arquitetura. Um
  programa interpretado geralmente necessita de uma estrutura intermediária
  que lerá o código e o interpretará para então executar instruções de máquina.

2. Diferencie as bibliotecas:

a) Estáticas de compartilhadas;

  A diferença essencial é que as bibliotecas compartilhadas permitem que somente
  uma cópia de cada biblioteca seja colocada na memória e utilizada por vários
  programas que necessitem dos procedimentos implementados, sem gerar um mal
  uso de memória ao fazer várias cópias de um mesmo trecho de instruções para
  ser utilizado por cada programa separadamente.

b) Estáticas de dinâmicas;

  Bibliotecas estáticas são carregadas quando o programa é executado, e assim
  permanecem até que o programa seja finalizado. Já as bibliotecas dinâmicas
  podem ser carregadas e descarregadas mediante instruções do programa à medida
  que ele necessita das implementações destas bibliotecas.

  A percepção de bom uso da memória neste caso fica a cargo do programador, que
  deve avaliar os momentos adequados para efetuar o carregamento e
  descarregamento das bibliotecas na memória de acordo com os processos em
  curso no programa.

c) Compartilhadas de dinâmicas.

  Por mais que uma biblioteca seja compartilhada, ela também pode resultar em
  mal uso de memória devido à pouca utilização pelo conjunto de programas
  sendo executados. Assim pode-se gerenciar o carregamento destas bibliotecas
  para que somente aquelas que serão efetivamente utilizadas sejam carregadas.

3. É sabido que um arquivo executável contém um programa a ser executado sob um
   sistema operacional específico e sob uma arquitetura (ou processador)
   específico. Na maioria das vezes, um código executável em uma arquitetura
   (SO + CPU) não pode ser executado em outra arquitetura.

a) Descreva em linhas gerais como fazer para que um arquivo executável seja
   também executado em outra arquitetura. Por exemplo, um arquivo executável no
   formato XYZ, cpu 987 em um computador que aceita arquivos de execução em
   formato ABC na cpu 123.

  Podem-se usar emuladores, interpretadores ou adaptadores.

  De modo geral, `emuladores` estão mais relacionados com sistemas de hardware,
  e constituem em implementações que fazem uma CPU 987 comportar-se como um
  outro tipo 123 de CPU. Neste caso, o sistema 987 está preparado para ler as
  instruções de hardware do sistema 123 e convertê-las para um formato adequado
  que permita o seu funcionamento em uma arquitetura 987.

  Já o termo `interpretador` está mais ligado a software, existindo algumas
  contradições do termo com os emuladores no que diz respeito à software básico.
  Aqui um determinado tipo de arquivo executável XYZ é interpretado, tendo suas
  configurações e particularidades convertidas para os formatos necessários
  à interpretação do tipo de arquivo ABC. Se isto implicar em chamadas de
  sistema e endereços de memória diferenciados, então um novo conjunto de
  implementações de chamadas de sistema faz-se necessário.

b) Existe uma "arquitetura universal" (algum formato executável que pode ser
   executado em qualquer SO + CPU). Como e porque ela funciona?

  Neste caso, há de fato uma `arquitetura universal`, mas que funciona com fins
  de portagem multipltaforma ao invés de ser destinada a uma CPU real. Um
  compilador deve ser capaz de traduzir um código fonte para um executável
  baseado nas instruções desta arquitetura padronizada, que aí sim será lido
  por interpretadores de diferentes CPUs e SOs.

  Isso faz com que o arquivo executável seja o mesmo para diferentes CPUs e SOs
  - o chamado multiplataforma, embora o interpretador deste arquivo seja
  diferente para cada sistema. No casa de Java, este executável chama-se
  `bytecode`, e segue o formato ELF, com instruções do padrão `bytecode` na
  seção `.text` ao invés de código assembly.
