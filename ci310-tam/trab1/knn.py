#!/usr/bin/python
# coding=utf-8

import os
import sys
import optparse
import math
import operator

class Knn:

    def __init__(self):
        self.nrows = 0             # número de linhas
        self.ncols = 0             # número de colunas
        self.match = 0             # número de acertos
        self.rows = list()         # linhas lidas do arquivo
        self.dists = list()        # distâncias relativas das linhas
        self.classes = list()      # classes das linhas
        self.uclasses = list()     # classes possíveis
        self.confm = list()        # matriz de confusão
        self.k = 0                 # valor K entrado no comando

    def parse_file(self, f):
        f = open(f, 'r')
        self.nrows, self.ncols = [int(i) for i in f.readline().split(' ')]

        for n in range(self.nrows):
            row = f.readline().split(' ')
            self.classes.append(int(row.pop()))
            self.rows.append([float(r) for r in row])

        for c in self.classes:
            if c not in self.uclasses:
                self.uclasses.append(c)
        self.uclasses.sort()

        for i in self.uclasses:
            self.confm.append(list())
            for j in self.uclasses:
                self.confm[i].append(0)

    def euclidean_distance(self, x, y):
        s = 0
        for i in range(len(x)):
            s += math.pow(x[i] - y[i], 2)
        return math.sqrt(s)

    def calculate_row(self, row, training):
        row = self.rows[row]
        dists = list()
        for i in range(training.nrows):
            dists.append((
                self.euclidean_distance(row, training.rows[i]),
                self.classes[i]))

        dists.sort(key = lambda x:x[0])
        w = {}
        for dist, i in dists[:self.k]:
            w[i] = w[i] + 1 if w.has_key(i) else 1
        w = sorted(
            w.iteritems(),
            key = operator.itemgetter(1),
            reverse = True)
        return w[0][0]

    def calculate(self, training, k):
        self.k = k
        for i in range(self.nrows):
            val = self.calculate_row(i, training)
            if val == self.classes[i]:
                self.match += 1
            self.confm[self.classes[i]][val] += 1

    def print_matches(self):
        print 'K = %d' % self.k
        print 'Taxa de Reconhecimento: %.2f%%' % (
            float(self.match) / float(self.nrows) * 100)

    def print_matrix(self):
        print '      ',
        for j in self.uclasses: print '%3d' % j,
        print
        print '      ',
        for j in self.uclasses: print '---',
        print
        for i in self.uclasses:
            print '%3d' % i, '| ',
            for j in self.uclasses:
                print '%3d' % self.confm[i][j],
            print

if __name__ == '__main__':

    parser = optparse.OptionParser()
    parser.add_option('--training', dest = 'training', metavar = 'FILE',
        help = 'arquivo de treinamento')
    parser.add_option('--testing', dest = 'testing', metavar = 'FILE',
        help = 'arquivo de teste')
    parser.add_option('--k', dest = 'k', metavar = 'INTEGER',
        help = 'valor de K > 0 a ser calculado')

    (options, args) = parser.parse_args()

    if not options.training or not options.testing \
        or not options.k or int(options.k) <= 0:
        parser.print_help()
        exit(0)

    if not os.path.isfile(options.training):
        sys.stderr.write(u"[knn] Erro: arquivo \'%s\' invalido\n" %
            options.training)
        exit(0)

    if not os.path.isfile(options.testing):
        sys.stderr.write(u"[knn] Erro: arquivo \'%s\' invalido\n" %
            options.testing)
        exit(0)

    training = Knn()
    training.parse_file(options.training)

    testing = Knn()
    testing.parse_file(options.testing)
    testing.calculate(training, int(options.k))
    testing.print_matches()
    testing.print_matrix()
