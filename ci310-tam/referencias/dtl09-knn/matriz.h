//
//  matriz.h
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface matriz : NSObject

@property (nonatomic,retain) NSMutableArray *matrix;
@property NSUInteger largura, altura;

- (void)inicializaComLargura:(NSUInteger)width eAltura:(NSUInteger)height;
- (void)setaValor:(float)value naColuna:(NSUInteger)col eLinha:(NSUInteger)lin;
- (float)valorDaColuna:(NSUInteger)col eLinha:(NSUInteger)lin;
- (void)meMata;
- (void)imprime;

@end
