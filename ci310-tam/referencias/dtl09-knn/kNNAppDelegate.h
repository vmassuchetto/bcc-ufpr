//
//  kNNAppDelegate.h
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class kNNViewController;

@interface kNNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) kNNViewController *viewController;

@end
