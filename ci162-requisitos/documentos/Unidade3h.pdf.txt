Unidade 3: Especificação de Requisitos (Parte h)

1

3.3.4. Identificação orientada a responsabilidades
Nesta técnica, a ênfase está na identificação de classes a partir dos seus comportamentos. O
esforço do modelador recai sobre a identificação das responsabilidades que cada classe tem no
sistema.
Segundo os autores (Wirfs-Brock e Wilkerson), o método enfatiza o encapsulamento da estrutura
e do comportamento dos objetos, pondo o foco nas responsabilidades de uma classe úteis
externamente à mesma. Os detalhes internos à classe (por exemplo, como ela faz para cumprir
suas responsabilidades) devem ser abstraídos.
Conceitos: Na prática, uma responsabilidade é algo que um objeto conhece ou faz, sozinho ou
com a ajuda de outros objetos. No caso de o objeto não conseguir cumprir sozinho uma
responsabilidade, ele deve requisitar a colaboração de outros objetos. A colaboração entre
objetos é feita via troca de mensagens.
Exemplo de classes que colaboram entre si para atingir os objetivos do sistema:
Cliente
nome
endereço
1

*
Pedido
ItemPedido
data
♦
quantidade
hora
1
* obterTotal()
obterTotalPedido()

Produto
nome
1 descrição
preçoUnitário
desconto
obterValor

*

3.3.5. Modelagem CRC
Conceito: A Modelagem CRC (Classe-Responsabilidade-Colaboradores) foi proposta por Beck
e Cunningham como uma forma de ensinar o paradigma de OO. Contudo, a simplicidade da
notação acabou levando à sua utilização na identificação de classes.
Formato Cartão CRC:
NomeClasse
Responsabilidade

Colaboradores

1a responsabilidade

1o colaborador, 2o colaborado

2a responsabilidade

3o colaborador

...

...

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte h)

2

Exemplo:
Para o diagrama de classes seguinte:
Cliente
nome
endereço
1

*
Pedido
ItemPedido
data
♦
quantidade
hora
1
* obterTotal()
obterTotalPedido()

Produto
nome
1 descrição
preçoUnitário
desconto
obterValor

*

os cartões CRC seriam:
Pedido
Responsabilidade

Colaboradores

obterTotalPedido

ItemPedido
ItemPedido

Responsabilidade

Colaboradores

obterTotal

Produto
Produto

Responsabilidade

Colaboradores

obterValor

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte h)

3

Estratégia: Parte-se das responsabilidades atribuídas ao sistema e, para cada uma delas, são
identificados os objetos envolvidos. Desta forma, as responsabilidades do sistema são distribuídas
entre os diversos objetos componentes do mesmo.
Esta técnica é útil na identificação de novas classes e na validação de classes obtidas por outras
técnicas.
Conceito: Nesta técnica, especialistas no domínio (negócio) e desenvolvedores trabalham juntos
numa sessão CRC:
1. Cada pessoa (de um total de em torno de 6) recebe um cartão denominado cartão
CRC;
2. Cada cartão corresponde a uma classe do sistema;
3. A coluna Colaboradores tem suas células definidas a partir das responsabilidades
atribuídas à classe;
4. Seleciona-se um caso de uso e, para cada cenário desse caso de uso, é feita uma
sessão CRC (Se o caso de uso for simples, ele pode ser analisado numa única
sessão);
5. O cartão CRC é dividido em várias partes: a parte superior aparece o nome de uma
classe. A parte inferior é dividida em duas colunas. Na coluna à esquerda, o “dono” do
cartão deve listar as responsabilidades da classe. Na coluna à direita, o nome das
outras classes que colaboram para que a classe cumpra suas responsabilidades;
6. A sessão começa com algum dos participantes simulando um ator primário do caso
de uso que “dispara” a sua execução. À medida que esse participante simula a
interação do ator com o sistema, os demais participantes da sessão encenam a
colaboração entre objetos para que o objetivo do ator seja alcançado;
7. Para cada responsabilidade atribuída a uma classe, o seu dono questiona se a classe
é capaz de cumprir com a responsabilidade sozinha. Se ela precisar de ajuda, ela é
dada por um colaborador. Os participantes da sessão, então, decidem qual outra
classe deve prestar a ajuda necessária à classe em foco. Se essa classe existir, ela
recebe esta nova responsabilidade. Do contrário, um novo cartão é aberto para uma
nova classe que a assume;
8. Uma classe pode participar de mais de um caso de uso. À medida que as sessões
CRC vão acontecendo, as classes vão acumulando responsabilidades;
9. Durante uma sessão CRC uma ou mais das seguintes ações podem ser necessárias:
adicionar uma responsabilidade a uma classe já existente; criar uma nova classe para
realizar uma responsabilidade identificada; mover responsabilidades quando uma
classe se tornar excessivamente complexa.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte h)

4

Diagramas de sequência (em tempo... “no espírito” da OO)
Os diagramas de sequência descrevem o comportamento do sistema. O comportamento do
sistema deve ser visto como uma “caixa preta”, devendo descrever o que o sistema faz e não
explicar como ele o faz.
Durante a interação ocorrida na execução de um caso de uso, os atores geram eventos e
requerem alguma operação como resposta do sistema.
A justificativa desta descrição em separado reside no fato de que as operações resultantes das
solicitações dos atores constituem uma parte importante do comportamento de um sistema.
Conceito: Um diagrama de sequência é uma figura que mostra, para um cenário particular de um
caso de uso, os eventos externos (gerados pelos atores) e a ordem em que eles se sucedem.
A criação dos diagramas de sequência têm como objetivo identificar os eventos e as operações do
sistema.
Esta atividade faz parte da Análise do sistema a ser construído. Os diagramas de sequência são,
assim, parte do modelo da Análise.
Devem ser construídos Diagramas de sequência para os cenários típicos (fluxo principal) e,
também, para os cenários alternativos de relevância especial.
Observação: O sistema deve ser tratado nesta atividade como uma “caixa preta”. A ênfase no
Diagrama deve ser posta nos eventos que atravessam a fronteira do sistema entre os atores e
os sistemas.
Elementos: Um Diagrama de sequência mostra, para uma instância particular de um caso de uso,

•

os atores externos que interagem diretamente com o sistema;

•

o sistema como “caixa preta” e

•

os eventos que os atores geram.

Tudo é representado na ordem cronológica.
Exemplo: O fluxo típico do caso de uso “Comprar produtos”, onde o caixa é o único ator do
sistema.
Como “caixa 
preta”
Caixa
Repeat until
no more

Sistema
entrarItem(item,quantidade)
fecharVenda
fazerPagamento(quantia)

Texto que esclarece questões 
de controle, lógica, iteração,...

Evento do sistema 
Dispara uma operação
no sistema.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte h)
Conceitos: Um evento do sistema é um evento externo gerado por um ator para o sistema.
Uma operação do sistema é uma ação que o sistema executa em resposta a um evento do
sistema (ação externa).
Em outras palavras, eventos do sistema são os estímulos e operações são as respostas.
Elaboração de um Diagrama de sequência:

1. Desenhe uma linha representando o sistema como uma “caixa preta”;
2. Identifique cada ator que interage diretamente com o sistema. desenhe uma linha para
cada ator;

3. A partir do texto descritivo do cenário típico (fluxo principal) do caso de uso, identifique os
eventos do sistema que cada ator gera e represente-os no Diagrama;

4. Nos casos em que uma operação disparada por um evento não seja óbvia, inclua-a como
comentário associado ao evento;

5. Opcionalmente, inclua o texto do caso de uso à esquerda do Diagrama.
Rotulação:
Os eventos do sistema e as operações do sistema associadas devem ser expressos no nível da
intenção do ator e não em termos dos objetos físicos de entrada (widgets).
Exemplos:
entrarItem(item,quantidade)

(solução melhor)

entrarBotãoItem(item,quantidade)

(solução a ser evitada)

fazerPagamento(quantia)

(solução melhor)

entrarPagamento(quantia)
entrarQuantiaDigitada(quantia)

(solução pior)

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

5

