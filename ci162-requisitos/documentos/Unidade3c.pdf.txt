Unidade 3: Especificação de Requisitos (Parte b)

1

3.2. Mecanismos de Apoio na UML (portanto, matéria entre parênteses)
Há diversos mecanismos de apoio úteis nas diversas representações na UML
3.2.1. Estereótipos
Conceito: Um estereótipo é um dos mecanismos da UML de uso geral, utilizado para estender o
significado de um determinado diagrama.
Conceitos: A UML predefine diversos estereótipos e permite que o usuário defina os estereótipos
a serem utilizados em determinada situação de modelagem. Assim, existem dois tipos de
estereótipos: aqueles predefinidos pela UML e aqueles definidos pela equipe de
desenvolvimento (incluindo usuários).
Os estereótipos definidos pela equipe de desenvolvimento devem ser expressos de tal forma que
a sua semântica seja entendida sem ambiguidades por toda a equipe.
Além disso, um estereótipo definido pelo usuário deve ser utilizado de forma consistente em toda
a modelagem do sistema. Em outras palavras, não é correto utilizar um mesmo estereótipo para
denotar diferentes significados, mesmo em diferentes modelos dentro de um projeto.
Outra classificação para os estereótipos é a de estereótipos gráficos (chamados popularmente
de ícones) e estereótipos textuais (rótulos).
Exemplos (estereótipos gráficos):

Ator


Os estereótipos gráficos exibidos acima são: Ator e Componente (predefinidos pela UML) e
computador (supostamente definido pela equipe).
Um estereótipo textual é uma forma de rotular a representação de um objeto num diagrama. Ele é
feito por meio da delimitação do rótulo por “aspas francesas”.
Exemplos (estereótipos textuais):
<<documento>>

<<interface>>

<<controle>>

<<entidade>>

Estes quatro exemplos são predefinidos pela UML.
3.2.2. Notas explicativas
Conceito: Notas explicativas são utilizadas para comentar ou esclarecer alguma coisa sobre um
componente de um diagrama.
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte b)

2

As notas explicativas podem ser descritas em texto livre ou serem expressões formais na
linguagem de restrição de objetos da UML (OCL – Object Constraint Language).
Graficamente, as notas são representadas por um retângulo com uma quina virada para a frente.
O conteúdo da nota é inserido no retângulo e este é ligado ao elemento sobre o qual se quer fazer
a observação. Elas devem ser situadas próximas ao elemento ao qual se referem.



Este é um esterótipo que poderia ser definido pela
equipe para representar um ator quando o modo dele
em relação ao sistema fosse relevante.

3.2.3. Etiquetas valoradas (tagged values)
Os elementos gráficos de um diagrama UML possuem propriedades predefinidas. Uma classe, por
exemplo, tem três: nome, lista de atributos e lista de operações.
Conceito: Etiquetas valoradas são um mecanismo para definir novas propriedades para
determinados elementos de um diagrama.
Na UML 2.0 etiquetas valoradas somente podem ser definidas como atributos de estereótipos
(predefinidos ou definidos pelo usuário). Portanto, o elemento determinado deve primeiro ser
estendido por meio de um estereótipo para só depois ser estendido por uma etiqueta valorada.
Notação para a definição de etiquetas (tags) na UML:
{ tag = valor }
{ tag1 = valor1, tag2 = valor2...}
Exemplo de uso das etiquetas:
Uma etiqueta pode ser definida, por exemplo, para informar o autor e a data de criação de um
elemento ou diagrama.
Pedido
{autor: Maria Oliveira,
data de criação: 04/04/2009}
3.2.4. Restrições
Todo elemento da UML tem associada alguma semântica. Esse significado atribuído fica valendo
ao longo da utilização desse elemento ou diagrama no projeto todo.
Conceito: As restrições permitem estender ou alterar a semântica natural de um elemento gráfico.
Este mecanismo geral especifica restrições sobre um ou mais valores de um ou mais elementos
de um modelo.
Restrições podem ser especificadas tanto formal quanto informalmente. A especificação informal é
feita em texto livre. A descrição formal é realizada por meio da OCL.
Notação: As devem ser como notas explicativas {restrição qualquer }
3.2.5. Pacotes
Conceito: Um pacote é um agrupamento de elementos semanticamente relacionados, que deve
ter um nome associado.
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte b)

3

Notação: Os pacotes são representados por meio de retângulos com abas.
Conceito: Ligações entre pacotes indicam interdependência entre eles. Um pacote P1 é
dependente de um pacote P2 se algum elemento contido em P1 depende de algum elemento
contido em P2. O significado específico de uma dependência pode ser especificado por meio de
estereótipos.
Um pacote pode ser utilizado para agrupar quaisquer elementos, inclusive outros pacotes.
Representação dos componentes de um pacote: Há duas formas de representar o conteúdo de
um pacote: com os elementos desenhados no seu interior ou com eles como “filhos” numa
estrutura hierárquica.
3.2.6. A OCL
Conceito: A UML define uma linguagem formal eu pode ser utilizada para especificar restrições
sobre diversos elementos de um modelo. Esta linguagem se chama OCL.
Aplicações: A OCL pode ser usada, entre outras coisas, para definir:
•

expressões de navegação entre objetos;

•

expressões lógicas;

•

pré-condições;

•

pós-condições.

Notação: A maioria das declarações em OCL consiste de três tipos de elemento:
a) contexto;
b) propriedade;
c) operação.
Conceito: O contexto define o domínio ao qual a declaração em OCL se aplica.
Exemplos: uma instância ou uma classe
Conceito: Propriedades dentro de uma declaração em OCL correspondem a elementos do
domínio.
Exemplos: o nome de um atributo em uma classe, uma associação entre dois objetos.
Conceito: Operações podem envolver operadores:
•

aritméticos;

•

de conjuntos;

•

de tipo;

•

outros: and, or, implies, if, then, else, not, in.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

