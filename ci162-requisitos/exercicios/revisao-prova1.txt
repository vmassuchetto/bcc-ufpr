Engenharia de requisitos – 1o semestre de 2011 - Revisão unidades 1 e 2

1.  Conceitos

    * perfis de usuário:
      conjunto de usuário potenciais com capacidades e interesses
      semelhantes em relação ao sistema

    * stakeholders
      todos os que possam vir a ser materialmente afetados pelo sistema

    * requisitos
      conjunto de necessidades reais dos usuários

    * domínio de aplicação
      área de conhecimento e aplicação do sistema

    * atividades no desenvolvimento de software
      especificação dos requisitos, análise, projeto, implementação,
      testes, implantação

    * etapas no desenvolvimento de software

    * engenharia de requisitos
      procedimentos e técnicas utilizadas no processo de desenvolvimento de
      um software no que compete às etapas de levantamento de requisitos e
      análise

    * modelo de ciclo de vida
      o modo como o fluxo de trabalho é encadeado e organizado segundo as
      fases de desenvolvimento de um software

    * ciclo de vida em cascata, sequencial, tradicional ou clássico
      modelo em que se executam as tarefas linearmente, o que é um problema
      já que raramente os projetos são lineares e as especificações das
      fases dificilmente são completas e detalhadas o suficiente para uma
      implantação satisfatória

    * ciclo de vida iterativo incremental
      envolve ciclos que compõem as atividades do desenvolvimento clássico,
      visando requisitos e entregas parciais de um produto passível de uso

2.  Atividades comuns às diversas abordagens ou metodologias de
    desenvolvimento de software

    * Levantamento e especificação de requisitos
    * Análise
    * Projeto (Design)
    * Implementação
    * Testes
    * Implantação

3. Fases do processo de desenvolvimento

    * Concepção
      - ideia geral e escopo
      - planejamento de alto nível
      - separação das fases

    * Elaboração
      - entendimento inicial de construção do software
      - planejamento do projeto de desenvolvimento
      - análise do domínio do negócio
      - classificação dos requisitos pelas prioridades e riscos
      - planejamento das iterações da fase seguinte

    * Construção
      - aumento das atividades de análise
      - aumento das iterações incrementais
      - tomada de decisão sobre entrega do produto
      - se entregue, fazem-se manuais e descrição dos incrementos

    * Transição
      - treinamento dos usuários
      - instalação e configuração
      - avaliação da aceitação do incremento pelos usuários
      - avaliação dos gastos

4.  Distribuição (tendência) das atividades nas fases

    A tendência na evolução de um projeto é que as atividades relacionadas à
    análise e ao levantamento de requisitos fiquem menos intensas.

5.  Diferença estrutural entre as abordagens tradicional e iterativa
    incremental

    No modelo clássico as fases são lineares, isto é, pressupõe-se que as
    atividades são definitivamente concluídas antes de se passar para a
    próxima fase. Assim naturalmente gasta-se mais tempo em cada fase a fim
    de cobrir um maior número de atividades para um grande número de
    requisitos enumerados.

    Já o modelo iterativo incremental trabalha com a mesma ideia do modelo
    clássico mas com um número reduzido de requisitos e visando a entrega
    parcial do produto.

6.  Diferença na essência entre as duas abordagens que determinam a melhoria
    do produto final

    Os projetos raramente são lineares, além de ser muito difícil
    identificar satisfatoriamente os requisitos para todo um sistema de uma
    só vez, pois eles são voláteis e se modificam muito à medida que se
    aprofundam os estudos.

    Ao trabalhar com um conjunto reduzido e concentrado de requisitos
    pode-se dar prioridade às necessidades mais críticas, além da diminuição
    dos riscos de desenvolvimento, uma vez que o usuário interage e valida
    as implementações de cada ciclo, fornecendo um direcionamento melhor
    para os analistas em relação aos próximos ciclos, e fazendo com que os
    danos de um risco grave sejam minimizados.

7.  Conceitos:

    * domínio do problema
      análise do problema a ser resolvido

    * engenharia de sistemas
      conjunto de métodos e técnicas utilizadas para a compreensão e
      resolução de problemas do mundo real e das necessidades do usuário

    * domínio do negócio
      identificação e modelagem dos objetos no mundo real que serão usados
      no desenvolvimento do sistema

    * análise do negócio
      processo de compreensão da dinâmica das organizações que envolve o
      sistema, de novos sistemas que possam vir a ser afetados pela
      construção de um novo, e também a busca pelo concenso entre usuários e
      desenvolvedores sobre o problema

    * problema
      a discrepância de um procedimento no mundo real para o que realmente
      se espera que ele seja

    * problema raiz ou raiz do problema
      um problema que acarreta em outro, em que geralmente são casos do tipo
      que diversos pequenos problemas acarretam em um maior

    * fronteiras do sistema
      o limiar entre a solução e o mundo real que a rodeia, no sentido de
      esclarecer quem são os atores e restrições

    * restrições, rationale
      grau de liberdade do projetista no desenvolvimento da solução

8.  Possíveis formas de resolver um problema

    desenvolver um novo sistema ou aperfeiçoar algum já existente,
    proporcionar soluções alternativas que não envolvem o desenvolvimento de
    um sistema, ou promover mais treinamentos a fim de cobrir
    funcionalidades existentes mas não utilizadas

9.  Passos envolvidos na resolução de um problema pela engenharia de sistemas

    * definir o problema em concordância com usuários e desenvolvedores
      é necessária a compreensão sobre o problema a ser resolvido, obtida
      através de entrevistas e questionários com os envolvidos no modelo:

        => o problema __ afeta __ e resulta em __ e poderia ser resolvido
           da forma __  com os benefícios de __

    * identificar problemas raiz
      para a identificação da raiz dos problemas pode-se também usar um
      diagrama de espinha de peixe, elencando todos os problemas menores que
      acarretam em um problema maior

    * identificação de stakeholders
      todos os usuários potenciais interessados e materialmente afetados
      pelo sistema, que compreendem as categorias:

10. Classes de stakeholders

    * contribuintes
      - executores: funcionários que operam o sistema
      - responsáveis: gerentes e responsáveis pela ideia, são os que mais
                      se beneficiam do sistema

    * fontes:
      - clientes: beneficiam-se dos produtos indiretos do sistema
      - fornecedores: promovem condições para que o sistema funcione

    * mercado:
      - parceiros: compartilhadores de recursos e ajudantes na
                   resolução de problemas
      - competidores: quem desafia concorrendo ou entrando em conflitos

    * comunidade:
      - legisladores: estabelecedores de regras e convenções
      - espectadores:  comunidade que ganha ou perde com o sistema

11. Elementos que precisam ser identificados e/ou construídos na modelagem
    do negócio

12. Características diferenciais da técnica de entrevista (potencial e
    limitações)

13. Características diferenciais da técnica de workshop de requisitos
    (potencial e limitações);

14. Características diferenciais da técnica de brainstorming
    (potencial e limitações).
